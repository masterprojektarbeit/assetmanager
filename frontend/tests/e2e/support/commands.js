// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("login", () => {
  cy.request("POST", "http://localhost:3000/api/Users/login", {
    email: "admin@test.de",
    password: "test"
  }).then(response => {
    sessionStorage.setItem("accessToken", response.body.id);
  });
});

Cypress.Commands.add("logout", () => {
  cy.request(
    "POST",
    `http://localhost:3000/api/Users/logout?access_token=${sessionStorage.getItem(
      `accessToken`
    )}`,
    {}
  );
});

Cypress.Commands.add("hideMenu", () => {
  cy.viewport("macbook-15");
  const menueButton = cy.get(
    ".v-toolbar__side-icon > .v-btn__content > .v-icon"
  );
  menueButton.click();
});

// geklaut von https://github.com/javieraviles/cypress-upload-file-post-form
/*
Lädt eine Datei in ein per Selektor angegebenes Inputfield.
Die Datei muss unter dem angegebenen Dateinamen im fixtures-ordner vorhanden sein.
*/
Cypress.Commands.add("upload_file", (fileName, fileType = " ", selector) => {
  cy.get(selector).then(subject => {
    cy.fixture(fileName, "base64")
      .then(Cypress.Blob.base64StringToBlob)
      .then(blob => {
        const el = subject[0];
        const testFile = new File([blob], fileName, { type: fileType });
        const dataTransfer = new DataTransfer();
        dataTransfer.items.add(testFile);
        el.files = dataTransfer.files;
      });
  });
});

/*
Setzt die Datenbank zurück (abgesehen vom Benutzer)
Falls im Backend Dev-API-Endpunkte verfügbar sind über diese, sonst per CLI
*/
Cypress.Commands.add("resetDB", () => {
  cy.request({
    url: `http://localhost:3000/dev/resetDatabase`,
    failOnStatusCode: false
  }).then(res => {
    if (res.status !== 200) {
      //dev api nicht verfügbar
      let commandBase = `${
        Cypress.platform === "win32"
          ? 'cd /d "..\\backend\\\\"'
          : "cd ../backend/"
      } & node . `;
      cy.exec(commandBase + "--migrate-db").then(() => {
        cy.exec(commandBase + "--fixture-user").then(() => {});
      });
    }
  });
});

/**
 * Lädt einen "lokalen blob" herunter und gibt den Inhalt zurück
 * Entnommen aus: https://github.com/cypress-io/cypress/issues/949
 */

Cypress.Commands.add("downloadLocalBlob", url => {
  return new Cypress.Promise(resolve => {
    // Use XHR to get the blob that corresponds to the object URL.
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "blob";

    // Once loaded, use FileReader to get the string back from the blob.
    xhr.onload = () => {
      if (xhr.status === 200) {
        const blob = xhr.response;
        const reader = new FileReader();
        reader.onload = () => {
          // Once we have a string, resolve the promise to let
          // the Cypress chain continue, e.g. to assert on the result.
          resolve(reader.result);
        };
        reader.readAsText(blob);
      }
    };
    xhr.send();
  });
});

/**
 * auswahl auf einer vuetify radiogroup mit label
 * @param{selector} jquery selector
 * @param{label} text of the radio button label to click
 */
Cypress.Commands.add("clickRadioButton", (selector, label) => {
  cy.get(selector)
    .contains(label)
    .click({ force: true });
});

/**
 * Ermittelt Validationmessage von vuetify textfeldern
 * @param{fixtureDirectory} subdirectory in config.backendFixturesFolder
 * @param{dontAppend} überschreibt DB-Inhalte
 */
Cypress.Commands.add("getValidationString", selector => {
  return findFromCommonAncestor(
    selector,
    ".v-input__control",
    ".v-text-field__details > .error--text > .v-messages__wrapper > .v-messages__message"
  );
});

/**
 * auswahl auf einem vuetify select
 * @param{selector} jquery selector
 * @param{option} zu selektierender wert
 */
Cypress.Commands.add("chooseSelectOption", (selector, option) => {
  cy.get(selector)
    .first()
    .click({ force: true });
  cy.get(".menuable__content__active > .v-select-list > .v-list")
    .contains(option)
    .click();
});

/**
 * auswahl auf einem vuetify select
 * @param{selector} jquery selector
 */
Cypress.Commands.add("getSelectedOption", selector => {
  findFromCommonAncestor(
    selector,
    ".v-select__slot",
    ".v-select__selections > .v-select__selection"
  );
});

/**
 * auswahl auf einem vuetify select
 * @param{selector} jquery selector
 * @param{option} zu selektierender wert
 */
Cypress.Commands.add("clearSelection", selector => {
  cy.get(selector)
    .closest(".v-select__slot")
    .find(":nth-child(3)")
    .click();
});

/**
 * Navigiert nach oben von currentSelector zum nächsten ancestor hoch und von dort zu selector runter
 * @param {*} currentSelector
 * @param {*} ancestorSelector
 * @param {*} selector
 */
function findFromCommonAncestor(currentSelector, ancestorSelector, selector) {
  cy.get(currentSelector)
    .closest(ancestorSelector)
    .find(selector);
}
/*
 * Spielt Fixtures aus einem angegebenen Ordner des Backends in die Datenbank ein.
 * Der angegebene Pfad muss das Format .*\fixtures\.*\ (entsprechend für Windows mit /) besitzen.
 * Vorhandene BeispielFixtures sind unter /backend/fixtures/test zu finden
 */
Cypress.Commands.add("insertFixture", fixtureFolder => {
  cy.request({
    url: `http://localhost:3000/dev/fixture`,
    failOnStatusCode: false,
    method: "POST",
    body: {
      path: fixtureFolder
    }
  }).then(res => {
    if (res.status !== 200) {
      //dev api nicht verfügbar
      let commandBase = `${
        Cypress.platform === "win32"
          ? 'cd /d "..\\backend\\\\"'
          : "cd ../backend/"
      } & node . `;
      cy.exec(`${commandBase} --fixture ${fixtureFolder}`).then(() => {});
    }
  });
});
