const VALUE_INPUT = "#tf_value";
const DESCRIPTION_INPUT = "#cb_description";
const CATEGORY_INPUT = "#select_category";
const SAVE_BUTTON = "#btn_upsert";
const DELETE_BUTTON = "#btn_delete";
const URL_BASE = "/favorites/";
const URL_NEW = "new/";
const DB_FIXTURE_BASE_PATH = "/fixtures/test/favorites";
const BACKEND_FAVORITES_PATH = "**/api/favorites";
const BACKEND_RECOMMENDATION_PATH = "**/api/funds/recommend";
const BACKEND_CATEGORIES_PATH = "**/api/categories/";
const PROPERTY_VALUE = "value";
const PROPERTY_DESCRIPTION = "description";
const PROPERTY_CATEGORY_ID = "categoryId";
const PROPERTY_ID = "id";

describe("Favoriten CRUD", function() {
  describe("Stubbed Tests", function() {
    describe("favorites_create", function() {
      before(() => {
        cy.resetDB();
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        //Stub
        cy.route("GET", `${BACKEND_CATEGORIES_PATH}`, [
          {
            id: 1,
            name: "KAT_1"
          },
          {
            id: 2,
            name: "KAT_2"
          }
        ]).as("categories");
        cy.route(
          "POST",
          `${BACKEND_FAVORITES_PATH}/**`,
          "fixture:favorites/get_empty_favorites.json"
        ).as("submit");
        cy.route(
          "POST",
          BACKEND_RECOMMENDATION_PATH,
          "fixture:favorites/funds_recommend_empty.json"
        ).as("recommendations");

        cy.visit(URL_BASE + URL_NEW);
      });

      it("sollte die Formularfelder für Beschreibung, Kategorie und Wert anzeigen", () => {
        cy.get(VALUE_INPUT).should("be.visible");
        cy.get(DESCRIPTION_INPUT).should("be.visible");
        cy.get(CATEGORY_INPUT).should("be.visible");
      });

      it("sollte die Seite mit leeren Werte laden", () => {
        cy.get(VALUE_INPUT).should("have.value", "0");
        cy.get(DESCRIPTION_INPUT).should("be.empty");
        cy.get(CATEGORY_INPUT).should("be.empty");
      });

      it("sollte das Eintragen eines Betrags erlauben", () => {
        cy.get(VALUE_INPUT).should("be.visible");
        cy.get(VALUE_INPUT).type("1234567890");
        cy.getValidationString(VALUE_INPUT).should("not.be.visible");
        cy.get(VALUE_INPUT).should("have.value", "1234567890");
      });

      it("sollte das Eintragen von Nachkommastellen erlauben", () => {
        cy.get(VALUE_INPUT).should("be.visible");
        cy.get(VALUE_INPUT).type("11.30");
        cy.getValidationString(VALUE_INPUT).should("not.be.visible");
        cy.get(VALUE_INPUT).should("have.value", "11.30");
      });

      it("sollte das Eintragen eines negativen ganzzahligen Betrags korrigieren", () => {
        cy.get(VALUE_INPUT).should("be.visible");
        cy.get(VALUE_INPUT).type("-11");
        cy.getValidationString(VALUE_INPUT).should("not.be.visible");
        cy.get(VALUE_INPUT).should("have.value", "11");
      });

      it("sollte das Eintragen eines negativen Betrags korrigieren", () => {
        cy.get(VALUE_INPUT).should("be.visible");
        cy.get(VALUE_INPUT).type("-412.00");
        cy.getValidationString(VALUE_INPUT).should("not.be.visible");
        cy.get(VALUE_INPUT).should("have.value", "412.00");
      });

      it("sollte das Eintragen einer Beschreibung erlauben", () => {
        cy.get(DESCRIPTION_INPUT).should("be.visible");
        let desc =
          'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ äöüÄÖÜß!"§$%&/()=+';
        cy.get(DESCRIPTION_INPUT)
          .clear() // entfernt test flakiness, weil die vorschlagsliste dann nicht im weg ist
          .type(desc);
        cy.getValidationString(DESCRIPTION_INPUT).should("not.be.visible");
        cy.get(DESCRIPTION_INPUT).should("have.value", desc);
      });

      it("sollte eine Validierungsnachricht bei fehlendem Betrag anzeigen", () => {
        //html element type number verhindert erstes clear
        cy.get(VALUE_INPUT).clear();
        cy.get(VALUE_INPUT).clear();
        cy.get(SAVE_BUTTON).click();
        cy.getValidationString(VALUE_INPUT).should(
          "contain",
          "Der Wert muss eine Zahl sein."
        );
      });

      it("sollte eine Validierungsnachricht bei fehlender Beschreibung anzeigen", () => {
        cy.get(DESCRIPTION_INPUT).clear();
        cy.get(SAVE_BUTTON).click();
        cy.getValidationString(DESCRIPTION_INPUT).should(
          "contain",
          "Die Beschreibung muss angegeben werden"
        );
      });

      it("sollte eine Kategorieauswahl zulassen", () => {
        cy.wait("@categories");
        cy.get("@categories").then(function(xhr) {
          let categories = xhr.response.body;
          cy.chooseSelectOption(CATEGORY_INPUT, categories[0].name);
          cy.getSelectedOption(CATEGORY_INPUT).should(
            "contain",
            categories[0].name
          );
        });
      });

      it("sollte bei Klick die Kategorie leeren", () => {
        cy.wait("@categories");
        cy.get("@categories").then(function(xhr) {
          let categories = xhr.response.body;
          cy.chooseSelectOption(CATEGORY_INPUT, categories[0].name);
          cy.clearSelection(CATEGORY_INPUT);
          cy.getSelectedOption(CATEGORY_INPUT).should(
            "not.contain",
            categories[0].name
          );
        });
      });

      it("sollte die Werte aus dem Formular bei Klick absenden", () => {
        cy.wait("@categories");
        cy.get("@categories").then(function(xhr) {
          let categories = xhr.response.body;
          let description = randomDescription(30);
          let value = randomNegativeValue(0, 5000);
          let categoryId = categories[0].id;
          let categoryName = categories[0].name;

          cy.get(DESCRIPTION_INPUT).clear();
          cy.get(DESCRIPTION_INPUT).type(description);
          cy.get(VALUE_INPUT).type(value);
          cy.chooseSelectOption(CATEGORY_INPUT, categoryName);
          cy.get(SAVE_BUTTON).click();
          cy.wait("@submit");
          cy.get("@submit").then(function(xhr) {
            expect(xhr.status).to.eq(200);
            expect(xhr.method).to.eq("POST");
            expect(xhr.requestBody).to.have.property(
              PROPERTY_CATEGORY_ID,
              categoryId
            );
            expect(xhr.requestBody).to.have.property(
              PROPERTY_DESCRIPTION,
              description
            );
            expect(xhr.requestBody).to.have.property(PROPERTY_VALUE, +value);
          });
        });
      });

      it("sollte Werte immer mit negativem Vorzeichen senden", () => {
        cy.wait("@categories");
        cy.get("@categories").then(function(xhr) {
          let categories = xhr.response.body;
          let description = "Beschreibung_412";
          let value = randomValue(0, 5000);
          let categoryId = categories[0].id;
          let categoryName = categories[0].name;

          cy.get(DESCRIPTION_INPUT).clear();
          cy.get(DESCRIPTION_INPUT).type(description);
          cy.get(VALUE_INPUT).type(value);
          cy.chooseSelectOption(CATEGORY_INPUT, categoryName);
          cy.get(SAVE_BUTTON).click();
          cy.wait("@submit");
          cy.get("@submit").then(function(xhr) {
            expect(xhr.status).to.eq(200);
            expect(xhr.method).to.eq("POST");
            expect(xhr.requestBody).to.have.property(
              PROPERTY_CATEGORY_ID,
              categoryId
            );
            expect(xhr.requestBody).to.have.property(
              PROPERTY_DESCRIPTION,
              description
            );
            expect(xhr.requestBody).to.have.property(PROPERTY_VALUE, -value);
          });
        });
      });

      it("sollte bei Klick auf die Vorschlagsliste das Formular mit Funddaten befüllen", () => {
        cy.server();
        cy.route(
          "POST",
          `${BACKEND_RECOMMENDATION_PATH}`,
          "fixture:favorites/funds_recommend.json"
        ).as("recommendations");

        cy.wait("@categories");
        cy.get("@categories").then(function() {
          let description = "Beschreibung_413";

          cy.get(DESCRIPTION_INPUT).clear();
          cy.get(DESCRIPTION_INPUT).type(description);
          cy.wait("@recommendations");
          cy.get("@recommendations").then(function() {
            cy.chooseSelectOption(DESCRIPTION_INPUT, "TF_UseRec");

            //assert data gets loaded from recommendation funds fixture fixtures/favorites/funds_recommend
            cy.get(VALUE_INPUT).should("have.value", "23.50");
            cy.get(DESCRIPTION_INPUT).should("have.value", "TF_UseRec");
            cy.getSelectedOption(CATEGORY_INPUT).should("contain", "KAT_2");
          });
        });
      });

      it("sollte nach dem Anlegen auf zur Liste zurückkehren", () => {
        let desc = randomDescription(50);
        let value = randomValue(1, 9000);
        cy.get(DESCRIPTION_INPUT)
          .type("a")
          .type(desc);
        cy.get(VALUE_INPUT).type(value);
        cy.get(SAVE_BUTTON).click();
        cy.wait("@submit");
        cy.get("@submit").then(function() {
          cy.url().then(url => {
            expect(url).to.match(/favorites(\/)?$/);
          });
        });
      });
    });

    describe("favorites_update", function() {
      before(() => {
        cy.resetDB();
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        //Stub Fund Daten
        cy.route("GET", `${BACKEND_FAVORITES_PATH}/**`, {
          id: 12,
          description: "Desc",
          value: "111",
          categoryId: 1
        }).as("favorites");
        //Stub Kategorien
        cy.route("GET", BACKEND_CATEGORIES_PATH, [
          {
            id: 1,
            name: "KAT_1"
          },
          {
            id: 2,
            name: "KAT_2"
          }
        ]).as("categories");
        //Stub response (wird nicht ausgewertet)
        cy.route(
          "POST",
          `${BACKEND_FAVORITES_PATH}/**`,
          "fixture:favorites/get_favorite_01.json"
        ).as("submit");
        cy.visit(URL_BASE + "1");
        cy.hideMenu();
      });

      it("sollte bei Seitenaufrufen das Formular befüllen", () => {
        cy.wait("@favorites");
        cy.get("@favorites").then(function() {
          cy.get(VALUE_INPUT).should("have.value", "111.00");
          cy.get(DESCRIPTION_INPUT).should("have.value", "Desc");
          cy.wait("@categories");
          cy.get("@categories").then(function() {
            cy.getSelectedOption(CATEGORY_INPUT).should("contain", "KAT_1");
          });
        });
      });

      it("sollte die veränderten Werte absenden", () => {
        let id = 12;
        //Stub Fav
        cy.visit(`/favorites/${id}`);
        cy.wait("@categories");
        cy.get("@categories").then(function(xhr) {
          cy.wait("@favorites");
          let categories = xhr.response.body;
          let value = randomValue(0, 5000);
          let categoryId = categories[0].id;
          let categoryName = categories[0].name;
          cy.get(VALUE_INPUT)
            .clear()
            .type(value);
          cy.chooseSelectOption(CATEGORY_INPUT, categoryName);

          cy.get(SAVE_BUTTON).click();
          cy.wait("@submit");
          cy.get("@submit").then(function(xhr) {
            expect(xhr.status).to.eq(200);
            expect(xhr.method).to.eq("POST");
            expect(xhr.requestBody).to.have.property(
              PROPERTY_CATEGORY_ID,
              categoryId
            );
            expect(xhr.requestBody).to.have.property(
              PROPERTY_VALUE,
              -Math.abs(+value)
            );
          });
        });
      });

      it("sollte bei Updates nicht nach Vorschlägen suchen", () => {
        cy.server();
        //Fail wenn recommendation request
        cy.route({
          url: BACKEND_RECOMMENDATION_PATH,
          onRequest: () => {
            expect(true).to.equal(false);
          }
        });
        cy.wait("@categories");
        cy.get("@categories").then(function() {
          let description = randomDescription(15);

          cy.get(DESCRIPTION_INPUT).clear();
          cy.get(DESCRIPTION_INPUT).type(description);
          cy.get(DESCRIPTION_INPUT)
            .first()
            .click({ force: true });
          cy.get("#cb_description > .v-list").should("not.exist");
        });
      });

      it("sollte nach dem Speichern auf zur Liste zurückkehren", () => {
        let desc = randomDescription(20);
        cy.get(DESCRIPTION_INPUT).type(desc);
        cy.get(SAVE_BUTTON).click();
        cy.wait("@submit");
        cy.get("@submit").then(function() {
          cy.url().then(url => {
            expect(url).to.match(/favorites(\/)?$/);
          });
        });
      });
    });

    describe("favorites_delete", function() {
      before(() => {
        cy.resetDB();
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        cy.route("GET", BACKEND_CATEGORIES_PATH, [
          {
            id: 1,
            name: "KAT_1"
          },
          {
            id: 2,
            name: "KAT_2"
          }
        ]).as("categories");
      });

      it("sollte einen Löschbutton anzeigen", () => {
        cy.server();
        //Stub Fav
        cy.route("GET", `${BACKEND_FAVORITES_PATH}/1`, {
          id: 1,
          description: "Desc",
          value: "111",
          categoryId: 1
        }).as("favorites");
        cy.visit(URL_BASE + "1");
        cy.get(DELETE_BUTTON).should("be.visible");
      });

      it("sollte einen delete request senden", () => {
        let id = Math.floor(Math.random() * 10000) + 1;
        cy.server();
        //Stub Fav
        cy.route("GET", `${BACKEND_FAVORITES_PATH}/${id}`, {
          id: id,
          description: "Beschreibung_418",
          value: "111",
          categoryId: 1
        }).as("favorites");
        //Stub Answer
        cy.route("DELETE", `${BACKEND_FAVORITES_PATH}/${id}`, { count: 1 }).as(
          "favoritesDelete"
        );

        //stub ab hier verfügbar
        cy.visit(`/favorites/${id}`);
        cy.get(DELETE_BUTTON).click();
        cy.wait("@favoritesDelete");
        cy.get("@favoritesDelete").then(function() {
          //fails the test if no call is made
        });
      });
      it("sollte nach dem Löschen auf zur Liste zurückkehren", () => {
        let id = Math.floor(Math.random() * 10000) + 1;
        cy.server();
        //Stub Fav
        cy.route("GET", `${BACKEND_FAVORITES_PATH}/${id}`, {
          id: id,
          description: "Beschreibung_420",
          value: "111",
          categoryId: 1
        }).as("favorites");
        //Stub Answer
        cy.route("DELETE", `${BACKEND_FAVORITES_PATH}/${id}`, { count: 1 }).as(
          "favoritesDelete"
        );

        //stub ab hier verfügbar
        cy.visit(`/favorites/${id}`);
        cy.get(DELETE_BUTTON).click();
        cy.wait("@favoritesDelete");
        cy.get("@favoritesDelete").then(function() {
          cy.url().then(url => {
            expect(url).to.match(/favorites(\/)?$/);
          });
        });
      });
    });
  });

  describe("e2e tests", function() {
    describe("favorites_create_e2e", function() {
      before(() => {
        cy.resetDB();
        cy.insertFixture(`${DB_FIXTURE_BASE_PATH}/create/`);
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        cy.route("POST", `${BACKEND_FAVORITES_PATH}/**`).as("submit");
        cy.visit(URL_BASE + URL_NEW);
        cy.hideMenu();
      });

      it("sollte im Backend die Werte aus dem Formular eintragen", () => {
        let description = randomDescription(50);
        let value = randomNegativeValue(1, 5000);
        let categoryId = 4;
        let categoryName = "Kat_4";

        cy.get(DESCRIPTION_INPUT).clear();
        cy.get(DESCRIPTION_INPUT).type(description);
        cy.get(VALUE_INPUT).type(value);
        cy.chooseSelectOption(CATEGORY_INPUT, categoryName);
        cy.get(SAVE_BUTTON).click();
        cy.wait("@submit");
        cy.get("@submit").then(function(xhr) {
          expect(xhr.status).to.eq(200);
          expect(xhr.method).to.eq("POST");
          expect(xhr.responseBody).to.have.property(
            PROPERTY_CATEGORY_ID,
            categoryId
          );
          expect(xhr.responseBody).to.have.property(
            PROPERTY_DESCRIPTION,
            description
          );
          expect(xhr.responseBody).to.have.property(PROPERTY_VALUE, +value);
        });
      });

      it("sollte Werte immer mit negativem Vorzeichen eintragen", () => {
        let description = "Updated";
        let value = randomValue(1, 9000);
        let categoryId = Math.floor(Math.random() * 19) + 1;
        let categoryName = `Kat_${categoryId}`;

        cy.get(DESCRIPTION_INPUT).clear();
        cy.get(DESCRIPTION_INPUT).type(description);
        cy.get(VALUE_INPUT).type(value);
        cy.chooseSelectOption(CATEGORY_INPUT, categoryName);
        cy.get(SAVE_BUTTON).click();
        cy.wait("@submit");
        cy.get("@submit").then(function(xhr) {
          expect(xhr.status).to.eq(200);
          expect(xhr.method).to.eq("POST");
          //checke hier response
          expect(xhr.responseBody).to.have.property(
            PROPERTY_CATEGORY_ID,
            categoryId
          );
          expect(xhr.responseBody).to.have.property(
            PROPERTY_DESCRIPTION,
            description
          );
          expect(xhr.responseBody).to.have.property(PROPERTY_VALUE, -value);
        });
      });

      it("sollte bei Klick auf die Vorschlagsliste das Formular mit Funddaten befüllen", () => {
        cy.insertFixture(`${DB_FIXTURE_BASE_PATH}/recommendations/`);
        let description = "T";

        cy.get(DESCRIPTION_INPUT).clear();
        cy.get(DESCRIPTION_INPUT).type(description);
        cy.chooseSelectOption(DESCRIPTION_INPUT, "TF_UseRec");

        //assert data gets loaded from recommendation funds fixture fixtures/favorites/funds_recommend
        cy.get(VALUE_INPUT).should("have.value", "23.50");
        cy.get(DESCRIPTION_INPUT).should("have.value", "TF_UseRec");
        cy.getSelectedOption(CATEGORY_INPUT).should("contain", "Kat_2");
      });
    });

    describe("favorites_update_e2e", function() {
      before(() => {
        cy.resetDB();
        cy.insertFixture(`${DB_FIXTURE_BASE_PATH}/update/`);
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        cy.route("POST", `${BACKEND_FAVORITES_PATH}/**`).as("submit");
        cy.hideMenu();
      });

      it("sollte die eingegebenen Werte an den Server senden", () => {
        let id = Math.floor(Math.random() * 100) + 1; //1..100
        let new_value = randomValue(1, 1000); //1.00..1000.00
        let new_cat_id = Math.ceil(Math.random() * 20); //1..20

        //Spy auf Abfrage der alten Werte
        cy.server();
        cy.route("GET", `${BACKEND_FAVORITES_PATH}/${id}`).as("favorites");
        cy.visit(`/favorites/${id}`);
        cy.wait("@favorites");
        cy.get("@favorites").then(function() {
          cy.get(VALUE_INPUT)
            .clear()
            .type(new_value);
          cy.chooseSelectOption(CATEGORY_INPUT, `Kat_${new_cat_id}`);
          cy.get(SAVE_BUTTON).click();
          cy.wait("@submit");
          cy.get("@submit").then(function(xhr) {
            expect(xhr.status).to.eq(200);
            expect(xhr.method).to.eq("POST");
            //prüfe response
            expect(xhr.responseBody).to.have.property(PROPERTY_ID, id);
            expect(xhr.responseBody).to.have.property(
              PROPERTY_CATEGORY_ID,
              new_cat_id
            );
            expect(xhr.responseBody).to.have.property(
              PROPERTY_VALUE,
              -Math.abs(new_value)
            );
          });
        });
      });

      it("sollte die neuen Werte laden", () => {
        let id = Math.floor(Math.random() * 100) + 1; //1..100
        let new_value = randomValue(1, 1000); //1.00..1000.00
        let new_cat_id = Math.floor(Math.random() * 19) + 1; //1..20
        cy.visit(`/favorites/${id}`);
        cy.get(VALUE_INPUT)
          .clear()
          .type(new_value);
        cy.chooseSelectOption(CATEGORY_INPUT, `Kat_${new_cat_id}`);
        cy.get(SAVE_BUTTON).click();
        cy.wait("@submit");

        cy.server();
        //Stub Fund Daten
        cy.route("GET", BACKEND_FAVORITES_PATH, {
          id: 1,
          description: "Desc",
          value: "111",
          categoryId: 1
        }).as("favorites");
        //act
        cy.visit(`/favorites/${id}`);
        cy.get(VALUE_INPUT).should("have.value", "" + new_value);
        cy.getSelectedOption(CATEGORY_INPUT).should(
          "contain",
          `Kat_${new_cat_id}`
        );
      });
    });

    describe("favorites_delete_e2e", function() {
      before(() => {
        cy.resetDB();
        cy.insertFixture(`${DB_FIXTURE_BASE_PATH}/delete/`);
        cy.visit("/");
        cy.login();
      });

      beforeEach(() => {
        cy.server();
        //Spy auf Response, aber keine eigene Response angegeben => weiterleitung an server
        cy.route("DELETE", `${BACKEND_FAVORITES_PATH}/**`).as(
          "favoritesDelete"
        );
      });

      it("sollte auf einen delete request eine antwort erhalten", () => {
        let id = Math.floor(Math.random() * 150) + 1;
        cy.visit(`/favorites/${id}`);
        cy.get(DELETE_BUTTON).click();
        cy.wait("@favoritesDelete");
        cy.get("@favoritesDelete").then(function(xhr) {
          expect(xhr.status).to.eq(200);
          expect(xhr.responseBody).to.have.property("count", 1);
        });
      });
    });
  });
});

function randomValue(min, max) {
  return (
    "" +
    (Math.floor(Math.random() * (max - 1)) + min + Math.random()).toFixed(2)
  );
}

function randomNegativeValue(min, max) {
  return "" + -randomValue(min, max);
}

function randomDescription(length) {
  if (!length) {
    length = 50;
  }
  let ret = "";
  let zeichen =
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 äöüÄÖÜß!"§$%&/()=+';

  for (let i = 0; i < length; i++) {
    ret += zeichen.charAt(Math.floor(Math.random() * zeichen.length));
  }
  return ret;
}
