describe("Seite für User-Login", function() {
  beforeEach(function() {
    cy.visit("/login"); // always visit root
  });

  const selector = `label`;
  const loginbuttonSelector = `#btn_login`;
  const emailTextFieldSelector = `#tf_email`;
  const passwordTextFieldSelector = `#tf_password`;

  const emailDataFailSet = [
    "admin@test.com",
    " ",
    "admin@.de",
    "admin@test",
    "admin.de",
    "@test.de",
    "@.de",
    "admin@de",
    "admin @test.de",
    "admin@test. de",
    "admin@test.de"
  ];

  const passwordDataFailSet = [" ", "tet", "123", "SELECT *"];

  it("sollte ein Login-Button und Zwei Textfelder haben", () => {
    cy.contains(selector, `Passwort`).should("be.visible");
    cy.contains(selector, `E-Mail`).should("be.visible");
    cy.get(loginbuttonSelector).should("be.visible");
  });

  it("sollte bei falsche E-Mail Formatierung eine Fehleranzeige präsentieren", () => {
    const emailLength = emailDataFailSet.length;
    for (let i = 1; i < emailLength; i++) {
      cy.get(emailTextFieldSelector).type(emailDataFailSet[i]);
      cy.contains(`Die E-mail muss gültig sein!`).should("exist"); // or check for .v-messages__message
      cy.get(emailTextFieldSelector).clear();
    }
  });

  it("sollte den User beim falschen Einloggen nicht weiterleiten", function() {
    const emailLength = emailDataFailSet.length;
    const pwdLength = passwordDataFailSet.length;
    for (let i = 0; i < emailLength; i++) {
      for (let j = 0; i < pwdLength; i++) {
        cy.get(emailTextFieldSelector).type(emailDataFailSet[i]);
        cy.get(passwordTextFieldSelector).type(passwordDataFailSet[j]);
        cy.get(loginbuttonSelector).click();
        cy.contains(`Benutzername oder Kennwort falsch!`).should("exist"); // zeige Error-Feedback
        cy.url().should("include", "/#/login"); // Seite bleibt
        cy.get(emailTextFieldSelector).clear();
        cy.get(passwordTextFieldSelector).clear();
      }
    }
  });

  function manualLogin() {
    cy.get(emailTextFieldSelector)
      .type("admin@test.de")
      .should("have.value", "admin@test.de");
    cy.get(passwordTextFieldSelector)
      .type("test")
      .should("have.value", "test");
    cy.get(loginbuttonSelector).click();
    cy.url().should("eq", "http://localhost:8080/#/");
  }

  it("sollte den User beim richtigen Einloggen korrekt weiterleiten", function() {
    manualLogin();
    cy.url().should("eq", "http://localhost:8080/#/");
    cy.logout();
  });

  const navSelectors = [
    `Geldströme`,
    `Regelmäßige Geldströme`,
    `Planziele`,
    `Kategorien`,
    `Favoriten`,
    `Import/Export (Geldströme)`
  ];

  const availableURLPaths = [
    `funds`,
    `regularFunds`,
    `planningTargets`,
    `categories`,
    `favorites`,
    `importexportfunds`
  ];

  it("der eingeloggte User kann alle verfügbaren Seiten besuchen", function() {
    manualLogin();
    cy.url().should("eq", "http://localhost:8080/#/");
    for (let i = 0; i < 6; i++) {
      cy.contains(navSelectors[i]).click();
      cy.url().should(
        "include",
        `http://localhost:8080/#/${availableURLPaths[i]}`
      );
    }
    cy.logout();
  });

  it("der User kann sich auf jeder Seite ausloggen und hat dann keinen Zugriff mehr auf die Seite", function() {
    // Testschritte: Einloggen > Seite auswählen > Ausloggen > Seite erneut auswählen > "stay"
    for (let i = 0; i < 6; i++) {
      manualLogin();
      cy.contains(navSelectors[i]).click();
      cy.url().should(
        "include",
        `http://localhost:8080/#/${availableURLPaths[i]}`
      );
      cy.get(
        ":nth-child(8) > .v-list__tile > .v-list__tile__content > .v-list__tile__title"
      ).click();
      cy.url().should("eq", "http://localhost:8080/#/login");
      cy.contains(navSelectors[i]).click();
      cy.url().should("eq", "http://localhost:8080/#/login");
    }
  });
});
