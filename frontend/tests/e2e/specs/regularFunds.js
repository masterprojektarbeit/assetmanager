describe("Seite für regelmäßige Verbuchungen", function() {
  before(function() {
    cy.resetDB();
    cy.insertFixture("/fixtures/dataWithoutUser/");
    cy.login();
  });
  beforeEach(function() {
    cy.visit("http://localhost:8080/#/regularFunds");
  });

  let fundTypeString = [
    `Jährliche Geldströme`,
    `Halbjährliche Geldströme`,
    `Vierteljährliche Geldströme`,
    `Monatliche Geldströme`,
    `Wöchentliche Geldströme`,
    `Tägliche Geldströme`
  ];

  const buttonNewRegularFund = `#btn_newRegularFund`;

  const inputFirstBooking = `#tf_firstBookingDate`;
  const inputLastBooking = `#tf_lastBookingDate`;
  const inputDescription = `#tf_description`;
  const inputValue = `#tf_value`;

  const buttonSave = `#btn_save`;

  it("Überprüfe, ob man eine regelmäßige Einnahmequelle und Ausgabequelle erstellen kann", () => {
    // Simuliere Einnahmeverbuchung von x Euro und -x Euro
    // x wird randomisiert [1;1000]
    const x = Math.floor(Math.random() * 1000 + 1);
    cy.get(buttonNewRegularFund).click();
    createFund(x);

    // `+` Button toggle income
    cy.get(`#btn_income`).click();
    cy.get(buttonSave).click();
    cy.url().should("eq", "http://localhost:8080/#/regularfunds");

    cy.contains(`Das ist eine Beschreibung`);
    cy.contains(`${x}.00`).should(`exist`);

    // Simuliere Einnahmeverbuchung von -23 Euro
    cy.get(buttonNewRegularFund).click();
    createFund(x);

    // `-` Button toggle expenses
    cy.get(`#btn_expense`).click();
    cy.get(buttonSave).click();
    cy.url().should("eq", "http://localhost:8080/#/regularfunds");

    cy.contains(`Das ist eine Beschreibung`);
    cy.contains(`-${x}.00`).should(`exist`);
  });

  it("Überprüfe, ob der Nutzer eine Verbuchung mit unausgefüllten Pflichtfeldern erstellen kann", () => {
    // click on the "+" symbol
    cy.get(buttonNewRegularFund).click();
    cy.url().should("eq", "http://localhost:8080/#/regularfunds/new");
    // check that both inputs are visible
    cy.get(inputFirstBooking).should("be.visible");
    cy.get(inputLastBooking).should("be.visible");

    const description = `Die Beschreibung muss angegeben werden`;
    const frequency = `Es muss eine Frequenz angegeben werden.`;
    const value = `Der Wert muss eine Zahl sein.`;

    // click on the "SPEICHERN" Button to check if it is submitable
    cy.get(buttonSave).click();
    cy.contains(description).should("be.visible");
    cy.contains(frequency).should("be.visible");
    cy.contains(value).should("be.visible");
    cy.url().should("eq", "http://localhost:8080/#/regularfunds/new");

    // Types a description
    cy.get(inputDescription).type(`Das ist eine Beschreibung`);
    cy.get(buttonSave).should("be.disabled");
    cy.contains(description).should("not.be.visible");
    cy.contains(frequency).should("be.visible");
    cy.contains(value).should("be.visible");
    cy.url().should("eq", "http://localhost:8080/#/regularfunds/new");

    // Open "Frequenz*" Dropdown and select "Täglich"
    cy.get(
      `#regularfundCRUD > div > div > form > div:nth-child(4) > div`
    ).click();
    cy.get(
      `#app > div.v-menu__content.theme--dark.menuable__content__active > div > div > div:nth-child(1) > a > div > div`
    ).click();
    cy.get(buttonSave).should("be.disabled");
    cy.contains(description).should("not.be.visible");
    cy.contains(frequency).should("be.visible");
    cy.contains(value).should("be.visible");
    cy.url().should("eq", "http://localhost:8080/#/regularfunds/new");

    // type value in
    cy.get(inputValue).type(`23`);
    cy.get(buttonSave).should("not.be.disabled");
    cy.get(buttonSave).click();
    cy.contains(description).should("not.be.visible");
    cy.contains(frequency).should("not.be.visible");
    cy.contains(value).should("not.be.visible");
    cy.url().should("eq", "http://localhost:8080/#/regularfunds");
  });

  it("Überprüfe, ob man eine erstellte Verbuchung bearbeiten kann", () => {
    const x = Math.floor(Math.random() * 1000 + 1);
    const id = Math.floor(Math.random() * 100 + 1);
    cy.get(buttonNewRegularFund).click();
    createFund(x, "" + id);
    cy.get(buttonSave).click();
    cy.url().should("eq", "http://localhost:8080/#/regularfunds");

    // Checke, dass der fund erstellt wurde
    cy.contains(`Das ist eine Beschreibung mit id ${id}`).should("exist");
    cy.contains(`${x}`).should("exist");

    // select newly created fund and click on it
    cy.contains(`Das ist eine Beschreibung mit id ${id}`).click();

    const modifiedDescriptionText = `Das ist eine bearbeitete Beschreibung mit id ${id}`;
    // Bearbeite den Fund
    cy.get(inputDescription).clear();
    cy.get(inputValue).clear();
    cy.get(inputDescription).type(modifiedDescriptionText);
    cy.get(inputValue).type(`${x + x}`);
    selectFrequenz(4);
    cy.get(buttonSave).click();

    // Checke, dass der fund erstellt wurde
    cy.contains(modifiedDescriptionText).should("exist");
    cy.contains(`${x + x}`).should("exist");
  });

  it("Überprüfe, ob man eine Verbuchung mehrmals triggern kann", () => {
    cy.get(buttonNewRegularFund).click();
    createFund();

    // click on the "SPEICHERN" Button multiple times
    cy.get(buttonSave).click();
    cy.get(buttonSave).should(`not.exist`);
    cy.url().should("eq", "http://localhost:8080/#/regularfunds");
  });

  it("Überprüfe, ob man die Beschreibung bei maximal 255 Zeichen gecappt ist", () => {
    cy.get(buttonNewRegularFund).click();
    createFund();
    cy.get(inputDescription).clear();

    // Schreibe 240 Zeichen in das Beschreibungsfeld
    for (let i = 0; i < 12; i++) {
      cy.get(inputDescription).type(`Das sind 20 Zeichen!`);
      cy.get(buttonSave).should("not.be.disabled");
    }

    // nochmal 16 Zeichen dazu
    cy.get(inputDescription).type(`aaaaaaaaaaaaaaaa`);
    cy.get(buttonSave).should("be.disabled");
  });

  it("Überprüfe, ob die erstellte regelmäßige Verbuchung auch in der Übersichtsanzeige auftaucht", () => {
    const x = Math.floor(Math.random() * 1000 + 1);
    const id = Math.floor(Math.random() * 100 + 1);

    cy.get(buttonNewRegularFund).click();
    // `+` Button für income
    cy.get(`#btn_income`).click();
    createFund(x, id);

    cy.get(buttonSave).click();
    cy.contains(`Das ist eine Beschreibung mit id ${id}`).should(`exist`);
    cy.contains(`${x}`).should(`exist`);
  });

  it("Überprüfe, ob die `Rows per page`-Anzeige funktioniert", () => {
    // soll initial nur 5 verbuchungen anzeigen
    cy.get(
      `#funds > section > div.layout.column > div > div > div:nth-child(3) > div > div > div.v-table__overflow > table > tbody`
    )
      .find("tr")
      .should("be.have.length.lte", 5);

    // show all funds
    cy.get(
      `#funds > section > div.layout.column > div > div > div:nth-child(3) > div > div > div.v-datatable.v-table.v-datatable--select-all.theme--dark > div > div.v-datatable__actions__select > div > div > div > div.v-select__slot`
    ).click();
    cy.get(
      `#app > div.v-menu__content.theme--dark.v-menu__content--auto.menuable__content__active > div > div > div:nth-child(4) > a > div > div`
    ).click();

    cy.get(
      `#funds > section > div.layout.column > div > div > div:nth-child(3) > div > div > div.v-table__overflow > table > tbody`
    )
      .find("tr")
      .should("be.have.length.greaterThan", 5); // von allen erstellten Funds in dieser Testsuite
  });

  let selectFrequenz = function(i) {
    cy.get(
      `#regularfundCRUD > div > div > form > div:nth-child(4) > div`
    ).click();
    cy.get(
      `#app > div.v-menu__content.theme--dark.menuable__content__active > div > div > div:nth-child(${i}) > a > div > div`
    ).click();
  };

  let selectCategory = function(i) {
    cy.get(
      `#regularfundCRUD > div > div > form > div:nth-child(3) > div`
    ).click();
    cy.get(
      `#app > div.v-menu__content.theme--dark.menuable__content__active > div > div > div:nth-child(${i}) > a > div > div`
    ).click();
  };

  let createFund = function(value = 23, id = "") {
    // Types a description
    let description = `Das ist eine Beschreibung`;
    if (id != "") {
      description += " mit id " + id;
    }
    cy.get(inputDescription).type(description);
    // Open "Frequenz*" Dropdown and select "Monatlich"
    selectFrequenz(3);
    selectCategory(1);
    // Enters the number 23
    cy.get(inputValue).clear();
    cy.get(inputValue).type(`${value}`);
  };
});
