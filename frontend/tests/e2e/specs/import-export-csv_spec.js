/// <reference types="Cypress"/>
const moment = require("moment");
const IMPORT_BUTTON = "#btn_import";
const EXPORT_BUTTON = "#btn_export";
const GENERATED_DOWNLOAD_BTN = "#btn_hidden_download";
const IMPORT_CORRECT_COUNT_ELEMENT = "#label_Import_ImportedCount";
const IMPORT_DISCARDED_COUNT_ELEMENT = "#label_Import_DiscardedCount";
const IMPORT_ERROR_COUNT_ELEMENT = "#label_Import_ErrorCount";
const INPUT_IMPORT_FILE = "#input_importFile";
const INPUT_DATE_FROM = "#tf_dateFrom";
const INPUT_DATE_TO = "#tf_dateTo";
const CSV_FIRST_LINE = "id;value;description;date;categoryId;favoriteId";
const FIXTURE_1 = "uploadCSV.csv";
const FIXTURE_2 = "uploadCSV2.csv";
const FIXTURE_ERROR = "uploadCSVError.csv";
const FIXTURE_PART_ERROR = "uploadCSVPartlyError.csv";

describe("Seite für Import-Export von CSV", function() {
  before(function() {
    cy.resetDB();
    cy.login();
  });
  beforeEach(function() {
    cy.visit("/importexportfunds");
  });

  it("sollte erreichbar sein", function() {
    cy.get(EXPORT_BUTTON).should("be.visible");
  });

  it("sollte doppelte Imports verhindern", function() {
    cy.upload_file(FIXTURE_2, "text/csv", INPUT_IMPORT_FILE);
    cy.get(IMPORT_BUTTON).click();
    cy.get(IMPORT_CORRECT_COUNT_ELEMENT).contains(/.*2.*/);
    cy.get(IMPORT_DISCARDED_COUNT_ELEMENT).contains(/.*0.*/);
    cy.upload_file(FIXTURE_2, "text/csv", INPUT_IMPORT_FILE);
    cy.get(IMPORT_BUTTON).click();
    cy.get(IMPORT_CORRECT_COUNT_ELEMENT).contains(/.*0.*/);
    cy.get(IMPORT_DISCARDED_COUNT_ELEMENT).contains(/.*2.*/);
  });

  it("sollte das Hochladen von Dateien ermöglichen", function() {
    cy.upload_file(FIXTURE_1, "text/csv", INPUT_IMPORT_FILE);
    cy.get(IMPORT_BUTTON).click();
    cy.get(IMPORT_CORRECT_COUNT_ELEMENT).contains(/.*2.*/);
    cy.get(IMPORT_DISCARDED_COUNT_ELEMENT).contains(/.*0.*/);
  });

  it("sollte bei fehlerhaften Dateien Fehler anzeigen", function() {
    cy.upload_file(FIXTURE_ERROR, "text/csv", INPUT_IMPORT_FILE);
    cy.get(IMPORT_BUTTON).click();
    cy.get(IMPORT_CORRECT_COUNT_ELEMENT).contains(/.*0.*/);
    cy.get(IMPORT_DISCARDED_COUNT_ELEMENT).contains(/.*0.*/);
    cy.get(IMPORT_ERROR_COUNT_ELEMENT).contains(/.*1.*/);
  });

  it("sollte bei fehlerhaften Dateien korrekte Zeilen importieren", function() {
    cy.upload_file(FIXTURE_PART_ERROR, "text/csv", INPUT_IMPORT_FILE);
    cy.get(IMPORT_BUTTON).click();
    cy.get(IMPORT_CORRECT_COUNT_ELEMENT).contains(/.*2.*/);
    cy.get(IMPORT_DISCARDED_COUNT_ELEMENT).contains(/.*0.*/);
    cy.get(IMPORT_ERROR_COUNT_ELEMENT).contains(/.*1.*/);
  });

  it("sollte das Herunterladen von Dateien ermöglichen", function() {
    cy.get(EXPORT_BUTTON).click();
    cy.get(GENERATED_DOWNLOAD_BTN).then(btn => {
      cy.downloadLocalBlob(btn.prop("href")).then(csvContent => {
        expect(csvContent).to.contain(CSV_FIRST_LINE);
      });
    });
  });

  it("sollte das Herunterladen von Dateien mit angegebenem Zeitintervall ermöglichen", function() {
    const yesterday = moment()
      .subtract(1, "days")
      .format("YYYY-MM-DD");
    const dayBeforeYesterday = moment()
      .subtract(2, "days")
      .format("YYYY-MM-DD");
    cy.get(INPUT_DATE_FROM).type(dayBeforeYesterday);
    cy.get(INPUT_DATE_TO).type(yesterday);
    cy.get(EXPORT_BUTTON).click();
    cy.get(GENERATED_DOWNLOAD_BTN).then(btn => {
      cy.downloadLocalBlob(btn.prop("href")).then(csvContent => {
        expect(csvContent).to.contain(CSV_FIRST_LINE);
      });
    });
  });

  it("sollte das Herunterladen von Dateien mit ungültig angegebenem Zeitintervall unterbinden", function() {
    const tomorrow = moment()
      .add(1, "days")
      .format("YYYY-MM-DD");
    const dayAfterTomorrow = moment()
      .add(2, "days")
      .format("YYYY-MM-DD");
    cy.get(INPUT_DATE_FROM).type(dayAfterTomorrow);
    cy.get(INPUT_DATE_FROM).then(dateFrom => {
      expect(dateFrom.prop("value")).to.not.be.eq(dayAfterTomorrow);
    });
    cy.get(INPUT_DATE_TO).type(tomorrow);
    cy.get(INPUT_DATE_TO).then(dateTo => {
      expect(dateTo.prop("value")).to.not.be.eq(tomorrow);
    });
  });
});
