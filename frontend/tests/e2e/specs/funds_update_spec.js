/// <reference types="Cypress"/>

const moment = require("moment");
const URL_BASE = "/funds";
const DATE = "1";
const DESCRIPTION = "2";
const CATEGORY = "3";
const VALUE = "5";
const DATE_INPUT = "#tf_date";
const TIME_INPUT = "#tf_time";
const DESCRIPTION_INPUT = "#cb_description";
const CATEGORY_SELECT =
  "#fundsCRUD > div > div > form > div:nth-child(3) > div";
const CATEGORY_DELETE =
  "#fundsCRUD > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(3)";
const EXPENSE_BUTTON = "#btn_expense";
const INCOME_BUTTON = "#btn_income";
const VALUE_INPUT = "#tf_value";
const SAVE_BUTTON = "#btn_save";
const ERROR_MSG = ".v-messages__message";

const today = moment();
const tomorrow = moment().add(1, "days");
const tomorrowValue = tomorrow.format("YYYY-MM-DD");
const time = moment().add(1, "hours");
const timeValue = time.format("HH:mm");

describe("Einmalige Geldströme", function() {
  before(function() {
    cy.resetDB();
    cy.login();
  });
  beforeEach(function() {
    visitOrReload();
  });
  describe("aktualisieren", function() {
    describe("Ausgaben", function() {
      var id = 1;
      var value = -15.0;
      var description = "Ausgabe";
      beforeEach(function() {
        cy.resetDB();
        createFund({
          id: id,
          value: value,
          description: description,
          date: today
        });
        visitOrReload();
      });
      it("Datum", function() {
        cy.get(getFundInTable(id)).click();
        setDate(tomorrowValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should("eq", tomorrow.format("DD.MM.YYYY"));
      });
      it("ohne Datum", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(DATE_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss ein Datum angegeben werden.");
      });
      it("Uhrzeit", function() {
        cy.get(getFundInTable(id)).click();
        setTime(timeValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundInTable(id)).click();
        cy.get(TIME_INPUT)
          .invoke("val")
          .should("eq", timeValue);
      });
      it("ohne Uhrzeit", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(TIME_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss eine Uhrzeit angegeben werden.");
      });
      it("Beschreibung", function() {
        var changeDescription = "Beschreibung geändert";
        cy.get(getFundInTable(id)).click();
        setDescription(changeDescription);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DESCRIPTION))
          .invoke("text")
          .should("eq", "Beschreibung geändert");
      });
      it("ohne Beschreibung", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(DESCRIPTION_INPUT)
          .click({ force: true })
          .clear({ force: true })
          .type("{enter}", { force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Die Beschreibung muss angegeben werden");
      });
      it("Kategorie einfügen", function() {
        var categoryId = 1;
        var categoryName = "Kategorie Ausgaben";
        createCategory({
          name: categoryName
        });
        cy.get(getFundInTable(id)).click();
        setCategory(categoryId);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", categoryName);
      });
      it("Kategorie ändern", function() {
        var categoryId = 1;
        var categoryName = "Kategorie ändern";
        createCategory({
          name: categoryName
        });
        cy.get(getFundInTable(id)).click();
        setCategory(categoryId);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", categoryName);
      });
      it("ohne Kategorie", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(CATEGORY_DELETE).click({ force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", "");
      });
      it("Betrag", function() {
        var changeValue = "10.00";
        cy.get(getFundInTable(id)).click();
        setValue(changeValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, VALUE))
          .invoke("text")
          .should("eq", "-" + changeValue);
      });
      it("ohne Betrag", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(VALUE_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Ausgabe zu Einnahme", function() {
        var value = "15.00";
        cy.get(getFundInTable(id)).click();
        cy.get(INCOME_BUTTON).click({ force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, VALUE))
          .invoke("text")
          .should("eq", value);
      });
    });
    describe("Einnahmen", function() {
      var id = 1;
      var value = 15.0;
      var description = "Einnahme";
      beforeEach(function() {
        cy.resetDB();
        createFund({
          id: id,
          value: value,
          description: description,
          date: today
        });
        visitOrReload();
      });

      it("Datum", function() {
        cy.get(getFundInTable(id)).click();
        setDate(tomorrowValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should("eq", tomorrow.format("DD.MM.YYYY"));
      });
      it("ohne Datum", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(DATE_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss ein Datum angegeben werden.");
      });
      it("Uhrzeit", function() {
        cy.get(getFundInTable(id)).click();
        setTime(timeValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundInTable(id)).click();
        cy.get(TIME_INPUT)
          .invoke("val")
          .should("eq", timeValue);
      });
      it("ohne Uhrzeit", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(TIME_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss eine Uhrzeit angegeben werden.");
      });
      it("Beschreibung", function() {
        var changeDescription = "Beschreibung geändert";
        cy.get(getFundInTable(id)).click();
        setDescription(changeDescription);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DESCRIPTION))
          .invoke("text")
          .should("eq", "Beschreibung geändert");
      });
      it("ohne Beschreibung", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(DESCRIPTION_INPUT)
          .click({ force: true })
          .clear({ force: true })
          .type("{enter}", { force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Die Beschreibung muss angegeben werden");
      });
      it("Kategorie einfügen", function() {
        var categoryId = 1;
        var categoryName = "Kategorie Einnahmen";
        createCategory({
          name: categoryName
        });
        cy.get(getFundInTable(id)).click();
        setCategory(categoryId);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", categoryName);
      });
      it("Kategorie ändern", function() {
        var categoryId = 1;
        var categoryName = "Kategorie ändern";
        createCategory({
          name: categoryName
        });
        cy.get(getFundInTable(id)).click();
        setCategory(categoryId);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", categoryName);
      });
      it("ohne Kategorie", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(CATEGORY_DELETE).click({ force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", "");
      });
      it("Betrag", function() {
        var changeValue = "10.00";
        cy.get(getFundInTable(id)).click();
        setValue(changeValue);
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, VALUE))
          .invoke("text")
          .should("eq", changeValue);
      });
      it("ohne Betrag", function() {
        cy.get(getFundInTable(id)).click();
        cy.get(VALUE_INPUT)
          .click({ force: true })
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Einnahme zu Ausgabe", function() {
        var value = "-15.00";
        cy.get(getFundInTable(id)).click();
        cy.get(EXPENSE_BUTTON).click({ force: true });
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, VALUE))
          .invoke("text")
          .should("eq", value);
      });
    });
  });
});

function visitOrReload() {
  cy.url().then(url => {
    if (url.endsWith(URL_BASE)) {
      cy.reload(true);
    } else {
      cy.visit(URL_BASE);
    }
  });
}

function setDate(date) {
  cy.get(DATE_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(date, { force: true });
}
function setTime(time) {
  cy.get(TIME_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(time, { force: true });
}
function setDescription(description) {
  cy.get(DESCRIPTION_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(description, { force: true });
}
function setValue(value) {
  cy.get(VALUE_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(value, { force: true });
}
function getFundInTable(i) {
  return `tbody > :nth-child(${i})`;
}
function getFundDataInTable(i, data) {
  return `tbody > :nth-child(${i}) > :nth-child(${data})`;
}
function setCategory(categoryId) {
  cy.get(CATEGORY_SELECT).click();
  cy.get(
    `#app > div.v-menu__content.theme--dark.menuable__content__active > div > div > div:nth-child(${categoryId}) > a > div > div`
  ).click();
}
function createFund(fund) {
  return cy.request("POST", "http://localhost:3000/api/funds", fund);
}
function createCategory(category) {
  return cy.request("POST", "http://localhost:3000/api/categories", category);
}
