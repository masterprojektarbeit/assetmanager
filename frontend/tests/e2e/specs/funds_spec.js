/// <reference types="Cypress"/>

const moment = require("moment");
const URL_BASE = "/funds";
const FAVORITE = "4";
const DESCRIPTION_INPUT = "#cb_description";
const MONTHLY_INCOME = ".offset-xs1 > :nth-child(2) > .text-xs-right";
const MONTHLY_EXPENSES =
  ".column > :nth-child(2) > :nth-child(2) > .text-xs-right";
const MONTHLY_BALANCE = ":nth-child(3) > :nth-child(2) > .text-xs-right";
const INCOME_BUTTON = "#btn_income";
const VALUE_INPUT = "#tf_value";
const DELETE_BUTTON = "#btn_delete";
const SAVE_BUTTON = "#btn_save";
const NEW_BUTTON = "#btn_newFund";
const SPLIT_BUTTON = "#btn_split";
const FAVORITE_BUTTON = "#btn_favorite";

const today = moment();

describe("Einmalige Geldströme", function() {
  before(function() {
    cy.resetDB();
    cy.login();
  });
  beforeEach(function() {
    visitOrReload();
  });

  describe("löschen", function() {
    it("Ausgaben", function() {
      cy.resetDB();
      var id = 1;
      var value = -15.0;
      var description = "Ausgaben";
      createFund({
        id: id,
        value: value,
        description: description,
        date: today
      });
      visitOrReload();
      cy.get(getFundInTable(id)).click();
      cy.get(DELETE_BUTTON).click({ force: true });
      cy.get(getFundInTable(id)).should("not.exist");
    });
    it("Einnahmen", function() {
      cy.resetDB();
      var id = 1;
      var value = 15.0;
      var description = "Einnahmen";
      createFund({
        id: id,
        value: value,
        description: description,
        date: today
      });
      visitOrReload();
      cy.get(getFundInTable(id)).click();
      cy.get(DELETE_BUTTON).click({ force: true });
      cy.get(getFundInTable(id)).should("not.exist");
    });
  });

  describe("Einnahmen", function() {
    it("anlegen: Button Favorit deaktiviert", function() {
      var expense = "12.50";
      cy.get(NEW_BUTTON).click();
      cy.get(INCOME_BUTTON).click({ force: true });
      setValue(expense);
      cy.get(FAVORITE_BUTTON).should("be.disabled");
    });
    it("Button Aufteilen und Favorit deaktiviert", function() {
      cy.resetDB();
      var id = 1;
      var value = 15.0;
      var description = "Einnahmen";
      createFund({
        id: id,
        value: value,
        description: description,
        date: today
      });
      visitOrReload();
      cy.get(getFundInTable(id)).click();
      cy.get(SPLIT_BUTTON).should("be.disabled");
      cy.get(FAVORITE_BUTTON).should("be.disabled");
    });
  });

  describe("Ausgabe als Favorit speichern", function() {
    it("beim Anlegen", function() {
      cy.resetDB();
      var description = "Beschreibung Ausgaben";
      var expense = "12.50";
      var id = 1;
      cy.get(NEW_BUTTON).click();
      setDescription(description);
      cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
      setValue(expense);
      cy.get(FAVORITE_BUTTON).click({ force: true });
      cy.get(SAVE_BUTTON).click({ force: true });
      visitOrReload();
      cy.get(getFundDataInTable(id, FAVORITE))
        .invoke("text")
        .should("eq", "star");
    });
    it("beim Aktualisieren", function() {
      cy.resetDB();
      var id = 1;
      var value = -15.0;
      var description = "Beschreibung Ausgabe";
      createFund({
        id: id,
        value: value,
        description: description,
        date: today
      });
      visitOrReload();
      cy.get(getFundInTable(id)).click();
      cy.get(FAVORITE_BUTTON).click({ force: true });
      cy.get(SAVE_BUTTON).click({ force: true });
      visitOrReload();
      cy.get(getFundDataInTable(id, FAVORITE))
        .invoke("text")
        .should("eq", "star");
    });
  });

  describe("Einnahmen, Ausgaben, Bilanz", function() {
    it("testen", function() {
      cy.resetDB();
      var idExpense1 = 1;
      var idExpense2 = 2;
      var idIncome1 = 3;
      var idIncome2 = 4;
      var descriptionExpense1 = "Ausgabe 1";
      var descriptionExpense2 = "Ausgabe 2";
      var descriptionIncome1 = "Einnahme 1";
      var descriptionIncome2 = "Einnahme 1";
      var valueExpense1 = -15.0;
      var valueExpense2 = -20.2;
      var valueIncome1 = 50.0;
      var valueIncome2 = 10.0;
      createFund({
        id: idExpense1,
        value: valueExpense1,
        description: descriptionExpense1,
        date: today
      });
      createFund({
        id: idExpense2,
        value: valueExpense2,
        description: descriptionExpense2,
        date: today
      });
      createFund({
        id: idIncome1,
        value: valueIncome1,
        description: descriptionIncome1,
        date: today
      });
      createFund({
        id: idIncome2,
        value: valueIncome2,
        description: descriptionIncome2,
        date: today
      });
      visitOrReload();
      cy.get(MONTHLY_INCOME)
        .invoke("text")
        .should("eq", "60.00€");
      cy.get(MONTHLY_EXPENSES)
        .invoke("text")
        .should("eq", "-35.20€");
      cy.get(MONTHLY_BALANCE)
        .invoke("text")
        .should("eq", "24.80€");
    });
  });
});

function visitOrReload() {
  cy.url().then(url => {
    if (url.endsWith(URL_BASE)) {
      cy.reload(true);
    } else {
      cy.visit(URL_BASE);
    }
  });
}

function setDescription(description) {
  cy.get(DESCRIPTION_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(description, { force: true });
}
function setValue(value) {
  cy.get(VALUE_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(value, { force: true });
}
function getFundInTable(i) {
  return `tbody > :nth-child(${i})`;
}
function getFundDataInTable(i, data) {
  return `tbody > :nth-child(${i}) > :nth-child(${data})`;
}
function createFund(fund) {
  return cy.request("POST", "http://localhost:3000/api/funds", fund);
}
