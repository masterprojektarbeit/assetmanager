/// <reference types="Cypress"/>

const moment = require("moment");
const URL_BASE = "/funds";
const VALUE = "tr > .text-xs-right";
const DATE = "1";
const CATEGORY = "3";
const DATE_INPUT = "#tf_date";
const TIME_INPUT = "#tf_time";
const DESCRIPTION_INPUT = "#cb_description";
const CATEGORY_SELECT =
  "#fundsCRUD > div > div > form > div:nth-child(3) > div";
const INCOME_BUTTON = "#btn_income";
const VALUE_INPUT = "#tf_value";
const SAVE_BUTTON = "#btn_save";
const NEW_BUTTON = "#btn_newFund";
const ERROR_MSG = ".v-messages__message";

const today = moment();
const todayValue = today.format("YYYY-MM-DD");
const tomorrow = moment().add(1, "days");
const tomorrowValue = tomorrow.format("YYYY-MM-DD");
const yesterday = moment().subtract(1, "days");
const yesterdayValue = yesterday.format("YYYY-MM-DD");

describe("Einmalige Geldströme", function() {
  before(function() {
    cy.resetDB();
    cy.login();
  });
  beforeEach(function() {
    visitOrReload();
  });
  describe("anlegen", function() {
    it("default Datum und Uhrzeit gesetzt", function() {
      cy.get(NEW_BUTTON).click();
      cy.get(DATE_INPUT).then(input => expect(input.val()).to.eq(todayValue));
      cy.get(TIME_INPUT).then(input =>
        expect(input.val()).to.eq(moment().format("HH:mm"))
      );
    });
    it("default Ausgabe gesetzt", function() {
      cy.get(NEW_BUTTON).click();
      cy.get(INCOME_BUTTON)
        .invoke("hasClass", "v-btn--active")
        .should("be.false");
    });
    describe("Ausgaben", function() {
      it("mit korrekten Daten ohne Kategorie mit morgigem Datum", function() {
        cy.resetDB();
        var description = "Beschreibung Ausgaben";
        var expense = "12.50";
        var id = 1;
        cy.get(NEW_BUTTON).click();
        setDate(tomorrowValue);
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should(
            "eq",
            moment()
              .add(1, "days")
              .format("DD.MM.YYYY")
          );
      });
      it("mit korrekten Daten ohne Kategorie mit gestrigem Datum", function() {
        cy.resetDB();
        var description = "Beschreibung Ausgaben 2";
        var expense = "20.50";
        var id = 1;
        cy.get(NEW_BUTTON).click();
        setDate(yesterdayValue);
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should(
            "eq",
            moment()
              .subtract(1, "days")
              .format("DD.MM.YYYY")
          );
      });
      it("mit korrekten Daten mit Kategorie", function() {
        cy.resetDB();
        var description = "Beschreibung Ausgaben mit Kategorie";
        var expense = "10.50";
        var id = 1;
        var categoryId = 1;
        var categoryName = "Kategorie Ausgaben";
        createCategory({
          name: categoryName
        });
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setCategory(categoryId);
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", "Kategorie Ausgaben");
      });
      it("ohne Datum", function() {
        var description = "Beschreibung Ausgaben ohne Datum";
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        cy.get(DATE_INPUT)
          .click()
          .clear({ force: true });
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss ein Datum angegeben werden.");
        cy.visit(URL_BASE);
      });
      it("ohne Uhrzeit", function() {
        var description = "Beschreibung Ausgaben ohne Uhrzeit";
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        cy.get(TIME_INPUT)
          .click()
          .clear({ force: true });
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss eine Uhrzeit angegeben werden.");
      });
      it("ohne Beschreibung", function() {
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        cy.get(ERROR_MSG).contains("Die Beschreibung muss angegeben werden");
      });
      it("ohne Betrag", function() {
        var description = "Beschreibung Ausgaben ohne Betrag";
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(VALUE_INPUT)
          .click()
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Betrag aus Buchstaben", function() {
        var description = "Ausgaben mit Betrag aus Buchstaben";
        var expense = "ggdzs";
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Betrag hat mehr als zwei Nachkommastellen", function() {
        cy.resetDB();
        var description =
          "Beschreibung Ausgaben mehr als zwei Nachkommastellen";
        var expense = "5.5052452";
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(VALUE)
          .invoke("text")
          .should("eq", "-5.51");
      });
    });
    describe("Einnahmen", function() {
      it("mit korrekten Daten ohne Kategorie mit morgigem Datum", function() {
        cy.resetDB();
        var description = "Beschreibung Einnahmen";
        var expense = "12.50";
        var id = 1;
        cy.get(NEW_BUTTON).click();
        setDate(tomorrowValue);
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should(
            "eq",
            moment()
              .add(1, "days")
              .format("DD.MM.YYYY")
          );
      });
      it("mit korrekten Daten ohne Kategorie mit gestrigem Datum", function() {
        cy.resetDB();
        var description = "Beschreibung Einnahmen 2";
        var expense = "20.50";
        var id = 1;
        cy.get(NEW_BUTTON).click();
        setDate(yesterdayValue);
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, DATE))
          .invoke("text")
          .should(
            "eq",
            moment()
              .subtract(1, "days")
              .format("DD.MM.YYYY")
          );
      });
      it("mit korrekten Daten mit Kategorie", function() {
        cy.resetDB();
        var description = "Beschreibung Einnahmen mit Kategorie";
        var expense = "10.50";
        var id = 1;
        var categoryId = 1;
        var categoryName = "Kategorie Einnahmen";
        createCategory({
          name: categoryName
        });
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setCategory(categoryId);
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(getFundDataInTable(id, CATEGORY))
          .invoke("text")
          .should("eq", "Kategorie Einnahmen");
      });
      it("ohne Datum", function() {
        var description = "Beschreibung Einnahmen ohne Datum";
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        cy.get(DATE_INPUT)
          .click()
          .clear({ force: true });
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss ein Datum angegeben werden.");
      });
      it("ohne Uhrzeit", function() {
        var description = "Beschreibung Einnahmen ohne Uhrzeit";
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        cy.get(TIME_INPUT)
          .click()
          .clear({ force: true });
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Es muss eine Uhrzeit angegeben werden.");
      });
      it("ohne Beschreibung", function() {
        var expense = "12.50";
        cy.get(NEW_BUTTON).click();
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        cy.get(ERROR_MSG).contains("Die Beschreibung muss angegeben werden");
      });
      it("ohne Betrag", function() {
        var description = "Beschreibung Einnahmen ohne Betrag";
        cy.get(NEW_BUTTON).click();
        cy.get(INCOME_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(VALUE_INPUT)
          .click()
          .clear({ force: true });
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Betrag aus Buchstaben", function() {
        var description = "Einnahmen mit Betrag aus Buchstaben";
        var expense = "ggdzs";
        cy.get(NEW_BUTTON).click();
        cy.get(INCOME_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        setValue(expense);
        cy.get(SAVE_BUTTON).should("be.disabled");
        cy.get(ERROR_MSG).contains("Der Wert muss eine Zahl sein.");
      });
      it("Betrag hat mehr als zwei Nachkommastellen", function() {
        cy.resetDB();
        var description =
          "Beschreibung Einnahmen mehr als zwei Nachkommastellen";
        var expense = "5.5052452";
        cy.get(NEW_BUTTON).click();
        setDescription(description);
        cy.get(DESCRIPTION_INPUT).type("{enter}", { force: true });
        cy.get(INCOME_BUTTON).click();
        setValue(expense);
        cy.get(SAVE_BUTTON).should("not.be.disabled");
        cy.get(SAVE_BUTTON).click({ force: true });
        visitOrReload();
        cy.get(VALUE)
          .invoke("text")
          .should("eq", "5.51");
      });
    });
  });
});

function visitOrReload() {
  cy.url().then(url => {
    if (url.endsWith(URL_BASE)) {
      cy.reload(true);
    } else {
      cy.visit(URL_BASE);
    }
  });
}
function setDate(date) {
  cy.get(DATE_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(date, { force: true });
}
function setDescription(description) {
  cy.get(DESCRIPTION_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(description, { force: true });
}
function setValue(value) {
  cy.get(VALUE_INPUT)
    .click({ force: true })
    .clear({ force: true })
    .type(value, { force: true });
}
function getFundDataInTable(i, data) {
  return `tbody > :nth-child(${i}) > :nth-child(${data})`;
}
function setCategory(categoryId) {
  cy.get(CATEGORY_SELECT).click();
  cy.get(
    `#app > div.v-menu__content.theme--dark.menuable__content__active > div > div > div:nth-child(${categoryId}) > a > div > div`
  ).click();
}
function createCategory(category) {
  return cy.request("POST", "http://localhost:3000/api/categories", category);
}
