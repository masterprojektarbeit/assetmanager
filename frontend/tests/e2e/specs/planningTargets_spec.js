/// <reference types="Cypress"/>

const moment = require("moment");
const URL_BASE = "/planningTargets";
const DETAIL_URL_BASE = "/planningTargetDetails/";
const FUND_CREATE_URL = "/funds/new";
const PAGINATION_CONTENT = ".v-datatable__actions__pagination";
const FIRST_PLANNINGTARGET_IN_TABLE = "tbody > :nth-child(1) > :nth-child(1)";
const SELECTION_FUTURE_PAST = "#input_selectionIsFuture";
const START_DATE_INPUT = "#tf_date_start";
const END_DATE_INPUT = "#tf_date_due";
const VALUE_INPUT = "#tf_value";
const SAVE_BUTTON = "#btn_save";
const NEW_BUTTON = "#btn_newTarget";
const EXPENSE_BUTTON = "#btn_expense";
const INCOME_BUTTON = "#btn_income";
const EDIT_BUTTON = "#btn_edit";
const DELETE_BUTTON = "#btn_delete";
const FUND_PLANNING_TIP = ".text-xs-right";
const BALANCE_OF_FIRST_PLANNINGTARGET = "tbody > tr > .text-xs-right";
const LABEL_START = "#lbl_startDate";
const LABEL_END = "#lbl_dueDate";
const LABEL_VALUE = "#lbl_targetValue";
const LABEL_BALANCE = "#lbl_balance";
const FUND_TABLE_IN_DETAIL = ".row > :nth-child(1) > .flex";
const ERROR_MSG_WRONG_DATE = ".v-messages__message";
const LABEL_RADIOBUTTON_PAST = "Abgeschlossene Ziele anzeigen";

const today = moment();
const todayTxt = today.format("DD.MM.YYYY");
const tomorrow = moment().add(1, "days");
const tomorrowTxt = tomorrow.format("DD.MM.YYYY");

describe("Planziele sollten", function() {
  before(function() {
    cy.resetDB();
    cy.login();
  });
  beforeEach(function() {
    visitOrReload();
  });
  describe("bei Testdaten", function() {
    before(function() {
      cy.insertFixture("/fixtures/test/planningTarget/");
    });
    it("alle offene Planziele enthalten", function() {
      cy.get(PAGINATION_CONTENT).contains(/1-5.+12/);
    });

    it("alle abgeschlossene Planziele enthalten", function() {
      cy.clickRadioButton(SELECTION_FUTURE_PAST, LABEL_RADIOBUTTON_PAST);
      cy.get(PAGINATION_CONTENT).contains(/1-10.+10/);
    });

    it("auf die einzelnen Planziele per Klick weiterleiten", function() {
      cy.get(FIRST_PLANNINGTARGET_IN_TABLE).click();
      cy.url().should("match", /.+\/planningTargetDetails\/\d+/);
    });
  });

  it("beim Anlegen eines neuen Geldstromes angezeigt werden", function() {
    const value = "1500.10";
    const fundValue = "1100";
    cy.get(NEW_BUTTON).click();
    setStartDate(todayTxt);
    setEndDate(tomorrowTxt);
    setValueInput(value);
    cy.get(SAVE_BUTTON).click();
    cy.visit(FUND_CREATE_URL);
    cy.get(FUND_PLANNING_TIP).then(content => {
      let txt = content.text();
      expect(txt)
        .to.include(tomorrowTxt)
        .and.to.include(value)
        .and.to.include("0.00");
      setValueInput(fundValue);
      cy.get(EXPENSE_BUTTON).click();
      cy.get(FUND_PLANNING_TIP).then(content =>
        expect(content.text()).to.include("-" + fundValue)
      );
      cy.get(INCOME_BUTTON).click();
      cy.get(FUND_PLANNING_TIP).then(content =>
        expect(content.text()).to.include(fundValue)
      );
      cy.visit(URL_BASE);
      cy.contains(value).click();
      cy.get(EDIT_BUTTON).click();
      cy.get(DELETE_BUTTON).click();
    });
  });

  it("bearbeitet werden können", function() {
    const newEnd = tomorrow.add(1, "days").format("DD.MM.YYYY");
    const newStart = tomorrowTxt;
    const newValue = "3.21";
    createPlanningTarget({
      value: "1.10",
      dueDate: tomorrow,
      startDate: today
    }).then(response => {
      const id = response.body.id;
      cy.visit(URL_BASE);
      cy.reload(true);
      cy.get(BALANCE_OF_FIRST_PLANNINGTARGET).contains("0.00");
      cy.visit(DETAIL_URL_BASE + id);
      cy.get(EDIT_BUTTON).click();
      setStartDate(newStart);
      setEndDate(newEnd);
      setValueInput(newValue);
      cy.get(SAVE_BUTTON).click();
      cy.visit(URL_BASE + "/");
      cy.contains(newValue).click();
      cy.get(LABEL_START).contains(newStart);
      cy.get(LABEL_END).contains(newEnd);
      cy.get(LABEL_VALUE).contains(newValue);
      deletePlanningTarget(id);
    });
  });
  it("die beinhaltenden Geldströme anzeigen", function() {
    const firstDescription = "Testbeschreibung1";
    const secondDescription = "Testbeschreibung2";
    const fund1Id = 1000;
    const fund2Id = 1001;

    createPlanningTarget({
      value: "1.10",
      dueDate: tomorrow,
      startDate: today
    }).then(response => {
      const id = response.body.id;
      createFund({
        id: fund1Id,
        value: -2.0,
        description: firstDescription,
        date: today
      });
      createFund({
        id: fund2Id,
        value: -22.0,
        description: secondDescription,
        date: today
      });
      cy.visit(DETAIL_URL_BASE + id);
      cy.get(FUND_TABLE_IN_DETAIL).contains(firstDescription);
      cy.get(FUND_TABLE_IN_DETAIL).contains(secondDescription);
      deletePlanningTarget(id);
      deleteFund(fund1Id);
      deleteFund(fund2Id);
    });
  });

  it("die beinhaltenden regulären Geldströme anzeigen", function() {
    const dueDate = moment().add(7, "days");
    const thirdDescription = "Testbeschreibung3";
    createPlanningTarget({
      value: "1.10",
      dueDate,
      startDate: today
    }).then(response => {
      const id = response.body.id;
      const fundId = 1000;
      createRegularFund({
        firstBooking: today,
        lastBooking: dueDate,
        frequenzy: "0 0 * * *", //daily
        value: 11.11,
        description: thirdDescription,
        date: today,
        id: fundId
      });
      cy.visit(DETAIL_URL_BASE + id);
      cy.get("td:contains('" + thirdDescription + "')").should(
        "have.length",
        5
      );
      deletePlanningTarget(id);
      deleteRegularFund(fundId);
    });
  });

  describe("beim Erstellen", function() {
    beforeEach(function() {
      visitOrReload();
      cy.get(NEW_BUTTON).click();
    });
    it("Start-, Enddatum und Betrag beim Anlegen abfragen", function() {
      setStartDate(todayTxt);
      setEndDate(tomorrowTxt);
      cy.get(SAVE_BUTTON).click();
    });
    it("auch wieder gelöscht werden können", function() {
      cy.visit(URL_BASE);
      cy.get(FIRST_PLANNINGTARGET_IN_TABLE).click();
      cy.get(EDIT_BUTTON).click();
      cy.url().then(url => {
        let id = url.replace("http://localhost:8080/#/planningtargets/", "");
        cy.get(DELETE_BUTTON).click();
        getPlanningTarget(id);
      });
    });

    it("kein Startdatum nach dem Enddatum erlauben", function() {
      setStartDate(tomorrowTxt);
      setEndDate(todayTxt);
      cy.get(SAVE_BUTTON).click();
      cy.get(ERROR_MSG_WRONG_DATE).contains(
        "Das Enddatum muss nach dem Startdatum liegen."
      );
    });

    it("kein Enddatum erlauben, was bereits in einem anderen Planziel enddatum ist ", function() {
      const fund1Id = 1000;
      const fund2Id = 1001;
      createPlanningTarget({
        value: "1.10",
        dueDate: tomorrow,
        startDate: today
      }).then(response => {
        const id = response.body.id;
        cy.visit(URL_BASE);
        cy.get(BALANCE_OF_FIRST_PLANNINGTARGET).contains("0.00");
        cy.visit(DETAIL_URL_BASE + id);
        cy.get(LABEL_BALANCE).contains("0.00");
        createFund({
          id: fund1Id,
          value: -2.0,
          description: "Testbeschreibung",
          date: today
        });
        cy.visit(URL_BASE);
        cy.get(BALANCE_OF_FIRST_PLANNINGTARGET).contains("-2.00");
        cy.visit(DETAIL_URL_BASE + id);
        cy.get(LABEL_BALANCE).contains("-2.00");
        createFund({
          id: fund2Id,
          value: 12.0,
          description: "Testbeschreibung",
          date: today
        });
        cy.visit(URL_BASE);
        cy.get(BALANCE_OF_FIRST_PLANNINGTARGET).contains("10.00");
        cy.visit(DETAIL_URL_BASE + id);
        cy.get(LABEL_BALANCE).contains("10.00");
        deletePlanningTarget(id);
        deleteFund(fund1Id);
        deleteFund(fund2Id);
      });
    });
    it("keinen negativen Betrag erlauben", function() {
      setValueInput("-420.69");
      cy.get(SAVE_BUTTON).should("be.disabled");
    });
  });
});

function createPlanningTarget(planningTarget) {
  return cy.request(
    "POST",
    "http://localhost:3000/api/planningtargets",
    planningTarget
  );
}

function getPlanningTarget(id) {
  return cy.request({
    method: "GET",
    url: "http://localhost:3000/api/planningtargets/" + id,
    failOnStatusCode: false
  });
}

function deletePlanningTarget(id) {
  return cy.request(
    "DELETE",
    "http://localhost:3000/api/planningtargets/" + id
  );
}

function createFund(fund) {
  return cy.request("POST", "http://localhost:3000/api/funds", fund);
}

function deleteFund(id) {
  return cy.request("DELETE", "http://localhost:3000/api/funds/" + id);
}

function createRegularFund(fund) {
  return cy.request("POST", "http://localhost:3000/api/regularfunds", fund);
}

function deleteRegularFund(id) {
  return cy.request("DELETE", "http://localhost:3000/api/regularfunds/" + id);
}

function visitOrReload() {
  cy.url().then(url => {
    if (url.match(URL_BASE)) {
      cy.reload(true);
    } else {
      cy.visit(URL_BASE);
    }
  });
}

function setStartDate(date) {
  cy.get(START_DATE_INPUT)
    .click()
    .clear()
    .type(date);
}

function setEndDate(date) {
  cy.get(END_DATE_INPUT)
    .click()
    .clear()
    .type(date);
}

function setValueInput(value) {
  cy.get(VALUE_INPUT)
    .click()
    .clear()
    .type(value);
}
