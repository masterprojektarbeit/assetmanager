import Vue from "vue";
import Router from "vue-router";
import Login from "@/components/Login";
import Dashboard from "@/components/Dashboard";
import PlanningTargets from "@/components/PlanningTargets";
import PlanningTargetsDetails from "@/components/PlanningTargetsDetails";
import PlanningTargetsCrud from "@/components/PlanningTargetsCrud";
import Categories from "@/components/Categories";
import CategoriesDetails from "@/components/CategoriesDetails";
import CategoriesFunds from "@/components/CategoriesFunds";
import Funds from "@/components/Funds";
import FundsDetails from "@/components/FundsDetails";
import FundsSplit from "@/components/FundsSplit";
import RegularFunds from "@/components/RegularFunds";
import RegularFundsDetails from "@/components/RegularFundDetail";
import Favorites from "@/components/Favorites";
import FavoritesDetails from "@/components/FavoritesDetails";
import ImportExportFunds from "@/components/ImportExportFunds";
import FundsDetailsMobile from "@/components/mobile/FundsDetailsMobile";
import FundsMobile from "@/components/mobile/FundsMobile";
import Configuration from "@/components/Configuration";
Vue.use(Router);
export const routes = [
  {
    path: "/mobile/funds/:id",
    customPath: "/mobile/funds/new",
    name: "Geldstrom Anlegen",
    component: FundsDetailsMobile,
    showInMenu: false,
    icon: "local_atm",
    mobile: true
  },
  {
    path: "/mobile/funds",
    name: "Geldströme",
    component: FundsMobile,
    showInMenu: true,
    icon: "local_atm",
    mobile: true
  },
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
    showInMenu: true,
    icon: "bubble_chart"
  },
  {
    path: "/funds/:id/split",
    name: "Geldstrom aufteilen",
    component: FundsSplit,
    showInMenu: false
  },
  {
    path: "/funds/:id",
    name: "Geldstrom bearbeiten",
    component: FundsDetails,
    showInMenu: false
  },
  {
    path: "/funds/",
    name: "Geldströme",
    component: Funds,
    showInMenu: true,
    icon: "local_atm",
    children: [
      {
        path: "/new",
        name: "Geldstrom anlegen",
        component: FundsDetails,
        showInMenu: false
      }
    ],
    props: route => ({
      query: route.query.q
    })
  },

  {
    path: "/regularFunds",
    name: "Regelmäßige Geldströme",
    component: RegularFunds,
    showInMenu: true,
    icon: "access_time"
  },
  {
    path: "/regularFunds/:id",
    name: "Regelmäßigen Geldstrom verwalten",
    component: RegularFundsDetails,
    showInMenu: false
  },
  {
    path: "/planningTargets",
    name: "Planziele",
    component: PlanningTargets,
    showInMenu: true,
    icon: "account_balance_wallet"
  },
  {
    path: "/planningTargets/:id",
    name: "Planziel verwalten",
    component: PlanningTargetsCrud,
    showInMenu: false
  },
  {
    path: "/planningTargetDetails/:id",
    name: "Planzielübersicht",
    component: PlanningTargetsDetails,
    showInMenu: false
  },
  {
    path: "/categories",
    name: "Kategorien",
    component: Categories,
    showInMenu: true,
    icon: "list"
  },
  {
    path: "/categories/:id",
    name: "Kategorie bearbeiten",
    component: CategoriesDetails,
    showInMenu: false
  },
  {
    path: "/categoriesFunds/:id",
    name: "Geldströme in Kategorie",
    component: CategoriesFunds,
    showInMenu: false
  },
  {
    path: "/favorites",
    name: "Favoriten",
    component: Favorites,
    showInMenu: true,
    icon: "star"
  },
  {
    path: "/favorites/:id",
    name: "Favoriten bearbeiten",
    component: FavoritesDetails,
    showInMenu: false
  },
  {
    path: "/importexportfunds",
    name: "Import/Export (Geldströme)",
    component: ImportExportFunds,
    showInMenu: true,
    icon: "sd_storage"
  },
  {
    path: "/configuration",
    name: "Einstellungen",
    component: Configuration,
    showInMenu: true,
    icon: "settings"
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    showInMenu: false
  }
];
function userLoggedIn() {
  return !!sessionStorage.getItem("accessToken");
}

const router = new Router({
  routes
});

router.beforeEach((to, from, next) => {
  document.title = "Assetmanager";
  if (to.path !== "/login") {
    if (!userLoggedIn()) {
      next("/login");
    } else {
      if (to && !!to.name) {
        document.title = to.name;
      }
      next();
    }
  } else {
    next();
  }
});

export default router;
