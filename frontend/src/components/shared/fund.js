export default class Fund {
  constructor(value, description, date, favoriteId, categoryId) {
    this.value = value;
    this.description = description;
    this.date = date;
    this.favoriteId = favoriteId;
    this.categoryId = categoryId;
  }
}
