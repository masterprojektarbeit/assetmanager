export default {
  dataIterator: {
    rowsPerPageText: "Einträge pro Seite:",
    rowsPerPageAll: "Alle",
    pageText: "{0}-{1} von {2}",
    noResultsText: "Keine passenden Daten gefunden.",
    nextPage: "Nächste Seite",
    prevPage: "Vorige Seite"
  },
  dataTable: {
    rowsPerPageText: "Zeilen pro Seite:"
  },
  noDataText: "Keine Daten verfügbar"
};
