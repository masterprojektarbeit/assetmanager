import moment from "moment";
export const dateFormatting = {
  data() {
    return {
      weekDays: [
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag"
      ]
    };
  },
  methods: {
    dateDisplayToPicker(datevalue) {
      return moment(datevalue, "DD.MM.YYYY", true).format("YYYY-MM-DD");
    },
    dateDisplay(datevalue) {
      return moment(datevalue).format("DD.MM.YYYY");
    },
    df_daily() {
      return "täglich";
    },
    df_weekly(date) {
      return `${this.formatWeekday(date)}s`;
    },
    df_monthly(date) {
      return `An jedem ${moment(date).format("D")}ten`;
    },
    df_quaterYearlySimple() {
      return "vierteljährlich";
    },
    df_quaterYearly(date) {
      return `Am ${moment(date).format("Do")} 
      ${moment(date).format("MMM")},
      ${moment(date)
        .add(3, "months")
        .format("MMM")}, ${moment(date)
        .add(6, "months")
        .format("MMM")}, ${moment(date)
        .add(9, "months")
        .format("MMM")}`;
    },
    df_halfYearlySimple() {
      return "halbjährlich";
    },
    df_halfYearly(date) {
      return `Am ${moment(date).format("DD. MMM")} und ${moment(date)
        .add(6, "months")
        .format("DD. MMM")}`;
    },
    df_yearlySimple() {
      return "jährlich";
    },
    df_yearly(date) {
      return `${moment(date).format("DD. MMM")}`;
    },
    formatWeekday: function(date) {
      return this.weekDays[moment(date).format("d")];
    },
    df_frequenzy(frequenzy, value) {
      switch (frequenzy) {
        case "daily":
          return this.df_daily();
        case "weekly":
          return this.df_weekly(value);
        case "monthly":
          return this.df_monthly(value);
        case "quaterYearly":
          return this.df_quaterYearlySimple(value);
        case "halfYearly":
          return this.df_halfYearlySimple(value);
        case "yearly":
          return this.df_yearlySimple(value);
      }
    }
  }
};
