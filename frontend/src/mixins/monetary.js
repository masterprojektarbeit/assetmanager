export const monetary = {
  methods: {
    monetaryString(value, showCurrencySymbol = false, useGrouping = true) {
      if (!value) {
        value = 0.0;
      }

      let options = {};
      options.useGrouping = useGrouping ? true : false;
      if (showCurrencySymbol) {
        options.style = "currency";
        options.currency = "EUR";
        options.currencyDisplay = "symbol";
      } else {
        options.style = "decimal";
        options.minimumFractionDigits = 2;
        options.maximumFractionDigits = 2;
      }
      return Intl.NumberFormat(this.getLocale(), options).format(value);
    },
    simpleValueInput: function(oldvalue, newvalue) {
      //wert nicht durch NaN ersetzen
      if (isNaN(newvalue)) return newvalue;
      //manuell alles markiert und eingetippt, oder vorher doppelt {entfernen}
      if (newvalue.length == 1) return "0.0" + +newvalue;
      // wird erstmals eine zahl eingeben ->0,0x aber nicht wenn für beide nachkomma stellen 0 getippt wird (string konvertiert dann nach int)
      let needsPrepend = oldvalue == 0 && Number.isInteger(+newvalue);
      if (needsPrepend) {
        newvalue = "0.0" + +newvalue;
      } else {
        if (/\.\d\d\d/.exec(newvalue)) {
          newvalue = "" + newvalue * 10.0;
        }
      }
      return (+newvalue).toFixed(2);
    },
    getLocale() {
      if (navigator.languages != undefined) return navigator.languages[0];
      else return navigator.language;
    }
  }
};
