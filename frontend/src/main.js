import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import Vuetify from "vuetify";
import "./registerServiceWorker";
import "vuetify/dist/vuetify.min.css";
import AlertComponent from "./components/shared/Alert.vue";
import "./globalCatcher";

import de from "./vuetify/locale/de";

Vue.use(Vuetify, {
  lang: {
    locales: { de },
    current: "de"
  }
});
Vue.component("custom-alert", AlertComponent);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
