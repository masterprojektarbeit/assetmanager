import axios from "axios";
import router from "./router";

axios.interceptors.response.use(undefined, function(error) {
  if (error.response.status === 401) {
    sessionStorage.removeItem("accessToken");
    router.push("/login");
    return Promise.reject(error);
  }
});
