# assetmanager

## Anwendung starten
* Datenbank starten, entweder wie unten beschrieben per Docker oder die `/backend/config.env` so anpassen, dass auf eine vorhandene DB zugegriffen werden kann
* Vor dem ersten Start:
    * in beiden Verzeichnissen npm i
    * `node . --migrate-db` in /backend zum Erstellen des Datenbankschemas 
    *  Falls Testdaten gewollt sind `npm run import-testdata` in /backend
    *  Ansonsten manuell in der DB einen User anlegen. Alternativ:
        * `node . --fixture-user` importiert den in `/backend/fixtures/test/user/` angegebenen User
        * Wenn das Backend mit der `--secure` Option gestartet wird (vgl. CLI Hilfe, `--help`) wird der Benutzer aus der config ausgelesen. 
          In diesem Fall den nächsten Schritt überspringen
* `node .` in /backend
* `npm run serve` in /frontend


## Docker DB einzeln starten:

In root-Verzeichnis des Projektes: `docker-compose up -d db`

Dabei wird der unter `/backend/fixtures/test/user/` angegebene User eingespielt. Sollte dieser geändert werden sollen, muss dies VOR dem ersten Aufruf durch compose geschehen.

Sonst muss das Backend-Image neu gebaut werden.

## PSQL in Container aufrufen

`docker exec -it assetmanager-db_1 psql -h localhost -p 5432 -U assetmanager -d assetmanager`

Datenbank ist aber auch extern ansprechbar, auf host unter Port 5432.
(bspw. mit \dt Tabellen listen, \q beendet)

## Automigration der Tabellen

Backend mit parameter `--migrate-db` starten. Auch als `--update-db` verfügbar (versucht Update statt Überschreiben). Bei Migrate werden alle Daten gelöscht.

## Testdaten

Im Backendverzeichnis können mit `npm run import-testdata` Datenbankinhalte generiert werden.

## Testdatenbank

Beim Ausführen von tests kann das Backend falls gewollt per `--testing` flag auf eine in-memory Datenbank umgestellt werden. Diese ist abgesehen von dem Login leer und wird nach dem Beenden des Backends verworfen.

## Testen

In `/backend/test/unit` gibt es Jest tests, die in `/backend` per `npx jest` ausgeführt werden können. Mit der Flag `--coverage` kann die Testabdeckung ausgegeben werden.

Im `/frontend/tests/e2e/spec` gibt es Cypress tests, die in `/frontend` per `npx cypress run` headless ausgeführt werden können. Die `import-export-csv_spec.js` ist hierbei vorher mit `.skip` (hinter dem ersten describe) herauszunehmen, da sie nur im Browser funktioniert. Der Browser kann anstelle des headless-test per `npx cypress open` aufgerufen werden und zeigt die einzelnen Testdatein an.

Für das Ausführen der Frontend-Tests müssen Backend und Frontend bereits gestartet sein.

## Backup und Recovery der Datenbank

Im Backendverzeichnis kann mit `node . --backup-db <filename>` ein Backup der Datenbank erzeugt werden. Das Backup wird in die spezifizierte Datei geschrieben.
Mit dem Befehl `node . --recover-db <filename>` kann eine Backupdatei gelesen und damit die Datenbank überschrieben werden. Dabei sollte zumindest das Schema public vorhanden sein.

### Auto-Linting

Die Unterprojekte sind so konfiguriert, das Dateien beim Speichern automatisch (falls möglich) per eslint gefixt werden.

Dazu müssen in VSCode die Erweiterungen "ESLint, Vetur" installiert sein. Prettier sollte nicht vorhanden/deaktiviert sein.

Die User Settings von VSCode müssen folgende Zeilen enthalten:

```
...
 "eslint.enable": true,
    "eslint.run": "onType",
    "eslint.validate": [
        {
            "language": "vue",
            "autoFix": true
        },
        {
            "language": "html",
            "autoFix": true
        },
        {
            "language": "javascript",
            "autoFix": true
        },
        {
            "language": "javascriptreact",
            "autoFix": true
        }
    ],
    "eslint.alwaysShowStatus": true,
    "eslint.autoFixOnSave": true,
    "vetur.format.defaultFormatter.js": "prettier",
    "vetur.format.defaultFormatter.css": "prettier"
...
```

Optional können auf windows systemen noch unix line endings beim speichern forciert werden mit

    "files.eol": "\n"

Sollte ein Problem wie "https://google.com/#q=prettier%2Fprettier  Delete `CR`  src\router\index.js:54:28" auftreten, hilft im Backend  folgendes Kommando:
`npm run lint-autofix`

## Starten aller Services per Docker-Compose

Statt nur der Datenbank können auch alle Services per Docker per `docker-compose up -d` gestartet werden, d.h. DB, Backend & Frontend. 

Dabei wird die Datenbank zurückgesetzt.

Das Frontend arbeitet mit einem Serviceworker (PWA), daher sollte auf Caching geachtet werden.

## Backup & Restore der Datenbank in Container
Die folgenden Befehle zeigen eine Beispielausführung, wie ein Dump erstellt und wieder eingespielt werden kann. Der Dump wird währenddessen umbenannt, um Konflikte im Beispiel zu vermeiden. Bei den `exec`-Befehlen wird das Passwort der DB verlangt.

* docker exec -it assetmanager_backend_1 node . --backup-db test.dump
* docker cp assetmanager_backend_1:/backend/test.dump .
* docker cp .\test.dump assetmanager_backend_1:/backend/test2.dump
* docker exec -it assetmanager_backend_1 node . --recover-db test2.dump
