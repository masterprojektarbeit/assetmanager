"use strict";
const automigrate = require("../../server/boot/automigrate");
jest.mock("../../logger.js");
describe("Database-AutoUpdate", function() {
  test("should not be performed if flag not set by CLI", () => {
    const mockFn = jest.fn(() => {});
    const mock = {
      dataSources: {
        db: {
          autoupdate: mockFn
        }
      }
    };
    automigrate(mock);
    expect(mockFn.mock.calls.length).toBe(0);
  });

  test("should be performed if flag set by CLI", () => {
    const preTestEnv = process.env;
    process.env.DATABASE_AUTOUPDATE = true;
    const mockFn = jest.fn(() => {});
    const mock = {
      dataSources: {
        db: {
          autoupdate: mockFn
        }
      }
    };
    automigrate(mock);
    expect(mockFn.mock.calls.length).toBe(1);
    process.env = preTestEnv;
  });
});
