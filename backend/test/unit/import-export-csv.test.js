"use strict";
const createFundFunctionsInBackend = require("../../server/models/fund");

jest.mock("../../logger.js");
jest.mock("../../server/helper/recommend");
const TEST_FUND = {
  value: "420.69",
  description: "TestFund",
  date: new Date().getTime(),
  categoryId: "0",
  favoriteId: "0"
};

describe("Import/Exports von CSV's (serverseitig)", function() {
  describe("Import", function() {
    test("sollte in der Datei doppelt vorhandene Einträge beide verwerfen", function(done) {
      const fakeFund = getFundMock();
      let testLine = `1337;${TEST_FUND.value};${TEST_FUND.description};${
        TEST_FUND.date
      };${TEST_FUND.categoryId};${TEST_FUND.favoriteId}`;
      testLine = testLine + "\n" + testLine;
      fakeFund.create = jest.fn(() => {
        return true;
      });
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, testLine)
        .then(result => {
          expectImportOutputToBe(result, 0, 1, 1, 1);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte keine Fehler für die Beschreibungszeile werfen", function(done) {
      const descriptionLine = "id;value;description;date;categoryId;favoriteId";
      const fakeFund = getFundMock();
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, descriptionLine)
        .then(result => {
          expectImportOutputToBe(result, 0, 0, 0, 0);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte mit leeren Dateien klarkommen", function(done) {
      const fakeFund = getFundMock();
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, "")
        .then(result => {
          expectImportOutputToBe(result, 0, 0, 0, 0);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte Einträge ohne ID in die DB schreiben", function(done) {
      const fakeFund = getFundMock();
      const testLine = `;${TEST_FUND.value};${TEST_FUND.description};${
        TEST_FUND.date
      };${TEST_FUND.categoryId};${TEST_FUND.favoriteId}`;
      fakeFund.create = jest.fn(
        ({ id, value, description, date, categoryId, favoriteId }) => {
          expect(id).toBeNull();
          expect(value).toEqual(TEST_FUND.value);
          expect(description).toEqual(TEST_FUND.description);
          expect(new Date(date).getTime()).toEqual(TEST_FUND.date * 1000);
          expect(categoryId).toEqual(TEST_FUND.categoryId);
          expect(favoriteId).toEqual(TEST_FUND.favoriteId);
        }
      );
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, testLine)
        .then(result => {
          expectImportOutputToBe(result, 1, 0, 0, 0);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte Einträge mit ID in die DB schreiben", function(done) {
      const fakeFund = getFundMock();
      let testLine = `1;${TEST_FUND.value};${TEST_FUND.description};${
        TEST_FUND.date
      };${TEST_FUND.categoryId};${TEST_FUND.favoriteId}`;
      fakeFund.create = jest.fn(() => {
        return true;
      });
      fakeFund.find = jest.fn(filter => {
        return new Promise(res => res([]));
      }); // So werden keine Duplikate gefunden
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, testLine)
        .then(result => {
          expectImportOutputToBe(result, 1, 0, 0, 0);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte in der DB vorhandene Einträge (ID) nicht erneut importieren", function(done) {
      const fakeFund = getFundMock();
      const testFundWithId = Object.assign({ id: 1 }, TEST_FUND);
      let testLine = `1;${TEST_FUND.value};${TEST_FUND.description};${
        TEST_FUND.date
      };${TEST_FUND.categoryId};${TEST_FUND.favoriteId}`;
      fakeFund.create = jest.fn(() => {
        return true;
      });
      fakeFund.find = jest.fn(filter => {
        return new Promise(res => res([testFundWithId]));
      }); // So wird ein Duplikate gefunden
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, testLine)
        .then(result => {
          expectImportOutputToBe(result, 0, 1, 0, 0);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });

    test("sollte ungültige Einträge verwerfen & anzeigen", function(done) {
      const fakeFund = getFundMock();
      createFundFunctionsInBackend(fakeFund);
      mockBackendCall(fakeFund.import, "abc\ndef")
        .then(result => {
          expectImportOutputToBe(result, 0, 0, 2, 2);
          expect(result[2]).toEqual([0, 1]);
          done();
        })
        .catch(err => {
          expect(err).toBeNull();
        });
    });
  });

  describe("Export", function() {
    test("sollte Start- und Enddatum als Parameter beachten", function(done) {
      const fakeFund = getFundMock();
      createFundFunctionsInBackend(fakeFund);
      const from = 10000;
      const to = 11000;
      fakeFund.getRange = jest.fn((dateFrom, dateTo) => {
        expect(dateFrom.getTime()).toEqual(from * 1000);
        expect(dateTo.getTime()).toEqual(to * 1000);
        done();
      });
      mockBackendCall(fakeFund.export, from, to).catch(err => {
        expect(err).toBeNull();
      });
    });
  });
});

function parseImportOutput(output) {
  return {
    importedCount: parseInt(/\d+/.exec(output[0])[0]),
    discardedCount: parseInt(/\d+/.exec(output[1])[0]),
    errorLines: output[2],
    errorCount: output[3]
  };
}
function expectImportOutputToBe(
  res,
  importedCount,
  discardedCount,
  errorLinesLength,
  errorCount
) {
  const result = parseImportOutput(res);
  expect(result.discardedCount).toEqual(discardedCount);
  expect(result.errorCount).toEqual(errorCount);
  expect(result.errorLines.length).toEqual(errorLinesLength);
  expect(result.importedCount).toEqual(importedCount);
}

function getFundMock() {
  return {
    find: jest.fn(x => {
      return null;
    }),
    remoteMethod: jest.fn(x => {}),
    create: jest.fn(x => {
      return null;
    }),
    app: {
      models: {
        Favorite: {
          find: jest.fn(x => {
            return null;
          })
        }
      }
    }
  };
}

function mockBackendCall(functionToCall, arg1, arg2) {
  return new Promise((resolve, reject) => {
    let fn = function(err, result) {
      if (err) reject(err);
      resolve(result);
    };
    if (!arg2) {
      functionToCall(arg1, fn);
    } else {
      functionToCall(arg1, arg2, fn);
    }
  });
}
