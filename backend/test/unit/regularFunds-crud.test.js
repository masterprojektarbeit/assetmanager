"use strict";
jest.mock("../../logger.js");
const createFundFunctionsInBackend = require("../../server/models/regular-fund");
const mockFilteringFunctionWithoutPromise = require("./helper/mockFilteringFunctionWithoutPromise");
const moment = require("moment");

describe("CRUD von regelmäßigen Geldströmen", function() {
  /**
   * Hole alle Geldströme die täglich, wöchentlich,... , jährlich verbucht werden.
   * Erster Zeitpunkt: heute; Zweiter Zeitpunkt: +2 Jahre
   */
  test("getRange sollte alle regelmäßigen Geldströme zwischen zwei Zeitpunkten ausgeben", async function() {
    const fakeRegularFund = getRegularFundMock();
    fakeRegularFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeRegularFund);
    let result = await mockBackendCall(
      fakeRegularFund.getRange,
      moment()
        .startOf("days")
        .toDate(),
      moment()
        .add(2, "years")
        .endOf("days")
        .toDate()
    );

    expect(result[0].value).toEqual(-56.0);
    expect(result[1].value).toEqual(-156.0);
    expect(result[2].value).toEqual(-11.7);
    expect(result[3].value).toEqual(74.2);
    expect(result[4].value).toEqual(23.5);
    expect(result[5].value).toEqual(92.0);
    expect(result.length).toEqual(6);
  });
  /**
   * Hole alle Geldströme, die in diesem Monat verbucht werden.
   * Dazu zählt der tägliche und wöchentliche Eintrag
   */
  test("getMonthlyFunds sollte alle Geldströme innerhalb eines Monats zurückgeben", async function() {
    const fakeRegularFund = getRegularFundMock();
    fakeRegularFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeRegularFund);
    let result = await mockBackendCall(
      fakeRegularFund.getMonthlyFunds,
      parseInt(moment().format("YYYY")),
      parseInt(
        moment()
          .subtract(1, "months")
          .format("MM")
      )
    );

    expect(result[0].value).toEqual(-56.0);
    expect(result[1].value).toEqual(-156.0);
    expect(result.length).toEqual(2);
  });
  /**
   * Hole alle Geldströme zwischen heute und heute+2 Jahren und die Kategorie-ID 2 haben.
   */
  test("getRangeCategory sollte alle Geldströme innerhalb einer Kategorie zwischen zwei Zeitpunkten zurückgeben", async function() {
    const categoryId = 2;
    const fakeRegularFund = getRegularFundMock();
    fakeRegularFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeRegularFund);
    let result = await mockBackendCall(
      fakeRegularFund.getRangeCategory,
      categoryId,
      moment()
        .startOf("days")
        .toDate(),
      moment()
        .add(2, "years")
        .endOf("days")
        .toDate()
    );

    expect(result[0].value).toEqual(-11.7);
    expect(result[1].value).toEqual(74.2);
    expect(result.length).toEqual(2);
  });
  /**
   * Hole alle Geldströme, die in diesem Monat verbucht werden und die Kategorie-ID 1 haben.
   */
  test("getMonthlyFundsCategory sollte alle Geldströme innerhalb einer Kategorie innerhalb eines Monats zurückgeben", async function() {
    const categoryId = 1;
    const fakeRegularFund = getRegularFundMock();
    fakeRegularFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeRegularFund);
    let result = await mockBackendCall(
      fakeRegularFund.getMonthlyFundsCategory,
      categoryId,
      parseInt(moment().format("YYYY")),
      parseInt(
        moment()
          .subtract(1, "months")
          .format("MM")
      )
    );

    expect(result[0].value).toEqual(-56.0);
    expect(result.length).toEqual(1);
  });
  /**
   * Hole den einzigen monatlichen Geldstrom
   */
  test("getByFrequenzy sollte alle regelmäßigen Geldströme in einer gemeinsamen Frequenzeinheit ausgeben", async function() {
    const freq = "monthly";
    const fakeRegularFund = getRegularFundMock();
    fakeRegularFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeRegularFund);
    let result = await mockBackendCall(
      fakeRegularFund.getByFrequenzy,
      false,
      false,
      freq
    );
    expect(result[0].value).toEqual(-11.7);
    expect(result.length).toEqual(1);
  });
});

function getRegularFundMock() {
  return {
    find: jest.fn(x => {
      return null;
    }),
    remoteMethod: jest.fn(x => {}),
    create: jest.fn(x => {
      return null;
    }),
    app: {
      models: {
        Favorite: {
          find: jest.fn(x => {
            return null;
          })
        }
      }
    }
  };
}

function mockBackendCall(functionToCall, arg1, arg2, arg3) {
  return new Promise((resolve, reject) => {
    let fn = function(err, result) {
      if (err) reject(err);
      resolve(result);
    };
    if (!arg2 && !arg3) {
      functionToCall(arg1, fn);
    } else {
      if (!arg3) {
        functionToCall(arg1, arg2, fn);
      } else {
        functionToCall(arg1, arg2, arg3, fn);
      }
    }
  });
}

const frequencies = {
  // Jeden Tag um 0 Uhr
  daily: "0 0 * * *",
  // An jedem angegebenen Wochentag (So-Sa) um 0 Uhr
  weekly: "0 0 * * MON",
  // An jedem angebenen Tag im Monat um 0 Uhr
  monthly: "0 0 15 * *",
  // An jedem angebenen Tag (1-31) im Monat um 0 Uhr, alle 3 Monate
  quarterYearly: "0 0 15 */3 *",
  // An jedem angebenen Tag (1-31) im Monat um 0 Uhr, alle 6 Monate
  halfYearly: "0 0 15 */6 *",
  // An jedem angebenen Tag (1-31) im angegebenen Monat (1-12) um 0 Uhr
  yearly: "0 0 15 6 *"
};

const testFunds = [
  {
    id: 1,
    value: -56.0,
    description: "Ausgabe Täglich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(1, "days")
      .toDate(),
    frequenzy: frequencies.daily,
    categoryId: 1
  },
  {
    id: 2,
    value: -156.0,
    description: "Ausgabe Wöchentlich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(6, "days")
      .toDate(),
    frequenzy: frequencies.weekly
  },
  {
    id: 3,
    value: -11.7,
    description: "Ausgabe Monatlich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(1, "months")
      .toDate(),
    frequenzy: frequencies.monthly,
    categoryId: 2
  },
  {
    id: 4,
    value: 74.2,
    description: "Einnahme Vierteljährlich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(4, "months")
      .toDate(),
    frequenzy: frequencies.quarterYearly,
    categoryId: 2
  },
  {
    id: 5,
    value: 23.5,
    description: "Einahme Halbjährlich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(6, "months")
      .toDate(),
    frequenzy: frequencies.halfYearly
  },
  {
    id: 6,
    value: 92.0,
    description: "Einahme Jährlich",
    firstBooking: moment().toDate(),
    lastBooking: moment()
      .add(1, "years")
      .toDate(),
    frequenzy: frequencies.yearly,
    categoryId: 1
  }
];
