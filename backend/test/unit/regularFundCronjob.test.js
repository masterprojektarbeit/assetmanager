let mockCronFn = jest.fn(function(
  cronString,
  functionToExectucte,
  nonsense,
  nonsense2,
  nonsense3,
  context
) {});

jest.mock("../../logger.js");
jest.mock("cron", () => {
  return {
    CronJob: mockCronFn
  };
});

const createCronjob = require("../../server/boot/cronjob");
const cronParser = require("cron-parser");
const moment = require("moment");

describe("Das automatisiertes Verbuchen von regelmäßigen Geldströmen sollte", function() {
  it("1 mal Pro Tag buchen", async function() {
    let appMock = getAppMock();
    // fake DB Antwort, so dass heute schon gebucht wurde (ignoriert direktes Buchen bei Anwendungsstart, testet nur CronJob)
    appMock.models.Configuration.findOne = function(query, callback) {
      callback(null, { lastCronStart: moment() });
    };
    await createCronjob(appMock);
    const firstArgumentOfFirstCronjobCall = mockCronFn.mock.calls[0][0];
    const cronInterval = cronParser.parseExpression(
      firstArgumentOfFirstCronjobCall
    );
    let nextDates = cronInterval.iterate(3);
    let tomorrow = moment(nextDates[0].getTime());
    let dayAfterTomorrow = moment(nextDates[1].getTime());
    let dayAfterDayAfterTomorrow = moment(nextDates[2].getTime());
    expect(dayAfterTomorrow.diff(tomorrow, "days")).toBe(1);
    expect(dayAfterDayAfterTomorrow.diff(dayAfterTomorrow, "days")).toBe(1);
    expect(dayAfterDayAfterTomorrow.diff(tomorrow, "days")).toBe(2);
  });
  it("buchen falls noch nie gebucht wurde", async function() {
    let appMock = getAppMock();
    // fake DB Antwort, so dass noch nie gebucht wurde
    appMock.models.Configuration.findOne = function(query, callback) {
      callback(null, null);
    };
    let mockUpsertCfgFn = jest.fn(function(newCfg, callback) {
      callback(null);
    });
    appMock.models.Configuration.upsert = mockUpsertCfgFn;

    let mockGetByFrequenzyFn = jest.fn(function(
      includePassed,
      includeFuture,
      frequency,
      callback
    ) {
      callback(null, []);
    });
    //fake DB Antwort, sodass keine Regulären Geldströme gefunden werden
    appMock.models.RegularFund.getByFrequenzy = mockGetByFrequenzyFn;
    await createCronjob(appMock);
    const newlySavedCfg = mockUpsertCfgFn.mock.calls[0][0];
    // Teste ob gespeichert wurde, dass heute gebucht wurde
    expect(newlySavedCfg.lastCronStart.diff(moment(), "days")).toBe(0);
    // Für einen Tag gebucht  mal 6 Frequenzen => Sollte 6 mal aufgerufen worden sein
    expect(mockGetByFrequenzyFn.mock.calls.length).toBe(6);
  });
  it("buchen falls am aktuellen Tag noch nicht gebucht wurde", async function() {
    let yesterday = moment();
    yesterday.subtract(1, "day");
    let appMock = getAppMock();
    // fake DB Antwort, so dass gestern das letzte mal gebucht wurde
    appMock.models.Configuration.findOne = function(query, callback) {
      callback(null, callback(null, { lastCronStart: yesterday }));
    };
    let mockUpsertCfgFn = jest.fn(function(newCfg, callback) {
      callback(null);
    });
    appMock.models.Configuration.upsert = mockUpsertCfgFn;

    let mockGetByFrequenzyFn = jest.fn(function(
      includePassed,
      includeFuture,
      frequency,
      callback
    ) {
      callback(null, []);
    });
    //fake DB Antwort, sodass keine Regulären Geldströme gefunden werden
    appMock.models.RegularFund.getByFrequenzy = mockGetByFrequenzyFn;
    await createCronjob(appMock);
    const newlySavedCfg = mockUpsertCfgFn.mock.calls[0][0];
    // Teste ob gespeichert wurde, dass heute gebucht wurde
    expect(newlySavedCfg.lastCronStart.diff(moment(), "days")).toBe(0);
    // Für einen Tag gebucht  mal 6 Frequenzen => Sollte 6 mal aufgerufen worden sein
    expect(mockGetByFrequenzyFn.mock.calls.length).toBe(6);
  });
  it("nicht buchen falls am aktuellen Tag schon gebucht wurde", async function() {
    let today = moment();
    let appMock = getAppMock();
    // fake DB Antwort, so dass heute das letzte mal gebucht wurde
    appMock.models.Configuration.findOne = function(query, callback) {
      callback(null, callback(null, { lastCronStart: today }));
    };
    let mockUpsertCfgFn = jest.fn(function(newCfg, callback) {
      callback(null);
    });
    appMock.models.Configuration.upsert = mockUpsertCfgFn;

    await createCronjob(appMock);
    // Teste ob config nicht geupdated = nicht erneut gebucht wurde
    expect(mockUpsertCfgFn.mock.calls.length).toBe(0);
  });
  it("tägliche Geldströme verbuchen", async function() {
    let yesterday = moment().subtract(1, "day");
    let tomorrow = moment().add(1, "day");
    const dailyFund = {
      firstbooking: yesterday,
      lastbooking: tomorrow,
      frequenzy: "0 0 * * *",
      value: "99.99",
      description: "Testbeschreibung Daily",
      date: moment()
    };
    let appMock = mockAppForRegularBook(dailyFund, "daily");

    await createCronjob(appMock);
    // booking sollte für "täglich aufgerufen worden sein"
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[0][2]).toMatch(
      "daily"
    );
    // 1 Täglicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(dailyFund.description);
    expect(createdFund.value).toMatch(dailyFund.value);
  });
  it("wöchentliche Geldströme verbuchen", async function() {
    let aWeekAgo = moment().subtract(7, "days");
    let tomorrow = moment().add(1, "day");
    let weekDayForCron = moment().format("d");
    const weeklyFund = {
      firstbooking: aWeekAgo,
      lastbooking: tomorrow,
      frequenzy: `0 0 * * ${weekDayForCron}`,
      value: "99.99",
      description: "Testbeschreibung Weekly",
      date: moment()
    };
    let appMock = mockAppForRegularBook(weeklyFund, "weekly");

    await createCronjob(appMock);
    // booking sollte für "wöchentlich" aufgerufen worden sein
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[1][2]).toMatch(
      "weekly"
    );
    // 1 Wöchentlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(weeklyFund.description);
    expect(createdFund.value).toMatch(weeklyFund.value);
  });
  it("monatliche Geldströme verbuchen", async function() {
    let aMonthAgo = moment().subtract(1, "month");
    let tomorrow = moment().add(1, "day");
    let monthDayForCron = moment().format("D");
    const monthlyFund = {
      firstbooking: aMonthAgo,
      lastbooking: tomorrow,
      frequenzy: `0 0 ${monthDayForCron} * *`,
      value: "99.99",
      description: "Testbeschreibung Monthly",
      date: moment()
    };
    let appMock = mockAppForRegularBook(monthlyFund, "monthly");

    await createCronjob(appMock);
    // booking sollte für "monatlich" aufgerufen worden sein
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[2][2]).toMatch(
      "monthly"
    );
    // 1 Monatlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(monthlyFund.description);
    expect(createdFund.value).toMatch(monthlyFund.value);
  });
  it("vierteljährliche Geldströme verbuchen", async function() {
    let threeMonthsAgo = moment().subtract(3, "month");
    let tomorrow = moment().add(1, "day");
    let monthDayForCron = moment().format("D");
    const quaterYearlyFund = {
      firstbooking: threeMonthsAgo,
      lastbooking: tomorrow,
      frequenzy: `0 0 ${monthDayForCron} */3 *`,
      value: "99.99",
      description: "Testbeschreibung Vierteljährlich",
      date: moment()
    };
    let appMock = mockAppForRegularBook(quaterYearlyFund, "quarterYearly");

    await createCronjob(appMock);
    // booking sollte für "vierteljährlich" aufgerufen worden sein
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[3][2]).toMatch(
      "quarterYearly"
    );
    // 1 vierteljährlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(quaterYearlyFund.description);
    expect(createdFund.value).toMatch(quaterYearlyFund.value);
  });
  it("halbjährliche Geldströme verbuchen", async function() {
    let sixMonthsAgo = moment().subtract(6, "month");
    let tomorrow = moment().add(1, "day");
    let monthDayForCron = moment().format("D");
    const halfYearlyFund = {
      firstbooking: sixMonthsAgo,
      lastbooking: tomorrow,
      frequenzy: `0 0 ${monthDayForCron} */6 *`,
      value: "99.99",
      description: "Testbeschreibung Halbjährlich",
      date: moment()
    };
    let appMock = mockAppForRegularBook(halfYearlyFund, "halfYearly");

    await createCronjob(appMock);
    // booking sollte für "halbjährlich" aufgerufen worden sein
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[4][2]).toMatch(
      "halfYearly"
    );
    // 1 halbjährlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(halfYearlyFund.description);
    expect(createdFund.value).toMatch(halfYearlyFund.value);
  });
  it("jährliche Geldströme verbuchen", async function() {
    let aYearAgo = moment().subtract(1, "year");
    let tomorrow = moment().add(1, "day");
    let monthDayForCron = moment().format("D");
    let monthForCron = moment().format("M");
    const yearlyFund = {
      firstbooking: aYearAgo,
      lastbooking: tomorrow,
      frequenzy: `0 0 ${monthDayForCron} ${monthForCron} *`,
      value: "99.99",
      description: "Testbeschreibung Jährlich",
      date: moment()
    };
    let appMock = mockAppForRegularBook(yearlyFund, "yearly");

    await createCronjob(appMock);
    // booking sollte für "jährlich" aufgerufen worden sein
    expect(appMock.models.RegularFund.getByFrequenzy.mock.calls[5][2]).toMatch(
      "yearly"
    );
    // 1 jährlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(1);
    const createdFund = appMock.models.Fund.create.mock.calls[0][0];
    // gebuchter Fund sollte mit relevatenen Feldern des regulären Funds übereinstimmen
    expect(createdFund.description).toMatch(yearlyFund.description);
    expect(createdFund.value).toMatch(yearlyFund.value);
  });

  describe("Geldströme nicht verbuchen falls nicht 'heute' aktiv (Negativtest)", function() {
    it("jährlich", async function() {
      let aYearAgo = moment().subtract(1, "year");
      let tomorrow = moment().add(1, "day");
      let monthDayForCron = moment()
        .add(1, "day")
        .format("D");
      let monthForCron = moment().format("M");
      const halfYearlyFund = {
        firstbooking: aYearAgo,
        lastbooking: tomorrow,
        frequenzy: `0 0 ${monthDayForCron} ${monthForCron} *`,
        value: "99.99",
        description: "Testbeschreibung Jährlich",
        date: moment()
      };
      let appMock = mockAppForRegularBook(halfYearlyFund, "yearly");

      await createCronjob(appMock);
      // booking sollte für "jährlich" aufgerufen worden sein
      expect(
        appMock.models.RegularFund.getByFrequenzy.mock.calls[5][2]
      ).toMatch("yearly");
      // 1 jährlicher Fund wurde von DB zurückgegeben + 1 Tag zu buchen => 1x Aufgerufen
      expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
    });
    it("halbjährlich", async function() {
      let sixMonthsAgo = moment()
        .subtract(6, "month")
        .subtract(1, "day");
      let tomorrow = moment().add(1, "day");
      let monthDayForCron = moment()
        .add(1, "day")
        .format("D");
      const halfYearlyFund = {
        firstbooking: sixMonthsAgo,
        lastbooking: tomorrow,
        frequenzy: `0 0 ${monthDayForCron} */6 *`,
        value: "99.99",
        description: "Testbeschreibung Halbjährlich",
        date: moment()
      };
      let appMock = mockAppForRegularBook(halfYearlyFund, "halfYearly");

      await createCronjob(appMock);
      expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
    });
    it("vierteljährlich", async function() {
      let threeMonthsAgo = moment()
        .subtract(3, "month")
        .subtract(1, "day");
      let tomorrow = moment().add(1, "day");
      let monthDayForCron = moment()
        .add(1, "day")
        .format("D");
      const quaterYearlyFund = {
        firstbooking: threeMonthsAgo,
        lastbooking: tomorrow,
        frequenzy: `0 0 ${monthDayForCron} */3 *`,
        value: "99.99",
        description: "Testbeschreibung Vierteljährlich",
        date: moment()
      };
      let appMock = mockAppForRegularBook(quaterYearlyFund, "quarterYearly");

      await createCronjob(appMock);
      expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
    });
    it("monatlich", async function() {
      let aMonthAgo = moment().subtract(1, "month");
      let tomorrow = moment().add(1, "day");
      let monthDayForCron = moment()
        .add(1, "day")
        .format("D");
      const monthlyFund = {
        firstbooking: aMonthAgo,
        lastbooking: tomorrow,
        frequenzy: `0 0 ${monthDayForCron} * *`,
        value: "99.99",
        description: "Testbeschreibung Monthly",
        date: moment()
      };
      let appMock = mockAppForRegularBook(monthlyFund, "monthly");

      await createCronjob(appMock);
      expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
    });
    it("wöchentlich", async function() {
      let aWeekAgo = moment().subtract(7, "days");
      let tomorrow = moment().add(1, "day");
      let weekDayForCron = moment()
        .add(1, "day")
        .format("d");
      const weeklyFund = {
        firstbooking: aWeekAgo,
        lastbooking: tomorrow,
        frequenzy: `0 0 * * ${weekDayForCron}`,
        value: "99.99",
        description: "Testbeschreibung Weekly",
        date: moment()
      };
      let appMock = mockAppForRegularBook(weeklyFund, "weekly");

      await createCronjob(appMock);
      expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
    });
    //täglich offensichtlicherweise nicht
  });

  it("abgeschlossene reguläre Geldströme nicht verbuchen", async function() {
    let yesterday = moment().subtract(1, "day");
    let dayBeforeYestarday = moment().subtract(2, "days");
    const dailyFund = {
      firstbooking: dayBeforeYestarday,
      lastbooking: yesterday,
      frequenzy: "0 0 * * *",
      value: "99.99",
      description: "Testbeschreibung Daily",
      date: moment()
    };
    let appMock = mockAppForRegularBook(dailyFund, "daily");

    await createCronjob(appMock);
    // 1 Täglicher Fund wurde von DB zurückgegeben, der aber schon "abgelaufen" ist => Keine Buchungen
    expect(appMock.models.Fund.create.mock.calls.length).toBe(0);
  });

  it("die Tage nachbuchen, die verpasst wurden", async function() {
    let aWeekAgo = moment().subtract(7, "days");
    let appMock = getAppMock();
    // fake DB Antwort, so dass vor einer Woche das letzte mal gebucht wurde
    appMock.models.Configuration.findOne = function(query, callback) {
      callback(null, callback(null, { lastCronStart: aWeekAgo }));
    };
    let mockUpsertCfgFn = jest.fn(function(newCfg, callback) {
      callback(null);
    });
    appMock.models.Configuration.upsert = mockUpsertCfgFn;

    let mockGetByFrequenzyFn = jest.fn(function(
      includePassed,
      includeFuture,
      frequency,
      callback
    ) {
      callback(null, []);
    });
    //fake DB Antwort, sodass keine Regulären Geldströme gefunden werden
    appMock.models.RegularFund.getByFrequenzy = mockGetByFrequenzyFn;
    await createCronjob(appMock);
    // Für sieben Tag gebucht  mal 6 Frequenzen => Sollte 42 mal aufgerufen worden sein
    expect(mockGetByFrequenzyFn.mock.calls.length).toBe(42);
  });
});

function getAppMock() {
  return {
    models: {
      Fund: {
        create: jest.fn(x => {})
      },
      RegularFund: {
        getByFrequenzy: jest.fn(x => {})
      },

      Configuration: {
        findOne: jest.fn(x => {}),
        upsert: jest.fn(x => {})
      }
    }
  };
}

function mockAppForRegularBook(regularFund, frequencyInput) {
  let appMock = getAppMock();
  // keine Config in db => noch nicht gebucht => für heute buchen
  appMock.models.Configuration.findOne = function(query, callback) {
    callback(null, null);
  };
  // Rückspeichern/Update der Config wird hier ignoriert
  appMock.models.Configuration.upsert = jest.fn(function(newCfg, callback) {
    callback(null);
  });

  let mockGetByFrequenzyFn = jest.fn(function(
    includePassed,
    includeFuture,
    frequency,
    callback
  ) {
    if (frequency === frequencyInput) {
      //Für die zu testende Frequenz gebe genau einen regulären Fund zurück
      callback(null, [regularFund]);
    } else {
      //und sonst keine
      callback(null, []);
    }
  });
  //fake DB Antwort, sodass nur der angegebene Regulären Geldström gefunden wird
  appMock.models.RegularFund.getByFrequenzy = mockGetByFrequenzyFn;

  // Später wird abgefragt welche Funds erzeugt wurden
  appMock.models.Fund.create = jest.fn(function(newFund, callback) {
    callback(null);
  });
  return appMock;
}
