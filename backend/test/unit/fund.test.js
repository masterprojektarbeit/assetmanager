"use strict";
jest.mock("../../logger.js");
const createFundFunctionsInBackend = require("../../server/models/fund");
const mockFilteringFunctionWithoutPromise = require("./helper/mockFilteringFunctionWithoutPromise");
const moment = require("moment");

// bei Änderungen die Auswirkung auf die berechneten Äquivalenzklassen beachten
const testFunds = [
  {
    value: -10.0,
    description: "Ausgabe 1",
    date: moment().toDate(),
    categoryId: "0",
    favoritId: "0"
  },
  {
    value: -20.5,
    description: "Ausgabe 2",
    date: moment().toDate(),
    categoryId: "1",
    favoritId: "0"
  },
  {
    value: -100.0,
    description: "Ausgabe 3",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "0",
    favoritId: "0"
  },
  {
    value: -10.0,
    description: "Ausgabe 4",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "1",
    favoritId: "0"
  },
  {
    value: -350.0,
    description: "Ausgabe 5",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "1",
    favoritId: "0"
  },
  {
    value: 50.0,
    description: "Einnahme 1",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "0",
    favoritId: "0"
  },
  {
    value: 5.0,
    description: "Einnahme 2",
    date: moment().toDate(),
    categoryId: "1",
    favoritId: "0"
  },
  {
    value: 10.0,
    description: "Einnahme 3",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "1",
    favoritId: "0"
  },
  {
    value: 50.0,
    description: "Einnahme 4",
    date: moment()
      .add(1, "months")
      .toDate(),
    categoryId: "1",
    favoritId: "0"
  }
];

describe("Einmalige Geldströme", function() {
  test("getRange sollte alle Geldströme zwischen zwei Zeitpunkten zurückgeben", async function() {
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getRange,
      moment()
        .startOf("days")
        .toDate(),
      moment()
        .add(2, "days")
        .endOf("days")
        .toDate()
    );
    expect(result[0].value).toEqual(-10.0);
    expect(result[1].value).toEqual(-20.5);
    expect(result[2].value).toEqual(5.0);
    expect(result.length).toEqual(3);
  });
  test("getMonthlyFunds sollte alle Geldströme innerhalb eines Monats zurückgeben", async function() {
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getMonthlyFunds,
      parseInt(moment().format("YYYY")),
      parseInt(
        moment()
          .subtract(1, "months")
          .format("MM")
      )
    );
    expect(result[0].value).toEqual(-10.0);
    expect(result[1].value).toEqual(-20.5);
    expect(result[2].value).toEqual(5.0);
    expect(result.length).toEqual(3);
  });
  test("getRangeCategory sollte alle Geldströme innerhalb einer Kategorie zwischen zwei Zeitpunkten zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getRangeCategory,
      categoryId,
      moment()
        .startOf("days")
        .toDate(),
      moment()
        .add(2, "days")
        .endOf("days")
        .toDate()
    );
    expect(result[0].value).toEqual(-20.5);
    expect(result[1].value).toEqual(5.0);
    expect(result.length).toEqual(2);
  });
  test("getMonthlyFundsCategory sollte alle Geldströme innerhalb einer Kategorie innerhalb eines Monats zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getMonthlyFundsCategory,
      categoryId,
      parseInt(moment().format("YYYY")),
      parseInt(
        moment()
          .subtract(1, "months")
          .format("MM")
      )
    );
    expect(result[0].value).toEqual(-20.5);
    expect(result[1].value).toEqual(5.0);
    expect(result.length).toEqual(2);
  });
  test("getSumCategoryExpense sollte die Summe aller Ausgaben innerhalb einer Kategorie zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getSumCategoryExpense,
      categoryId
    );
    expect(result).toEqual(-380.5);
  });
  test("getSumCategoryMonthExpense sollte die Summe aller Ausgaben innerhalb einer Kategorie innerhalb eines Monats zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getSumCategoryMonthExpense,
      categoryId,
      moment().format("YYYY"),
      moment().format("MM")
    );
    expect(result).toEqual(-360);
  });
  test("getSumCategoryIncome sollte die Summe aller Einnahmen innerhalb einer Kategorie zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getSumCategoryIncome,
      categoryId
    );
    expect(result).toEqual(65.0);
  });
  test("getSumCategoryMonthIncome sollte die Summe aller Einnahmen innerhalb einer Kategorie innerhalb eines Monats zurückgeben", async function() {
    const categoryId = 1;
    const fakeFund = getFundMock();
    fakeFund.find = mockFilteringFunctionWithoutPromise(testFunds);
    createFundFunctionsInBackend(fakeFund);
    let result = await mockBackendCall(
      fakeFund.getSumCategoryMonthIncome,
      categoryId,
      moment().format("YYYY"),
      moment().format("MM")
    );
    expect(result).toEqual(60.0);
  });
});

function getFundMock() {
  return {
    find: jest.fn(x => {
      return null;
    }),
    remoteMethod: jest.fn(x => {}),
    create: jest.fn(x => {
      return null;
    }),
    app: {
      models: {
        Favorite: {
          find: jest.fn(x => {
            return null;
          })
        }
      }
    }
  };
}

function mockBackendCall(functionToCall, arg1, arg2, arg3) {
  return new Promise((resolve, reject) => {
    let fn = function(err, result) {
      if (err) reject(err);
      resolve(result);
    };
    if (!arg2 && !arg3) {
      functionToCall(arg1, fn);
    } else {
      if (!arg3) {
        functionToCall(arg1, arg2, fn);
      } else {
        functionToCall(arg1, arg2, arg3, fn);
      }
    }
  });
}
