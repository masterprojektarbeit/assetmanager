"use strict";
jest.mock("../../logger.js");
jest.mock("../../server/server", () => {
  let appMock = {
    models: {
      Fund: {
        getRange: (start, end, cb) => {
          cb(null, [
            { value: 3 },
            { value: 10 },
            { value: 100 },
            { value: -2 }
          ]);
        }
      },
      RegularFund: {
        getRange: (start, end, cb) => {
          cb(null, [
            { value: 5 },
            { value: 20 },
            { value: 200 },
            { value: -3 }
          ]);
        }
      }
    }
  };
  return appMock;
});
const createPlanningTargetFunctionsInBackend = require("../../server/models/planning-target");
const mockFilteringFunction = require("./helper/mockFilteringFunction");
const moment = require("moment");

describe("Planziele", function() {
  test("getAfterDate sollte alle Planziele mit Ende an dem angegebenen Tag oder danach zurückgeben", async function() {
    const fakePlanningTarget = getPlanningTargetMock();
    fakePlanningTarget.find = mockFilteringFunction(testPlanningTargets);
    createPlanningTargetFunctionsInBackend(fakePlanningTarget);
    let result = await mockBackendCall(
      fakePlanningTarget.getAfterDate,
      moment().toDate()
    );
    expect(result[0].value).toEqual(4);
    expect(result[1].value).toEqual(2);
    expect(result[2].value).toEqual(3);
  });

  test("getBeforeDate sollte alle Planziele mit Ende vor dem angegebenen Tag zurückgeben", async function() {
    const fakePlanningTarget = getPlanningTargetMock();
    fakePlanningTarget.find = mockFilteringFunction(testPlanningTargets);
    createPlanningTargetFunctionsInBackend(fakePlanningTarget);
    let result = await mockBackendCall(
      fakePlanningTarget.getBeforeDate,
      moment().toDate()
    );
    expect(result[0].value).toEqual(1);
    expect(result[1].value).toEqual(5);
  });

  test("getNextDueTarget sollte das nächstmögliche Planziel zurückgeben", async function() {
    const fakePlanningTarget = getPlanningTargetMock();
    fakePlanningTarget.findOne = mockFilteringFunction(testPlanningTargets);
    createPlanningTargetFunctionsInBackend(fakePlanningTarget);

    let result = await mockBackendCall(
      fakePlanningTarget.getNextDueTarget,
      moment().toDate()
    );
    expect(result[0][0].value).toEqual(4);
  });

  test("sollten Infos über den aktuellen Stand enthalten (addFulfillment)", async function() {
    const fakePlanningTarget = getPlanningTargetMock();
    fakePlanningTarget.find = mockFilteringFunction(testPlanningTargets);
    createPlanningTargetFunctionsInBackend(fakePlanningTarget);
    let result = await mockBackendCall(
      fakePlanningTarget.getAfterDate,
      moment().toDate()
    );
    expect(
      result.filter(res => parseFloat(res.fundsSum) === 111).length
    ).toEqual(3);
    expect(
      result.filter(res => parseFloat(res.regularFundsSum) === 222).length
    ).toEqual(3);
    expect(
      result.filter(res => parseFloat(res.balance) === 333).length
    ).toEqual(3);
  });
});

function getPlanningTargetMock() {
  let dummyFn = jest.fn(x => {
    return null;
  });
  return {
    validatesUniquenessOf: dummyFn,
    find: dummyFn,
    findOne: dummyFn,
    remoteMethod: jest.fn(x => {})
  };
}

function mockBackendCall(functionToCall, arg1, arg2) {
  return new Promise((resolve, reject) => {
    let fn = function(err, result) {
      if (err) reject(err);
      resolve(result);
    };
    if (!arg2) {
      functionToCall(arg1, fn);
    } else {
      functionToCall(arg1, arg2, fn);
    }
  });
}

const testPlanningTargets = [
  {
    value: 1,
    dueDate: moment()
      .subtract(1, "days")
      .toDate(),
    startDate: moment()
      .subtract(5, "days")
      .toDate()
  },
  {
    value: 2,
    dueDate: moment()
      .add(2, "days")
      .toDate(),
    startDate: moment()
      .add(1, "days")
      .toDate()
  },
  {
    value: 3,
    dueDate: moment()
      .add(4, "days")
      .toDate(),
    startDate: moment()
      .add(3, "days")
      .toDate()
  },
  {
    value: 4,
    dueDate: moment()
      .add(1, "days")
      .toDate(),
    startDate: moment()
      .subtract(2, "days")
      .toDate()
  },
  {
    value: 5,
    dueDate: moment()
      .subtract(10, "days")
      .toDate(),
    startDate: moment()
      .subtract(15, "days")
      .toDate()
  }
];
