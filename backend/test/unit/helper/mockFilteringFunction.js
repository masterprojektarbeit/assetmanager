"use strict";
const filterApllier = require("my-loopback-filter");

/**
 * Erstellt eine Funktion, die einen LoopbackFilter auf die angebenen Werte anwendet und ein Promise returnt
 * @param {*} valuesToFilter Die Werte, auf die der Filter bei Aufruf der zurückgegebenen Funktion angewendet werden soll
 */
function fn(valuesToFilter) {
  let filteringFunction = function(filter) {
    return new Promise(res => {
      res(filterApllier.applyLoopbackFilter(valuesToFilter, filter));
    });
  };
  return filteringFunction;
}
module.exports = fn;
