"use strict";
const filterApllier = require("my-loopback-filter");

/**
 * Erstellt eine Funktion, die einen LoopbackFilter auf die angebenen Werte anwendet
 * @param {*} valuesToFilter Die Werte, auf die der Filter bei Aufruf der zurückgegebenen Funktion angewendet werden soll
 */
function fn(valuesToFilter) {
  let filteringFunction = function(filter, cb) {
    cb(null, filterApllier.applyLoopbackFilter(valuesToFilter, filter));
  };
  return filteringFunction;
}
module.exports = fn;
