"use strict";
const Log = require("log");
const logger = new Log(process.env.LOG_LEVEL || "notice");
module.exports = {
  /**
   *  Nur für Debugging
   */
  debug: msg => logger.debug(msg),
  /**
   *  Nicht ungedingt benötigte Infos
   */
  info: msg => logger.info(msg),
  /**
   *  Infos die relevant, aber normal sind
   */
  notice: msg => logger.notice(msg),
  /**
   *  Ungewöhnliches, was aber noch kein Fehler ist
   */
  warning: msg => logger.warning(msg),
  /**
   *  Fehler
   */
  error: msg => logger.error(msg)
};
