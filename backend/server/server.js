"use strict";
const dotenv = require("dotenv");
var loopback = require("loopback");
const logger = require("../logger");
var boot = require("loopback-boot");
const cli = require("./cli");
const keyStore = require("./keyStore");
const fs = require("fs");
const CONFIG_PATH = "./config.env";
const KEY_STORE_PATH = "./keys.store";

let bootOptions = {
  appRootDir: __dirname,
  dataSources: {}
};
if (process.argv.filter(arg => arg.includes("--testing")).length !== 0) {
  //Testumgebung
  bootOptions.dataSources.db = {
    name: "db",
    connector: "memory"
  };
  dotenv.config({ path: CONFIG_PATH });
  cli();
  startServer(bootOptions);
} else if (process.argv.filter(arg => arg.includes("secure")).length !== 0) {
  // Umgebung mit Passwort
  cli();
  if (!process.env.PASSWORD || process.env.PASSWORD.length === 0) {
    logger.error("option 'secure' needs a password!");
    process.exit(1);
  }
  prepareServerWithSecureConfig(bootOptions, KEY_STORE_PATH)
    .then(bootOptions => startServer(bootOptions))
    .catch(err => {
      logger.error(err);
      process.exit(1);
    });
} else {
  // Normale Umgebung
  dotenv.config({ path: CONFIG_PATH });
  cli();
  let envFileContent = dotenv.parse(fs.readFileSync(CONFIG_PATH));
  if (process.env.DATABASE_COMPOSE_URL)
    envFileContent.DATABASE_HOST = process.env.DATABASE_COMPOSE_URL;
  bootOptions.dataSources.db = createDBConfigFromObject(envFileContent);
  startServer(bootOptions);
}

function startServer(bootOptions) {
  var app = (module.exports = loopback());
  app.use(loopback.token());
  app.start = function() {
    // start the web server
    return app.listen(function() {
      app.emit("started");
      var baseUrl = app.get("url").replace(/\/$/, "");
      logger.info(`Web server listening at: ${baseUrl}`);
      if (app.get("loopback-component-explorer")) {
        var explorerPath = app.get("loopback-component-explorer").mountPath;
        logger.notice(`Browse your REST API at ${baseUrl}${explorerPath}`);
      }
    });
  };

  // Bootstrap the application, configure models, datasources and middleware.
  // Sub-apps like REST API are mounted via boot scripts.
  boot(app, bootOptions, function(err) {
    if (err) throw err;
    // start the server if `$ node server.js`
    if (require.main === module) app.start();
  });
}

/**
 * Sollte eine verschlüsselte Konfiguration vorhanden sein, wird diese zum Starten des Servers verwendet.
 * Sollte diese nicht vorhanden sein, wird die Umgebungsdatei für den nächsten Start verschlüsselt.
 * Die Umgebungsdatei wird nicht automatisch gelöscht! (Sollte dann manuell geschehen.)
 * @param {*} bootOptions Serverstart-Optionen, in die die Informationen geschrieben werden
 * @param {*} keyStorePath Pfad zu der verschlüsselten Konfiguration
 */
async function prepareServerWithSecureConfig(bootOptions, keyStorePath) {
  let config;
  let password = process.env.PASSWORD;
  let key = "config";
  const store = await keyStore(keyStorePath);
  let keyStoreExists = fs.existsSync(keyStorePath);
  if (keyStoreExists) {
    config = store.getPrivateKeyData(key, password);
  } else {
    fs.writeFileSync(keyStorePath, "");
    config = dotenv.parse(fs.readFileSync(CONFIG_PATH));
    await store.saveKey(key, password, config);
  }
  bootOptions.dataSources.db = createDBConfigFromObject(config);
  if (!keyStoreExists) {
    process.env.EMAIL = config.EMAIL;
    process.env.USERNAME_APP = config.USERNAME;
    process.env.USER_PASSWORD = config.PASSWORD;
  }
  return bootOptions;
}

/**
 * Ließt aus einem geeigneten Objekt die benötigten Informationen für eine Datenbankquelle aus.
 */
function createDBConfigFromObject(obj) {
  return {
    host: obj.DATABASE_HOST,
    port: obj.DATABASE_PORT,
    url: "",
    database: obj.DATABASE_NAME,
    name: "db",
    user: obj.DATABASE_USERNAME,
    password: obj.DATABASE_PASSWORD,
    connector: "postgresql"
  };
}
