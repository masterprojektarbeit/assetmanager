"use strict";
const leven = require("leven");
const PriorityQueue = require("priorityqueuejs");

/**
 * Findet die ähnlichsten Wörter zu einem angegebenen String
 * @param {string} str (Teil)Wort zum vergleichen
 * @param list Liste die gefiltert wird
 * @param {string} key Eigenschaft der Listenelemente, die mit str verglichen wird
 * @param {number} limit Gewünschte Anzahl der Vorschläge
 * @param {number} maxDist Maximale Levenshtein-Distanz für Match (ignoriert für ersten limit Einträge)
 * @returns Array der Listenelemente die vorgeschlagen werden
 */

module.exports = function(str, list, key, limit = 1, maxDist = 5) {
  const queue = new PriorityQueue(function(a, b) {
    return b.distance - a.distance;
  });
  for (let i = 0; i < list.length; i++) {
    let element = list[i];
    let word = element[key];
    let distance = leven(
      str.toLowerCase(),
      word.toLowerCase().substr(0, str.length)
    );
    if (queue.size < limit || distance <= maxDist) {
      queue.enq({ distance, element });
    }
  }
  let result = [];
  for (let i = Math.min(limit, queue.size()); i > 0; i--) {
    result.push(queue.deq().element);
  }
  return result;
};
