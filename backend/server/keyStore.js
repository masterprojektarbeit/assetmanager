const fs = require("fs");
const util = require("util");
const createStore = require("key-store").createStore;
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
module.exports = async function(filePath) {
  const saveKeys = data => writeFile(filePath, JSON.stringify(data), "utf8");
  let store;
  if (fs.existsSync(filePath)) {
    content = JSON.parse(await readFile(filePath, "utf8"));
    store = createStore(saveKeys, content);
  } else {
    store = createStore(saveKeys);
  }
  return store;
};
