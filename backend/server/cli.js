"use strict";
const cli = require("command-line-args");
const commandLineUsage = require("command-line-usage");
const logger = require("../logger");
const optionsDefinition = [
  {
    name: "help",
    alias: "h",
    type: Boolean,
    description: "Gibt die angezeigte Hilfe in die Konsole aus"
  },
  {
    name: "migrate-db",
    alias: "m",
    type: Boolean,
    description: "Überschreibt das Datenbankschema und löscht alle Daten"
  },
  {
    name: "update-db",
    alias: "u",
    type: Boolean,
    description:
      "Versucht das Datenbankschema zu aktualiseren und die Daten zu erhalten.\nKann fehlschlagen!"
  },
  {
    name: "fixture-user",
    type: Boolean,
    description:
      "Spielt den Standardbenutzer in die Datenbank ein." +
      "\nDieser darf nicht bereits enthalten sein!"
  },
  {
    name: "fixture",
    alias: "f",
    type: String,
    typeLabel: "{underline Pfad-zur-Fixture}",
    description: "Spielt die angegebene Fixture ein und beendet den Prozess."
  },
  {
    name: "backup-db",
    type: String,
    typeLabel: "{underline dateiname}",
    description:
      "Erstellt nach Eingabe des Datenbankpassworts ein Backup der Datenbank." +
      "\nBenötigt eine aktuelle version von psql."
  },
  {
    name: "recover-db",
    type: String,
    typeLabel: "{underline dateiname}",
    description:
      "Spielt nach Eingabe des Datenbankpassworts das angegebene Backup in die Datenbank ein." +
      "\nBenötigt eine aktuelle version von psql."
  },
  {
    name: "testing",
    alias: "t",
    type: Boolean,
    description:
      "Startet eine In-Memory-Datenbank und spielt den Standardbenutzer ein.\n" +
      "CLI Operationen (Migration, ...) funktionieren nicht für diese Datenbank.\n" +
      "Nach Ende des Prozesses wird die Datenbank verworfen!\n" +
      "Erstellt zusätzliche API-Endpunkte zur Steuerung der Datenbank"
  },
  {
    name: "secure",
    type: String,
    typeLabel: "{underline Passwort}",
    description:
      "Sollte eine verschlüsselte Konfiguration vorhanden sein, wird diese (bei Übereinstimmen des Passworts) zum Starten des Servers verwendet." +
      "\nSollte diese nicht vorhanden sein, wird die Umgebungsdatei für den nächsten Start verschlüsselt." +
      "\nDie Umgebungsdatei wird nicht automatisch gelöscht! (Muss manuell erfolgen)" +
      "\nIst bereits ein Benutzer in der Datenbank vorhanden, wird er gelöscht." +
      "\nDiese Option sollte NICHT mit anderen Optionen zusammen verwendet werden!"
  }
];
const sections = [
  {
    header: "Assetmanager",
    content: "Ermöglicht die komfortable Verwaltung von Geldströmen."
  },
  {
    header: "Optionen",
    optionList: optionsDefinition
  }
];
const usage = commandLineUsage(sections);

module.exports = function() {
  const options = cli(optionsDefinition);
  if (options.help) {
    console.log(usage);
    process.exit(0);
  } else if (options.secure) {
    if (Object.keys(options).length > 1) {
      logger.error("Secure option cannot be combined with other options!");
      process.exit(1);
    }
    process.env.PASSWORD = options.secure;
  } else if (options.testing) {
    process.env.NODE_ENV = "development";
    process.env.DATABASE_AUTOUPDATE = true;
    process.env.FIXTURE_USER = true;
  } else {
    process.env.NODE_ENV = "production";
    if (options["migrate-db"]) {
      process.env.DATABASE_AUTOMIGRATE = true;
    } else if (options["update-db"]) {
      process.env.DATABASE_AUTOUPDATE = true;
    } else if (Object.keys(options).includes("backup-db")) {
      var backup = options["backup-db"];
      if (backup === null) backup = "assetmanager_backup.dump";
      process.env.DATABASE_BACKUP = backup;
    } else if (Object.keys(options).includes("recover-db")) {
      var recover = options["recover-db"];
      if (recover === null) recover = "assetmanager_backup.dump";
      process.env.DATABASE_RECOVERY = recover;
    }
    if (options["fixture-user"]) {
      process.env.FIXTURE_USER = true;
    }
  }
  if (options["fixture"]) {
    process.env.FIXTURE = options.fixture;
  }
};
