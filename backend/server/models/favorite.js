"use strict";

module.exports = function(Favorite) {
  /**
   * Löscht den gegebenen Favoriten und entfernt die Referenzen aus den Geldströmen
   * @param {number} id of favorite
   * @param {callback} cb callback fn
   */
  Favorite.deleteFavoriteWithReferences = function(id, cb) {
    let Fund = Favorite.app.models.Fund;
    Fund.updateAll(
      {
        favoriteId: id
      },
      { favoriteId: null },
      (err, info) => {
        if (err) cb(err);
        else {
          Favorite.destroyById(id, cb);
        }
      }
    );
  };

  Favorite.remoteMethod("deleteFavoriteWithReferences", {
    http: {
      path: "/deleteFavoriteWithReferences",
      verb: "get"
    },
    accepts: [{ arg: "id", type: "number", required: true }]
  });
};
