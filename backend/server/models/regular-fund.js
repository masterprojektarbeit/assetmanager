"use strict";
var logger = require("../../logger.js");
var parser = require("cron-parser");
const HttpErrors = require("http-errors");
const moment = require("moment");

const frequencyRegex = {
  // Jeden Tag um 0 Uhr
  daily: /0 0 \* \* \*/,
  // An jedem angegebenen Wochentag (So-Sa) um 0 Uhr
  weekly: /0 0 \* \* [0-6]/,
  // An jedem angebenen Tag im Monat um 0 Uhr
  monthly: /0 0 ([1-9]|[12]\d|3[0-1]) \* \*/,
  // An jedem angebenen Tag (1-31) im Monat um 0 Uhr, alle 3 Monate
  quarterYearly: /0 0 ([1-9]|[12]\d|3[0-1]) \*\/3 \*/,
  // An jedem angebenen Tag (1-31) im Monat um 0 Uhr, alle 6 Monate
  halfYearly: /0 0 ([1-9]|[12]\d|3[0-1]) \*\/6 \*/,
  // An jedem angebenen Tag (1-31) im angegebenen Monat (1-12) um 0 Uhr
  yearly: /0 0 ([1-9]|[12]\d|3[0-1]) (\d|1[0-2]) \*/
};

module.exports = function(Regularfund) {
  /**
   * Returns all RegularFunds that will occur between two dates
   * @param {date} from start date of the range
   * @param {date} to last date of the range
   * @param {callback} cb callback fn
   */
  Regularfund.getRange = function(from, to, cb) {
    logger.debug("Requesting RegularFunds in a date Range");
    var calculatedOccurrences = [];
    // early exit für requests in der vergangenheit
    if (new Date() - new Date(to) > 0) {
      cb(null, calculatedOccurrences);
      return;
    }
    var rangeFilter = {
      where: {
        and: [
          { firstBooking: { lte: to } },
          { or: [{ lastBooking: { gte: from } }, { lastBooking: null }] }
        ]
      }
    };
    Regularfund.find(rangeFilter, (err, regFunds) => {
      if (err) {
        logger.error(err);
        return cb(err);
      }
      regFunds.forEach(el => {
        try {
          let startDate = moment().isAfter(moment(from))
            ? moment()
            : moment(from);
          if (moment(el.firstBooking).isAfter(startDate)) {
            startDate = moment(el.firstBooking).startOf("day");
          }
          // damit alle Einträge >= Anfang des Monats gefunden werden
          startDate.subtract(1, "second");
          var options = {
            currentDate: startDate.toDate(),
            endDate: el.lastBooking
              ? new Date(Math.min(to, el.lastBooking))
              : new Date(to),
            tz: "Europe/Berlin",
            iterator: true
          };

          var interval = parser.parseExpression(el.frequenzy, options);
          while (true) {
            try {
              var obj = interval.next();
              var occurrency = {
                value: el.value,
                date: new Date(obj.value.toString()),
                id: el.id,
                description: el.description,
                categoryId: el.categoryId
              };
              calculatedOccurrences.push(occurrency);
            } catch (e) {
              break;
            }
          }
        } catch (err) {
          logger.error("Error: " + err.message);
        }
      });
      cb(null, calculatedOccurrences);
    });
  };

  Regularfund.remoteMethod("getRange", {
    http: {
      path: "/range",
      verb: "get"
    },
    accepts: [{ arg: "from", type: "date" }, { arg: "to", type: "date" }],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  /**
   * Returns all Regularfund occurences in a given month
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Regularfund.getMonthlyFunds = function(year, month, cb) {
    logger.debug("Requesting Funds in a given month.");
    var startDate = new Date(year, month, 1, 0, 0, 0, 0);
    // endDate = 00:00  the first day of the following month minus one second
    var endDate = new Date(startDate);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setSeconds(endDate.getSeconds() - 1);
    return Regularfund.getRange(startDate, endDate, cb);
  };

  Regularfund.remoteMethod("getMonthlyFunds", {
    http: {
      path: "/monthly",
      verb: "get"
    },
    accepts: [
      { arg: "year", type: "number" },
      { arg: "month", type: "number" }
    ],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  /**
   * Returns all Regularfund occurences in a given month grouped per id
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Regularfund.getGroupedMonthlyFunds = function(year, month, cb) {
    logger.debug("Requesting Funds in a given month.");
    let startOfMonth = moment(`${year}-${(month % 12) + 1}-01`, "YYYY-MM-DD");
    // endDate = 00:00  the first day of the following month minus one second
    let endOfMonth = startOfMonth.clone();
    endOfMonth.add(1, "month");
    endOfMonth.subtract(1, "second");
    return Regularfund.getGroupedRange(startOfMonth, endOfMonth, cb);
  };

  /**
   * Returns all RegularFunds that will occur between two dates
   * @param {date} from moment start date of the range
   * @param {date} to moment last date of the range
   * @param {callback} cb callback fn
   */
  Regularfund.getGroupedRange = function(from, to, cb) {
    let now = moment();
    let calculatedOccurrences = [];
    // early exit für requests in der vergangenheit
    if (to.isBefore(now)) {
      cb(null, calculatedOccurrences);
      return;
    }
    const rangeFilter = {
      where: {
        and: [
          { firstBooking: { lte: to } },
          {
            or: [{ lastBooking: { gte: from } }, { lastBooking: null }]
          }
        ]
      },
      order: ["description ASC"]
    };
    Regularfund.find(rangeFilter, (err, regFunds) => {
      if (err) {
        logger.error(err);
        return cb(err);
      }
      regFunds.forEach(regFund => {
        let occurrency = getRegularFundOccurrences(regFund, from, to);
        if (occurrency) {
          calculatedOccurrences.push(occurrency);
        }
      });
      cb(null, calculatedOccurrences);
    });
  };

  /**
   * Returns for a single RegularFund all dates when it will occur between two dates
   * @param {Regularfund} regularFund regular fund to test
   * @param {date} startOfRange moment start date of the range
   * @param {date} endOfRange last date of the range
   * @returns {Object} Regularfund als fund mit date array und textueller frequenz
   */
  function getRegularFundOccurrences(regularFund, startOfRange, endOfRange) {
    try {
      let now = moment();
      // liegt das aktuelle datum innerhalb des zeitraums berechne erst ab heute
      let startDate = now.isAfter(startOfRange) ? now : startOfRange.clone();

      let lastDate = endOfRange.clone();
      // wenn letzte buchung existiert und vor ende des zeitraums ist, nur bis dahin
      if (regularFund.lastBooking) {
        let lastBooking = moment(regularFund.lastBooking);
        if (lastBooking.isBefore(endOfRange)) {
          lastDate = lastBooking;
        }
      }

      var occurrency = {
        value: regularFund.value,
        dates: [],
        id: regularFund.id,
        description: regularFund.description,
        categoryId: regularFund.categoryId,
        counter: 0,
        frequenzy: frequency(regularFund.frequenzy)
      };
      let firstDate = moment(regularFund.firstBooking);
      switch (occurrency.frequenzy) {
        case "quaterYearly":
        case "halfYearly": {
          let monthDiff = 3;
          if (occurrency.frequenzy == "halfYearly") {
            monthDiff = 6;
          }
          // buchungstag (1-31) im zeitraum bereits abgelaufen
          if (firstDate.date() <= startDate.date()) {
            break;
          }
          if ((firstDate.month() - startDate.month()) % monthDiff === 0) {
            occurrency.counter = 1;
            let nextDate = firstDate
              .year(startDate.year())
              .month(startDate.month());
            if (!nextDate.isAfter(lastDate)) {
              occurrency.dates.push(nextDate);
            }
          }
          break;
        }
        default:
          if (firstDate.isAfter(startDate)) {
            startDate = moment(regularFund.firstBooking).startOf("day");
          }
          // damit alle Einträge >= Anfang des Monats gefunden werden
          startDate.subtract(1, "second");
          var options = {
            currentDate: startDate.toDate(),
            endDate: lastDate.toDate(),
            tz: "Europe/Berlin"
          };
          var interval = parser.parseExpression(regularFund.frequenzy, options);
          while (interval.hasNext()) {
            try {
              var obj = interval.next();
              // enthält umwandlung in neuere moment.js version für vergleichsoperationen
              if (startDate.isSameOrAfter(moment(obj.toDate()))) {
                continue;
              }
              occurrency.counter += 1;
              occurrency.dates.push(obj.toDate());
            } catch (e) {
              console.error(e);
              break;
            }
          }
      }
      if (occurrency.counter > 0) {
        occurrency.sum = occurrency.value * occurrency.counter;
        return occurrency;
      }
    } catch (e) {
      console.log(e);
    }
    return;
  }

  Regularfund.remoteMethod("getGroupedMonthlyFunds", {
    http: {
      path: "/monthlyGrouped",
      verb: "get"
    },
    accepts: [
      { arg: "year", type: "number" },
      { arg: "month", type: "number" }
    ],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  /**
   * Returns all RegularFunds that will occur between two dates in a given category
   * @param {number} categoryId
   * @param {date} from start date of the range
   * @param {date} to last date of the range
   * @param {callback} cb callback fn
   */
  Regularfund.getRangeCategory = function(categoryId, from, to, cb) {
    var calculatedOccurrences = [];
    // early exit für requests in der vergangenheit
    if (new Date() - new Date(to) > 0) {
      cb(null, calculatedOccurrences);
      return;
    }
    var rangeCategoryFilter = {
      where: {
        and: [
          {
            and: [
              { firstBooking: { lte: to } },
              { or: [{ lastBooking: { gte: from } }, { lastBooking: null }] }
            ]
          },
          { categoryId: categoryId }
        ]
      }
    };
    Regularfund.find(rangeCategoryFilter, (err, regFunds) => {
      if (err) {
        logger.error(err);
        return cb(err);
      }
      regFunds.forEach(el => {
        try {
          var options = {
            currentDate: new Date(Math.max(from, new Date())),
            endDate: new Date(Math.min(to, el.lastBooking)),
            tz: "Europe/Berlin"
          };

          var interval = parser.parseExpression(el.frequenzy, options);
          while (interval.hasNext()) {
            var obj = interval.next();
            var occurrency = {
              value: el.value,
              date: new Date(obj.value.toString()),
              id: el.id,
              description: el.description,
              categoryId: el.categoryId
            };
            calculatedOccurrences.push(occurrency);
          }
        } catch (err) {
          logger.error("Error: " + err.message);
        }
      });
      cb(null, calculatedOccurrences);
    });
  };

  Regularfund.remoteMethod("getRangeCategory", {
    http: {
      path: "/rangeCategory",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "from", type: "date", required: true },
      { arg: "to", type: "date", required: true }
    ],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  /**
   * Returns all Regularfund occurences in a given month in a given category
   * @param {number} categoryId
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Regularfund.getMonthlyFundsCategory = function(categoryId, year, month, cb) {
    var startDate = new Date(year, month, 1, 0, 0, 0, 0);
    // endDate = 00:00  the first day of the following month minus one second
    var endDate = new Date(startDate);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setSeconds(endDate.getSeconds() - 1);
    return Regularfund.getRangeCategory(categoryId, startDate, endDate, cb);
  };

  Regularfund.remoteMethod("getMonthlyFundsCategory", {
    http: {
      path: "/monthlyCategory",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "year", type: "number", required: true },
      { arg: "month", type: "number", required: true }
    ],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  /**
   * Returns all RegularFunds with a given frequency
   * @param { boolean } includePassed include funds which do not occure anymore
   * @param { boolean } includeFuture include funds which did not occure yet
   * @param { string } frequency daily, weekly, etc.
   * @param { callback } cb callback fn
   */
  Regularfund.getByFrequenzy = function(
    includePassed,
    includeFuture,
    frequency,
    cb
  ) {
    const matchingFrequencyRegex = frequencyRegex[frequency];
    if (!matchingFrequencyRegex) {
      logger.error(`Regularfund requested with wrong frequency "${frequency}"`);
      return cb(
        new HttpErrors.BadRequest(
          `Only ${Object.keys(frequencyRegex).reduce(
            (prev, cur) => prev + cur + ", ",
            ""
          )} allowed as frequency!`
        )
      );
    }
    logger.debug(
      `Requesting RegularFunds by frequency: ${frequency} ${
        includeFuture ? "including" : "excluding"
      } future and ${includePassed ? "including" : "excluding"} passed`
    );
    const frequencyFilter = { frequenzy: { regexp: matchingFrequencyRegex } };
    let additionalFilter = [];
    if (!includeFuture) {
      additionalFilter.push({ firstBooking: { lte: new Date() } });
    }
    if (!includePassed) {
      additionalFilter.push({
        or: [{ lastBooking: { gte: new Date() } }, { lastBooking: null }]
      });
    }
    let cronFilter =
      additionalFilter.length === 0
        ? { where: frequencyFilter, include: "category" }
        : additionalFilter.length === 1
        ? {
            where: { and: [additionalFilter[0], frequencyFilter] },
            include: "category"
          }
        : {
            where: {
              and: [
                { and: [additionalFilter[0], frequencyFilter] },
                additionalFilter[1]
              ]
            },
            include: "category"
          };
    Regularfund.find(cronFilter, (err, regFunds) => {
      if (err) {
        logger.error(err);
        return cb(new HttpErrors.InternalServerError());
      }
      cb(null, regFunds);
    });
  };

  Regularfund.remoteMethod("getByFrequenzy", {
    http: {
      path: "/frequency",
      verb: "get"
    },
    accepts: [
      { arg: "includePassed", type: "boolean" },
      { arg: "includeFuture", type: "boolean" },
      { arg: "frequency", type: "string" }
    ],
    returns: {
      arg: "regularfunds",
      type: "array"
    }
  });

  function frequency(cronstring) {
    if (cronstring.match(frequencyRegex.daily)) return "daily";
    if (cronstring.match(frequencyRegex.weekly)) return "weekly";
    if (cronstring.match(frequencyRegex.monthly)) return "monthly";
    if (cronstring.match(frequencyRegex.quarterYearly)) return "quaterYearly";
    if (cronstring.match(frequencyRegex.halfYearly)) return "halfYearly";
    if (cronstring.match(frequencyRegex.yearly)) return "yearly";
    return "other";
  }
  /**
   * Returns returns for all regularFunds that will occur between two dates: value of a single booking, next date, sum value of open bookings and number of future occurences
   * @param {date} from start date of the range
   * @param {date} to last date of the range
   * @param {callback} cb callback fn
   */
  Regularfund.getAggregatedRange = function(from, to, cb) {
    console.log("Requesting aggregated RegularFunds in a date Range");
    let calculatedOccurrences = [];
    // early exit für requests in der vergangenheit
    if (new Date() - new Date(to) > 0) {
      cb(null, []);
      return;
    }

    var rangeFilter = {
      where: {
        and: [
          { firstBooking: { lte: to } },
          { or: [{ lastBooking: { gte: from } }, { lastBooking: null }] }
        ]
      }
    };
    Regularfund.find(rangeFilter, (err, regFunds) => {
      if (err) {
        console.error(err);
        return cb(err);
      }

      // damit alle Einträge >= Anfang des Monats gefunden werden
      let startDate = (moment().isAfter(moment(from))
        ? moment()
        : moment(from)
      ).subtract(1, "second");

      regFunds.forEach(el => {
        try {
          let counter = 0;

          var options = {
            currentDate: startDate.toDate(),
            endDate: el.lastBooking
              ? new Date(Math.min(to, el.lastBooking))
              : new Date(to),
            tz: "Europe/Berlin"
          };
          let nextDate;
          var interval = parser.parseExpression(el.frequenzy, options);
          if (interval.hasNext()) {
            let obj = interval.next();
            nextDate = obj.toDate();
            counter + 1;
          } else {
            return;
          }
          while (interval.hasNext()) {
            interval.next();
            counter += 1;
          }
          counter += 1;
          calculatedOccurrences.push({
            id: el.id,
            value: el.value,
            counter: counter,
            sum: counter * el.value,
            categoryId: el.categoryId,
            date: nextDate
          });
        } catch (err) {
          console.error("Error: " + err.message);
        }
      });
      cb(null, calculatedOccurrences);
    });
  };
};
