"use strict";
const logger = require("../../logger.js");
module.exports = function(Category) {
  /**
   *Gibt die in einem angegebenen Zeitraum auftretenden Kategorien zurück, inkl. ihrer Summen der Ausgaben
   * @param {date} from Startddatum des Intervalls
   * @param {date} to Enddatum des Intervalls
   * @param {int} [limit] Anzahl der zurückgegebenen Kategorien, absteigend geordnet nach Summe
   */
  Category.getExpensesInRange = function(from, to, limit, cb) {
    logger.debug("Requesting category expenses in a date range");
    const Fund = Category.app.models.Fund;
    const rangeFilter = {
      where: {
        and: [
          { date: { gte: from } },
          { date: { lte: to } },
          { value: { lt: 0 } },
          { categoryId: { gt: 0 } }
        ]
      },
      include: "category"
    };
    Fund.find(rangeFilter, (err, funds) => {
      if (err) {
        cb(err);
      } else {
        let categoryMap = new Map();
        for (let index = 0; index < funds.length; index++) {
          const fund = funds[index];
          let name = funds[index].category().name;
          if (!categoryMap.has(name)) {
            categoryMap.set(name, -fund.value);
          } else {
            categoryMap.set(name, categoryMap.get(name) - fund.value);
          }
        }
        let categories = Array.from(categoryMap.entries());
        categories.sort((c1, c2) => c2[1] - c1[1]); // absteigend nach summe
        if (limit) categories = categories.slice(0, limit);
        categories = categories.map(c => {
          return { name: c[0], sum: c[1] };
        });
        cb(null, categories);
      }
    });
  };

  Category.remoteMethod("getExpensesInRange", {
    http: {
      path: "/expensesInRange",
      verb: "get"
    },
    accepts: [
      { arg: "from", type: "date" },
      { arg: "to", type: "date" },
      { arg: "limit", type: "number" }
    ],
    returns: {
      arg: "categories",
      type: "array"
    }
  });
};
