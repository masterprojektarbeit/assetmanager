"use strict";

module.exports = function(CategoryColor) {
  const config = require("../config.json");
  const logger = require("../../logger.js");
  /**
   * initialize the color cube.
   */
  function initColorList() {
    var colorList = [];
    for (var z = 0; z < 2; z++) {
      for (var y = 0; y < 2; y++) {
        for (var x = 0; x < 2; x++) {
          colorList.push([x * 255, y * 255, z * 255]);
        }
      }
    }
    return colorList;
  }

  /**
   * calculate a new set of distinct colors and append it to the color list.
   */
  function calcColors(colorList) {
    // half each partial cube in the color list
    // the resulting vertices represent the new colors
    var tmpList = colorList.slice();
    tmpList.forEach(v => {
      for (var z = -0.5; z <= 0.5; z += 0.5) {
        for (var y = -0.5; y <= 0.5; y += 0.5) {
          for (var x = -0.5; x <= 0.5; x += 0.5) {
            // round the half vertices
            var vx = ~~(v[0] + v[0] * x);
            var vy = ~~(v[1] + v[1] * y);
            var vz = ~~(v[2] + v[2] * z);

            // only accept a color, if it is in the valid range
            if (
              vx >= 0 &&
              vx <= 255 &&
              vy >= 0 &&
              vy <= 255 &&
              vz >= 0 &&
              vz <= 255
            ) {
              var newColor = [vx, vy, vz];
              if (
                !colorList.some(
                  cv =>
                    cv[0] == newColor[0] &&
                    cv[1] == newColor[1] &&
                    cv[2] == newColor[2]
                )
              ) {
                // check color distance
                if (
                  colorList.every(
                    c =>
                      colorDistance(c, newColor) >
                      config.categoryColors.minimumDistance
                  )
                ) {
                  colorList.push(newColor);
                }
              }
            }
          }
        }
      }
    });
  }

  /**
   * calculate the euclidean color distance of two colors
   */
  function colorDistance(color1, color2) {
    var r = (color1[0] + color2[0]) * 0.5;
    var dr = color1[0] - color2[0];
    var dg = color1[1] - color2[1];
    var db = color1[2] - color2[2];

    return Math.sqrt(
      2 * dr * dr +
        4 * dg * dg +
        3 * db * db +
        (r * (dr * dr - db * db)) / 256.0
    );
  }

  /**
   * calculate new colors until there are enough for all categories.
   */
  async function calcNewColors(categoryCount) {
    var catColors = [];
    // map database colors to rgb arrays
    (await CategoryColor.find()).forEach(c => {
      catColors.push([
        ((c.colorcode << 8) >> 24) & 255,
        ((c.colorcode << 16) >> 24) & 255,
        ((c.colorcode << 24) >> 24) & 255
      ]);
    });

    var colorList = initColorList();
    var distincts;
    var loops = 0;
    // calculate colors until there are enough
    do {
      calcColors(colorList);

      // filter distinct colors
      distincts = colorList.filter(c => {
        return !catColors.some(c2 => {
          return c[0] == c2[0] && c[1] == c2[1] && c[2] == c2[2];
        });
      });
      loops++;
    } while (
      distincts.length + catColors.length <
        categoryCount + config.categoryColors.blacklist.length ||
      loops == 50 // to prevent infinite loop
    );

    // since the loop can fail (not enough new colors after n cycles),
    // return null (fail message) to requester
    if (
      loops == 50 &&
      distincts.length + catColors.length <
        categoryCount + config.categoryColors.blacklist.length
    ) {
      return null;
    }

    // persist all new (distinct) colors
    var distinctCatColors = [];
    for (let dColor of distincts) {
      var dColorCode = (dColor[0] << 16) | (dColor[1] << 8) | dColor[2]; // rgb to int
      var catCol = await CategoryColor.create({
        id: null,
        categoryId: config.categoryColors.blacklist.includes(dColorCode)
          ? -2
          : null,
        colorcode: dColorCode
      });
      distinctCatColors.push(catCol);
    }
    return distinctCatColors.find(c => {
      return !c.categoryId;
    });
  }

  /**
   * helper function to parse an int color to chart rgb string.
   */
  function parseRgbString(colorCode) {
    var r = ((colorCode << 8) >> 24) & 255;
    var g = ((colorCode << 16) >> 24) & 255;
    var b = ((colorCode << 24) >> 24) & 255;
    return "rgb(" + r + "," + g + "," + b + ")";
  }

  /**
   * chain a set of promises to be executed sequentially.
   */
  async function chainPromises(promises) {
    var results = [];
    for (let promise of promises) {
      // must be for (.. of ..)
      results.push(await promise());
    }
    return results;
  }

  /**
   * Returns a color for a category. If no color is defined, set a new one.
   */
  CategoryColor.getColor = async function(catId, categoryCount) {
    var catColor = await CategoryColor.findOne({
      where: { categoryId: catId }
    });
    if (catColor) {
      return [catId, parseRgbString(catColor.colorcode)];
    }

    var freeColor = await CategoryColor.findOne({
      where: { categoryId: null }
    });
    if (freeColor) {
      freeColor.categoryId = catId;
      await freeColor.save();
      return [catId, parseRgbString(freeColor.colorcode)];
    }

    // if no category color is set and there is no free color available,
    // calculate new free colors
    var newColor = await calcNewColors(categoryCount);
    if (newColor == null) {
      return null;
    }
    newColor.categoryId = catId;
    await newColor.save();
    return [catId, parseRgbString(newColor.colorcode)];
  };

  /**
   * Returns the colors for all categories. If a color is not defined, set a new one.
   * If not enough colors could be generated or something went wrong while generating,
   * return null.
   */
  CategoryColor.getColors = function(cb) {
    try {
      const Category = CategoryColor.app.models.Category;
      Category.find((err, categories) => {
        var funcs = [];
        categories.forEach(cat => {
          funcs.push(
            () =>
              new Promise(res => {
                var c = CategoryColor.getColor(cat.id, categories.length);
                res(c);
              })
          );
        });
        chainPromises(funcs).then(res => {
          if (
            res.some(c => {
              return c == null;
            })
          ) {
            logger.error("Error on calculating category colors!");
            cb(null, null);
          } else {
            var sortedColors = res
              .sort(function(a, b) {
                return parseInt(a[0]) - parseInt(b[0]);
              })
              .map(function(s) {
                return s[1];
              });

            sortedColors.push(
              parseRgbString(config.categoryColors.miscCategory)
            );
            cb(null, sortedColors);
          }
        });
      });
    } catch (err) {
      logger.error(err);
      cb(null, null);
    }
  };

  CategoryColor.remoteMethod("getColors", {
    http: {
      path: "/getColors",
      verb: "get"
    },
    accepts: [],
    returns: {
      arg: "colors",
      type: "array"
    }
  });
};
