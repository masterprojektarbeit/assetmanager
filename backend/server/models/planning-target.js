"use strict";
var logger = require("../../logger.js");
module.exports = function(Planningtarget) {
  Planningtarget.validatesUniquenessOf("dueDate", {
    message: "dueDate is not unique"
  });

  var app = require("../server");
  /**
   * Returns all Planningtargets that are due on or after the given Date
   * @param {date} date start date of the range
   * @param {callback} cb callback fn
   */
  Planningtarget.getAfterDate = function(date, cb) {
    logger.debug("Requesting Planningtargets after a given Date");
    var dt = new Date(date);
    logger.debug(date);
    var dateFilter = {
      dueDate: { gte: dt }
    };
    var orderFilter = "dueDate ASC";
    var filter = {
      where: dateFilter,
      order: orderFilter
    };
    Planningtarget.find(filter)
      .then(targets => {
        cb(null, targets);
      })
      .catch(function(error) {
        cb(error);
      });
  };

  Planningtarget.remoteMethod("getAfterDate", {
    http: {
      path: "/afterDate",
      verb: "get"
    },
    accepts: [{ arg: "date", type: "date" }],
    returns: {
      arg: "targets",
      type: "array"
    }
  });

  /**
   * Returns all Planningtargets that are due before the given Date
   * @param {date} date first date after the range
   * @param {callback} cb callback fn
   */
  Planningtarget.getBeforeDate = function(date, cb) {
    logger.debug("Requesting Planningtargets before a given Date");
    logger.debug(date);
    var dateFilter = { dueDate: { lt: date } };
    var orderFilter = "dueDate DESC";
    var filter = { where: dateFilter, order: orderFilter };
    Planningtarget.find(filter)
      .then(targets => {
        addFulfillment(targets, cb);
      })
      .catch(function(error) {
        cb(error);
      });
  };

  Planningtarget.remoteMethod("getBeforeDate", {
    http: {
      path: "/beforeDate",
      verb: "get"
    },
    accepts: [{ arg: "date", type: "date" }],
    returns: {
      arg: "targets",
      type: "array"
    }
  });

  /**
   * Returns all Planningtargets that are due before the given Date
   * @param {date} date first date after the range
   * @param {callback} cb callback fn
   */
  Planningtarget.getBalance = function(id, cb) {
    logger.debug("Requesting Planningtargets before a given Date");
    calculateBalance(id, cb);
  };

  Planningtarget.remoteMethod("getBalance", {
    http: {
      path: "/balance",
      verb: "get"
    },
    accepts: [{ arg: "id", type: "number" }],
    returns: {
      arg: "balance",
      type: "number"
    }
  });

  /**
   * Evaluiert Bilanz inklusive zukünftiger regelmäßiger Geldströme für ein Planziel
   *
   * @param {number} id planningtargets that will be enrichted
   * @param {callback} cb callback fn
   */
  function calculateBalance(id, cb) {
    if (!id || Number.isNaN(id) || +id <= 0) {
      cb("No targets given");
      return;
    }
    let ret = {
      balance: 0,
      regularFundsSum: 0,
      fundsSum: 0
    };
    Planningtarget.findOne({ where: { id: id } })
      .then(pt => {
        let requestRegularFunds;
        if (pt.dueDate > new Date() && pt.dueDate > pt.startDate) {
          requestRegularFunds = regularFundsRangePromise(pt).then(
            regularFunds =>
              (ret.regularFundsSum = arrayFieldSum(
                regularFunds,
                item => item.sum
              ))
          );
        }
        let requestFunds = fundsRangePromise(pt).then(funds => {
          ret.fundsSum = arrayFieldSum(funds, item => item.value);
        });
        return [requestFunds, requestRegularFunds];
      })
      .then(async response => {
        try {
          const res = await Promise.all(response);
          ret.balance = (+(+ret.regularFundsSum + +ret.fundsSum)).toFixed(2);
          cb(null, ret.balance);
        } catch (err) {
          cb(err);
        }
      });
  }

  /**
   * Adds information about the fulfilment of the planningtargets
   *
   * @param {array} targets planningtargets that will be enrichted
   * @param {callback} cb callback fn
   */
  function addFulfillment(targets, cb) {
    if (!targets || targets.size == 0) {
      cb("No targets given");
    }
    targets.map(item => {
      item.balance = 0;
      item.regularFundsSum = 0;
      item.fundsSum = 0;
    });
    let promises = [];
    let requestRegularFunds = getRegularFundsForTargets(targets);
    let requestFunds = getFundsForTarget(targets);
    promises.push(...requestFunds);
    promises.push(...requestRegularFunds);
    Promise.all(promises).then(
      () => {
        targets.map(
          item =>
            (item.balance = (+(+item.regularFundsSum + +item.fundsSum)).toFixed(
              2
            ))
        );
        cb(null, targets);
      },
      function(err) {
        cb(err);
      }
    );
  }

  function regularFundsRangePromise(planningtarget) {
    var Regularfund = app.models.RegularFund;
    return new Promise(function(resolve, reject) {
      Regularfund.getAggregatedRange(
        planningtarget.startDate,
        planningtarget.dueDate,
        function(err, data) {
          return err ? reject(err) : resolve(data);
        }
      );
    });
  }

  function getRegularFundsForTargets(plannigtargets) {
    return plannigtargets
      .filter(
        item => item.dueDate > new Date() && item.dueDate > item.startDate
      )
      .map(async item => {
        const regularFunds = await regularFundsRangePromise(item);
        return (item.regularFundsSum = arrayFieldSum(
          regularFunds,
          item => item.sum
        ));
      });
  }

  function fundsRangePromise(planningtarget) {
    var Fund = app.models.Fund;
    return new Promise(function(resolve, reject) {
      Fund.getRange(
        new Date(planningtarget.startDate),
        new Date(planningtarget.dueDate),
        function(err, data) {
          return err ? reject(err) : resolve(data);
        }
      );
    });
  }

  function getFundsForTarget(plannigtargets) {
    return plannigtargets
      .filter(item => item.dueDate > item.startDate)
      .map(async item => {
        const funds = await fundsRangePromise(item);
        item.fundsSum = arrayFieldSum(funds, item => item.value);
      });
  }

  function arrayFieldSum(arr, fieldGetter) {
    return arr
      .map(f => +fieldGetter(f))
      .reduce((acc, cur) => acc + cur, 0)
      .toFixed(2);
  }

  /**
   * Returns the active Planningtarget with the nearest due date after startDate
   * @param {date} startDate date
   * @param {callback} cb callback fn
   */
  Planningtarget.getNextDueTarget = function(startDate, cb) {
    logger.debug("Requesting next one Planningtarget after a given Date");
    var dateFilter = {
      and: [{ dueDate: { gt: startDate } }, { startDate: { lte: startDate } }]
    };
    var orderFilter = "dueDate ASC";
    var filter = { where: dateFilter, order: orderFilter };
    Planningtarget.findOne(filter)
      .then(target => {
        if (target == null) {
          // keines existiert mit passenden daten
          cb(null, target);
          return Promise.resolve();
        }
        addFulfillment([target], cb);
      })
      .catch(function(error) {
        cb(error);
      });
  };

  Planningtarget.remoteMethod("getNextDueTarget", {
    http: {
      path: "/nextDueTarget",
      verb: "get"
    },
    accepts: [{ arg: "startDate", type: "date" }],
    returns: {
      arg: "targets",
      type: "array"
    }
  });
};
