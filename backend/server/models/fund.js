"use strict";
var logger = require("../../logger.js");
const app = require("../server.js");
const recommend = require("../helper/recommend");
module.exports = function(Fund) {
  /**
   * Returns all Funds between two dates
   * @param {date} from start date of the range
   * @param {date} to last date of the range
   * @param {callback} cb callback fn
   */
  Fund.getRange = function(from, to, cb) {
    logger.debug("Requesting Funds in a date Range");
    var rangeFilter = {
      where: { and: [{ date: { gte: from } }, { date: { lte: to } }] }
    };
    Fund.find(rangeFilter, (err, funds) => {
      if (err) return cb(err);
      cb(null, funds);
    });
  };

  Fund.remoteMethod("getRange", {
    http: {
      path: "/range",
      verb: "get"
    },
    accepts: [{ arg: "from", type: "date" }, { arg: "to", type: "date" }],
    returns: {
      arg: "funds",
      type: "array"
    }
  });

  /**
   * Returns all Funds in a given month
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Fund.getMonthlyFunds = function(year, month, cb) {
    logger.debug("Requesting Funds in a given month.");
    var startDate = new Date(year, month, 1, 0, 0, 0, 0);
    // endDate = 00:00  the first day of the following month minus one second
    var endDate = new Date(startDate);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setSeconds(endDate.getSeconds() - 1);
    return Fund.getRange(startDate, endDate, cb);
  };

  Fund.remoteMethod("getMonthlyFunds", {
    http: {
      path: "/monthly",
      verb: "get"
    },
    accepts: [
      { arg: "year", type: "number" },
      { arg: "month", type: "number" }
    ],
    returns: {
      arg: "funds",
      type: "array"
    }
  });

  /**
   * Splits Fund into multiple separate Funds
   * @param {date} from start date of the range
   * @param {date} to last date of the range
   * @param {callback} cb callback fn
   */
  Fund.split = function(id, newFunds, cb) {
    Fund.asyncSplit(id, newFunds, cb);
  };

  Fund.asyncSplit = async (id, newFunds, cb) => {
    try {
      await app.dataSources.db.transaction(async models => {
        const { Fund } = models;
        for (let i = 0; i < newFunds.length; i++) {
          await Fund.create(newFunds[i], function(err, res) {
            if (!err) logger.info("Created: " + res.id);
            else logger.error(err);
          });
        }
        await Fund.destroyById(id, function(err, res) {
          if (!err) logger.info("Deleted: " + id);
          else logger.error(err);
        });
        cb(null, true);
      });
    } catch (e) {
      logger.error(e);
      cb(false);
    }
  };

  Fund.remoteMethod("split", {
    http: {
      path: "/split",
      verb: "post"
    },
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true
      },
      {
        arg: "funds",
        type: "array",
        required: true
      }
    ],
    returns: {
      arg: "status",
      type: "bool"
    }
  });

  /**
   * Returns all Funds in a given category between two dates
   * @param { number } categoryId
   * @param { date } from start date of the range
   * @param { date } to last date of the range
   * @param { callback } cb callback fn
   */
  Fund.getRangeCategory = function(categoryId, from, to, cb) {
    var rangeCategoryFilter = {
      where: {
        and: [
          { and: [{ date: { gte: from } }, { date: { lte: to } }] },
          { categoryId: categoryId }
        ]
      }
    };
    Fund.find(rangeCategoryFilter, (err, funds) => {
      if (err) return cb(err);
      cb(null, funds);
    });
  };

  Fund.remoteMethod("getRangeCategory", {
    http: {
      path: "/rangeCategory",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "from", type: "date", required: true },
      { arg: "to", type: "date", required: true }
    ],
    returns: {
      arg: "funds",
      type: "array"
    }
  });

  /**
   * Returns all Funds in a given category in a given month
   * @param {number} categoryId
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Fund.getMonthlyFundsCategory = function(categoryId, year, month, cb) {
    var startDate = new Date(year, month, 1, 0, 0, 0, 0);
    // endDate = 00:00  the first day of the following month minus one second
    var endDate = new Date(startDate);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setSeconds(endDate.getSeconds() - 1);
    return Fund.getRangeCategory(categoryId, startDate, endDate, cb);
  };

  Fund.remoteMethod("getMonthlyFundsCategory", {
    http: {
      path: "/monthlyCategory",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "year", type: "number", required: true },
      { arg: "month", type: "number", required: true }
    ],
    returns: {
      arg: "funds",
      type: "array"
    }
  });

  /**
   * Returns Sum of Expenses of Funds in a given category
   * @param {number} categoryId
   * @param {callback} cb callback fn
   */
  Fund.getSumCategoryExpense = function(categoryId, cb) {
    let categoryExpense = new Promise((resolve, reject) => {
      var categoryExpenseFilter = {
        where: {
          and: [{ categoryId: categoryId }, { value: { lt: 0 } }]
        }
      };
      Fund.find(categoryExpenseFilter, (err, funds) => {
        if (err) return reject(err);
        resolve(funds);
      });
    });
    categoryExpense
      .then(funds =>
        cb(
          null,
          funds.reduce((expense, fund) => expense + parseFloat(fund.value), 0)
        )
      )
      .catch(error => logger.error(error));
  };

  Fund.remoteMethod("getSumCategoryExpense", {
    http: {
      path: "/sumCategoryExpense",
      verb: "get"
    },
    accepts: [{ arg: "categoryId", type: "number", required: true }],
    returns: {
      arg: "expenseSum",
      type: "number"
    }
  });

  /**
   * Returns Sum of Expenses of Funds in a given category in a given month
   * @param {number} categoryId
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Fund.getSumCategoryMonthExpense = function(categoryId, year, month, cb) {
    let newCallback = function(error, funds) {
      if (error) cb(error);
      var categoryMonthExpenseFunds = funds.filter(fund => fund.value < 0);
      cb(
        null,
        categoryMonthExpenseFunds.reduce(
          (expense, fund) => expense + parseFloat(fund.value),
          0
        )
      );
    };
    Fund.getMonthlyFundsCategory(categoryId, year, month, newCallback);
  };

  Fund.remoteMethod("getSumCategoryMonthExpense", {
    http: {
      path: "/sumCategoryMonthExpense",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "year", type: "number", required: true },
      { arg: "month", type: "number", required: true }
    ],
    returns: {
      arg: "expenseSum",
      type: "number"
    }
  });

  /**
   * Returns Sum of Incomes of Funds in a given category
   * @param {number} categoryId
   * @param {callback} cb callback fn
   */
  Fund.getSumCategoryIncome = function(categoryId, cb) {
    let categoryIncome = new Promise((resolve, reject) => {
      var categoryIncomeFilter = {
        where: {
          and: [{ categoryId: categoryId }, { value: { gt: 0 } }]
        }
      };
      Fund.find(categoryIncomeFilter, (err, funds) => {
        if (err) return reject(err);
        resolve(funds);
      });
    });
    categoryIncome
      .then(funds =>
        cb(
          null,
          funds.reduce((income, fund) => income + parseFloat(fund.value), 0)
        )
      )
      .catch(error => logger.error(error));
  };

  Fund.remoteMethod("getSumCategoryIncome", {
    http: {
      path: "/sumCategoryIncome",
      verb: "get"
    },
    accepts: [{ arg: "categoryId", type: "number", required: true }],
    returns: {
      arg: "incomeSum",
      type: "number"
    }
  });

  /**
   * Returns Sum of Incomes of Funds in a given category in a given month
   * @param {number} categoryId
   * @param {number} year yyyy four digit year A.D.
   * @param {number} month month id (0-11 as per javascript standard)
   * @param {callback} cb callback fn
   */
  Fund.getSumCategoryMonthIncome = function(categoryId, year, month, cb) {
    let newCallback = function(error, funds) {
      if (error) cb(error);
      var categoryMonthIncomeFunds = funds.filter(fund => fund.value > 0);
      cb(
        null,
        categoryMonthIncomeFunds.reduce(
          (income, fund) => income + parseFloat(fund.value),
          0
        )
      );
    };
    Fund.getMonthlyFundsCategory(categoryId, year, month, newCallback);
  };

  Fund.remoteMethod("getSumCategoryMonthIncome", {
    http: {
      path: "/sumCategoryMonthIncome",
      verb: "get"
    },
    accepts: [
      { arg: "categoryId", type: "number", required: true },
      { arg: "year", type: "number", required: true },
      { arg: "month", type: "number", required: true }
    ],
    returns: {
      arg: "incomeSum",
      type: "number"
    }
  });

  /**
   * Imports fund data based on the string of a csv file in format:
   * id;value;description;date;categoryId;favoriteId
   */

  function createNew(id, val, desc, date, catId, favId) {
    try {
      var fund = Fund.create({
        id: id,
        value: val,
        description: desc,
        date: date,
        categoryId: catId,
        favoriteId: favId
      });
      if (fund === null) return false;
      else return true;
    } catch (exception) {
      console.log(exception);
      return false;
    }
  }
  function interpret(id, val, desc, date, catId, favId) {
    var outString =
      id +
      ";" +
      val +
      ';"' +
      desc +
      '";' +
      date +
      ";" +
      catId +
      ";" +
      favId +
      " ";

    if (id === "") {
      return new Promise((resolve, reject) => {
        if (createNew(null, val, desc, date, catId, favId)) {
          console.log(outString + "(Imported)");
          resolve(0);
        } else {
          console.log(outString + "(Error)");
          resolve(2);
        }
      });
    } else {
      var filter = {
        where: {
          or: [
            { id: id },
            {
              and: [{ value: val }, { description: desc }, { date: date }]
            }
          ]
        }
      };
      return Fund.find(filter)
        .then(f => {
          if (f.length == 0) {
            if (createNew(id, val, desc, date, catId, favId)) {
              console.log(outString + "(Imported)");
              return 0;
            } else {
              console.log(outString + "(Error)");
              return 2;
            }
          } else {
            console.log(
              outString + "(Duplicate: [" + f.map(x => x.id).join() + "])"
            );
            return 1;
          }
        })
        .catch(reason => {
          console.log("Rejection in find: " + reason);
          return 2;
        });
    }
  }

  Fund.import = function(fd, cb) {
    console.log("Received Import Data:");
    const regex = /(\d*);([+-]?\d+(\.\d+)*);([^;]+);(\d+(\.\d+)*);(\d*);(\d*)/;
    var match;
    var promises = [];
    var noMatches = [];
    var sameIds = [];
    var lineIndex = 0;
    var ids = [];
    fd.split("\n").forEach(function(line) {
      // console.log('try: "' + line + '"');
      if ((match = regex.exec(line)) !== null) {
        try {
          var matchId = match[1];
          if (matchId !== "" && ids.includes(matchId)) {
            sameIds.push(matchId);
            console.log(line + "(Duplicate: [" + matchId + "])");
          } else {
            if (matchId !== "") ids.push(matchId);
            var matchValue = match[2];
            var matchDesc = match[4].replace(/%3b/i, ";");
            var matchDate = new Date(match[5] * 1000);
            var matchCat = match[7];
            var matchFav = match[8];

            var interpretRes = interpret(
              matchId,
              matchValue,
              matchDesc,
              matchDate,
              matchCat,
              matchFav
            );
            promises.push(interpretRes);
          }
        } catch (exception) {
          console.log('Error while importing "' + match[0] + '"');
          noMatches.push(lineIndex);
        }
      } else if (
        line.length > 0 &&
        line !== "id;value;description;date;categoryId;favoriteId"
      ) {
        console.log(line + "(No Regex Match)");
        noMatches.push(lineIndex);
      }
      lineIndex++;
    });

    Promise.all(promises).then(res => {
      var imported = res.filter(x => x === 0).length;
      var discarded = res.filter(x => x === 1).length + sameIds.length;
      var errorLines = res
        .filter(x => x === 2)
        .concat(noMatches)
        .map(Number)
        .sort();
      var errorCount = errorLines.length;
      console.log("Imported: " + imported);
      console.log("Discarded: " + discarded);
      console.log("Error in Lines: [" + errorLines + "]");
      cb(null, [imported, discarded, errorLines, errorCount]);
    });
  };
  Fund.remoteMethod("import", {
    http: {
      path: "/import",
      verb: "post"
    },
    accepts: [{ arg: "fd", type: "String" }],
    returns: {
      arg: "status",
      type: "array"
    }
  });

  Fund.export = function(dateFrom, dateTo, cb) {
    return Fund.getRange(
      new Date(dateFrom * 1000),
      new Date(dateTo * 1000),
      cb
    );
  };
  Fund.remoteMethod("export", {
    http: {
      path: "/export",
      verb: "get"
    },
    accepts: [
      { arg: "dateFrom", type: "number", required: true },
      { arg: "dateTo", type: "number", required: true }
    ],
    returns: {
      arg: "funds",
      type: "array"
    }
  });

  /**
   * Gibt möglichst ähnliche Geldströme zurück
   */
  Fund.recommend = function(partlyDescription, categoryId, onlyFavorites, cb) {
    logger.debug(
      `Requested fund recommendation with input "${partlyDescription}", category "${categoryId}" and ${
        onlyFavorites ? "" : "no"
      } favorites`
    );
    if (onlyFavorites) {
      const Favorite = Fund.app.models.Favorite;
      let filter = categoryId ? { where: { categoryId: categoryId } } : null;
      Favorite.find(filter, (err, favorites) => {
        let recommondations = recommend(
          partlyDescription,
          favorites,
          "description",
          5
        );
        cb(null, recommondations);
      });
    } else {
      let filter = {
        order: ["date DESC", "description DESC"]
      };
      if (categoryId) {
        filter.where = { categoryId: categoryId };
      }
      Fund.find(filter, (err, funds) => {
        let fundDescriptions = new Map();
        let filteredFunds = [];
        for (let index = 0; index < funds.length; index++) {
          if (!fundDescriptions.has(funds[index].description)) {
            fundDescriptions.set(funds[index].description);
            filteredFunds.push(funds[index]);
          }
        }
        let recommondations = recommend(
          partlyDescription,
          filteredFunds,
          "description",
          5
        );
        cb(null, recommondations);
      });
    }
  };
  Fund.remoteMethod("recommend", {
    http: {
      path: "/recommend",
      verb: "post"
    },
    accepts: [
      { arg: "partlyDescription", type: "string", required: true },
      { arg: "categoryId", type: "number", required: false },
      { arg: "onlyFavorites", type: "boolean", required: false }
    ],
    returns: {
      arg: "recommendations",
      type: "array"
    }
  });
};
