"use strict";
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const logger = require("../../logger");

module.exports = async function(app) {
  if (process.env.DATABASE_BACKUP) {
    var cmdBackup =
      "pg_dump -F t -o -v" +
      " -d " +
      process.env.DATABASE_NAME +
      " -h " +
      (process.env.DATABASE_COMPOSE_URL || process.env.DATABASE_HOST) +
      " -p " +
      process.env.DATABASE_PORT +
      " -U " +
      process.env.DATABASE_USERNAME +
      " > " +
      process.env.DATABASE_BACKUP;
    // execute backup
    const { stdout, stderr } = await exec(cmdBackup);
    // log output
    console.log("stdout:", stdout);
    console.log("stderr:", stderr);
    logger.notice("Performed database backup.");

    process.exit(0);
  } else if (process.env.DATABASE_RECOVERY) {
    var cmdRecover =
      "pg_restore -c --if-exists -e -F t -v" +
      " -d " +
      process.env.DATABASE_NAME +
      " -h " +
      (process.env.DATABASE_COMPOSE_URL || process.env.DATABASE_HOST) +
      " -p " +
      process.env.DATABASE_PORT +
      " -U " +
      process.env.DATABASE_USERNAME +
      " " +
      process.env.DATABASE_RECOVERY;
    // execute backup
    const { stdout, stderr } = await exec(cmdRecover);
    // log output
    console.log("stdout:", stdout);
    console.log("stderr:", stderr);
    logger.notice("Performed database recovery.");

    process.exit(0);
  }
};
