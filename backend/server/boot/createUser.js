"use strict";
const logger = require("../../logger");
module.exports = async function(app) {
  if (!process.env.USERNAME_APP) return;
  let user = {
    email: process.env.EMAIL,
    username: process.env.USERNAME_APP,
    password: process.env.USER_PASSWORD
  };
  createNewSingleUser(app, user);
};

function insertUser(user, Usermodel) {
  Usermodel.create(user, function(err) {
    if (err) {
      logger.error(err);
      process.exit(2);
    }
    logger.notice("Created configured user");
  });
}

function createNewSingleUser(app, newUser) {
  newUser.emailVerified = true;
  let User = app.models.User;
  User.findOne({}, function(err, user) {
    if (err) {
      logger.error(err);
      process.exit(2);
    } else if (user != null) {
      logger.warning("Cleared User-Table since it was not empty");
      User.destroyAll({}, function(err) {
        if (err) {
          logger.error(err);
          process.exit(2);
        }
        insertUser(newUser, User);
      });
    } else {
      insertUser(newUser, User);
    }
  });
}
