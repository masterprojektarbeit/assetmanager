"use strict";
const logger = require("../../logger");

module.exports = async function(app) {
  if (process.env.FIXTURE_USER) {
    require("loopback-fixtures")(app, {
      fixturePath: "/fixtures/test/user/",
      append: true
    });
    try {
      await app.loadFixtures();
    } catch (err) {
      logger.error(err.message, err);
      process.exit(1);
    }
    logger.notice("loaded user-fixtures");
    if (process.env.NODE_ENV !== "development") process.exit(0);
  } else if (process.env.FIXTURE) {
    let path = /(\/|\\)fixtures(\/|\\).+/.exec(process.env.FIXTURE)[0];
    path = path.split("\\").join("/");
    require("loopback-fixtures")(app, { fixturePath: path, append: true });
    let ret = await app.loadFixtures();
    // ret[0][0].fixtures könnte ausgelesen und synchronisiert werden, falls gewollt
    logger.notice("loaded fixture in " + path);
    process.exit(0);
  }
};
