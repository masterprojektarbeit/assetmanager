"use strict";
const logger = require("../../logger");
const CronJob = require("cron").CronJob;
const moment = require("moment-timezone");
moment.tz.setDefault("Europe/Berlin");
const cronParser = require("cron-parser");
const cronParserOptions = {
  tz: "Europe/Berlin"
};
// Jeden Tag um 00:01 Uhr
const cronArgument = "0 1 0 * * *";

module.exports = async function(app) {
  // Sonst ist DB nicht vorhanden
  if (app.models.RegularFund) {
    await bookMissedCrons(app);
    new CronJob(cronArgument, bookRegularFunds, null, true, "Europe/Berlin", {
      app
    });
    const cronInterval = cronParser.parseExpression(cronArgument);
    logger.notice(
      `Started cronjob for regular funds, next tick will be on ${moment(
        cronInterval.next()._date
      ).format("DD.MM HH:mm")}`
    );
  }
};

async function bookMissedCrons(app) {
  let cfg = await getConfig(app);
  // cfg existiert => nicht das erste mal, dass reguläre Geldströme gebucht werden
  if (cfg && lastCronRanToday(cfg)) {
    return;
  }
  logger.notice(
    "Booking missed regular funds since last Booking was not today"
  );
  let missedDay;
  if (!cfg) {
    cfg = {};
    // Falls noch nie gebucht wurde fange heute an
    missedDay = moment();
  } else {
    missedDay = moment(cfg.lastCronStart);
    missedDay.add(1, "days");
  }
  const today = nullSecondsMinutesHours(moment());
  while (today.diff(missedDay, "days") >= 0) {
    await bookRegularFunds(missedDay, app);
    missedDay.add(1, "days");
  }
  cfg.lastCronStart = moment();
  await new Promise(res => {
    app.models.Configuration.upsert(cfg, (err, cfg) => {
      if (err) logger.error(err);
      res();
    });
  });
}

function getConfig(app) {
  const Configuration = app.models.Configuration;
  return new Promise(res =>
    Configuration.findOne({}, (err, cfg) => {
      if (err) logger.error(err);
      res(cfg);
    })
  );
}

async function bookRegularFunds(dayToBookFor, appArgument) {
  dayToBookFor = dayToBookFor || moment();
  dayToBookFor = nullSecondsMinutesHours(dayToBookFor);
  const app = appArgument || this.app;
  let cfg = await getConfig(app);
  cfg = cfg || { lastCronStart: moment() };
  if (
    !lastCronRanToday(cfg, dayToBookFor) ||
    moment(dayToBookFor).diff(nullSecondsMinutesHours(moment()), "days") === 0
  ) {
    logger.notice(
      `Cronjob started: booking RegularFunds for ${dayToBookFor.format(
        "DD.MM"
      )}`
    );
    const RegularFund = app.models.RegularFund;
    const Fund = app.models.Fund;
    await bookFrequencyFunds(RegularFund, Fund, "daily", dayToBookFor);
    await bookFrequencyFunds(RegularFund, Fund, "weekly", dayToBookFor);
    await bookFrequencyFunds(RegularFund, Fund, "monthly", dayToBookFor);
    await bookFrequencyFunds(RegularFund, Fund, "quarterYearly", dayToBookFor);
    await bookFrequencyFunds(RegularFund, Fund, "halfYearly", dayToBookFor);
    await bookFrequencyFunds(RegularFund, Fund, "yearly", dayToBookFor);
    if (!appArgument) {
      // Aus cron gestartet & nicht aus Nachholen
      cfg.lastCronStart = dayToBookFor;
      await new Promise(res => {
        app.models.Configuration.upsert(cfg, err => {
          if (err) logger.error(err);
          res();
        });
      });
    }
  } else {
    logger.notice(
      "Skipped cronjob for regular funds since it already ran today"
    );
  }
}

function lastCronRanToday(cfg, dayToBookFor) {
  return (
    moment(dayToBookFor).diff(
      nullSecondsMinutesHours(moment(cfg.lastCronStart)),
      "days"
    ) === 0
  );
}

async function bookFrequencyFunds(RegularFund, Fund, frequency, dayToBookFor) {
  let regularFunds = await new Promise(res =>
    RegularFund.getByFrequenzy(true, false, frequency, (err, funds) => {
      if (err) logger.error(err);
      res(funds);
    })
  );
  let copy = moment(dayToBookFor);
  copy.subtract(1, "days"); // Damit der nächste Cron auch schon am "nächsten Tag" gebucht wird
  cronParserOptions.currentDate = new Date(
    nullSecondsMinutesHours(copy).format()
  );
  regularFunds.forEach(regularFund => {
    if (dayToBookFor.diff(moment(regularFund.lastbooking), "hours") > 0) return; // lastBooking < dayToBookFor
    const cronInterval = cronParser.parseExpression(
      regularFund.frequenzy,
      cronParserOptions
    );
    if (cronIntervalAffectedToday(cronInterval, regularFund, dayToBookFor)) {
      Fund.create(
        {
          description: regularFund.description,
          value: regularFund.value,
          date: new Date(),
          categoryId: regularFund.categoryId
        },
        (err, newFund) => {
          if (err) {
            logger.error(err);
          }
          logger.info(
            `created new fund "${
              regularFund.description
            }" for ${frequency} regular fund`
          );
        }
      );
    }
  });
}

function cronIntervalAffectedToday(cronInterval, regularFund, dayToBookFor) {
  const nextCronBooking = nullSecondsMinutesHours(cronInterval.next()._date);
  if (regularFund.frequenzy.includes("/")) {
    // Vierteljährlich oder Halbjährlich => erste Buchung entscheidet Monat
    const firstBooking = nullSecondsMinutesHours(
      moment(regularFund.firstbooking)
    );
    const now = nullSecondsMinutesHours(dayToBookFor);
    const monthdiff = now.diff(firstBooking, "months");
    firstBooking.add(monthdiff, "month");
    if (regularFund.frequenzy.includes("/3")) {
      return (
        monthdiff % 3 === 0 && dayToBookFor.diff(firstBooking, "days") === 0
      );
    } else {
      return (
        monthdiff % 6 === 0 && dayToBookFor.diff(firstBooking, "days") === 0
      );
    }
  } else {
    return (
      nullSecondsMinutesHours(dayToBookFor).diff(nextCronBooking, "days") === 0
    );
  }
}

function nullSecondsMinutesHours(moment) {
  moment.millisecond(0);
  moment.second(0);
  moment.minute(0);
  moment.hour(0);
  return moment;
}
