"use strict";
const logger = require("../../logger");
const bodyParser = require("body-parser");
const defaultConfig = {
  combineThreshold: 5,
  nightMode: true
}; // Bei Veränderung auch im Model Anpassen!
module.exports = function(app) {
  app.use(bodyParser.json()); // for parsing application/json
  app.get("/api/Configuration", function(req, res) {
    app.models.Configuration.findOne({}, (err, cfg) => {
      if (err) logger.error(err);
      if (cfg) {
        cfg.lastCronStart = undefined;
        res.send({
          config: cfg
        });
      } else {
        res.send(null);
      }
    });
  });
  app.post("/api/Configuration", function(req, res) {
    app.models.Configuration.findOne({}, (err, cfg) => {
      if (err) {
        logger.error(err);
        res.send(err);
      }
      if (cfg) {
        cfg.combineThreshold = parseFloat(req.body.combineThreshold);
        cfg.nightMode = req.body.nightMode;
        app.models.Configuration.upsert(cfg, err => {
          if (err) {
            logger.error(err);
            res.send(err);
          } else {
            res.send(cfg);
          }
        });
      } else {
        let err = "Cannot update config: no row in database!";
        logger.error(err);
        res.send(err);
      }
    });
  });
};
