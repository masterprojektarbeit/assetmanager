"use strict";
const logger = require("../../logger");
module.exports = async function(app) {
  if (process.env.DATABASE_AUTOMIGRATE) {
    await app.dataSources.db.automigrate();
    logger.notice("Performed automigration.");
    process.exit(0);
  } else if (process.env.DATABASE_AUTOUPDATE) {
    await app.dataSources.db.autoupdate();
    logger.notice("Performed autoupdate.");
    process.exit(0);
  }
};
