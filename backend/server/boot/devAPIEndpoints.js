"use strict";
const logger = require("../../logger");
module.exports = function(app) {
  if (process.env.NODE_ENV !== "development") return;
  logger.notice("Added development API-Endpoints");

  app.get("/dev/resetDatabase", async function(req, res) {
    await app.dataSources.db.automigrate();
    logger.notice("resetDatabase via dev Endpoint");
    require("loopback-fixtures")(app, {
      fixturePath: "/fixtures/test/user/",
      append: true
    });
    await app.loadFixtures();
    logger.notice("loaded user-fixtures");
    res.send("Database reset Abgeschlossen");
  });

  var bodyParser = require("body-parser");
  app.use(bodyParser.json()); // for parsing application/json

  app.post("/dev/fixture", function(req, res) {
    let path = /(\/|\\)fixtures(\/|\\).+/.exec(req.body.path)[0];
    path = path.split("\\").join("/");
    require("loopback-fixtures")(app, { fixturePath: path, append: true });
    app.loadFixtures().then(ret => {
      // ret[0][0].fixtures könnte ausgelesen und synchronisiert werden, falls gewollt
      logger.notice("loaded fixture in " + path);
      res.send(`fixtures in ${path} geladen!`);
    });
  });
};
